package com.latam.pax.robotdistr.exceptions;

/**
 * 
 * @author dcarvaja
 *
 */
public class InputQueueException extends Exception {

    private static final long serialVersionUID = 1L;

    public InputQueueException() {        
    }

    public InputQueueException(String message) {
        super(message);
    }

    public InputQueueException(Throwable cause) {
        super(cause);
    }

    public InputQueueException(String message, Throwable cause) {
        super(message, cause);
    }
}