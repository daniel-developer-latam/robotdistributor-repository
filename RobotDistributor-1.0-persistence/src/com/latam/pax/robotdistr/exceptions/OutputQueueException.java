package com.latam.pax.robotdistr.exceptions;

/**
 * 
 * @author dcarvaja
 *
 */
public class OutputQueueException extends Exception {

    private static final long serialVersionUID = 1L;

    public OutputQueueException() {        
    }

    public OutputQueueException(String message) {
        super(message);
    }

    public OutputQueueException(Throwable cause) {
        super(cause);
    }

    public OutputQueueException(String message, Throwable cause) {
        super(message, cause);
    }
}