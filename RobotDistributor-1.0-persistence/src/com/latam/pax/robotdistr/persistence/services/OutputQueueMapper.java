package com.latam.pax.robotdistr.persistence.services;

import org.apache.ibatis.annotations.Param;

import com.latam.arq.commons.persistence.ibatis.IBatisMapper;
import com.latam.pax.robotdistr.domain.Group;
import com.latam.pax.robotdistr.domain.InputQueue;
import com.latam.pax.robotdistr.domain.OfficeAccount;
import com.latam.pax.robotdistr.domain.OutputQueue;


/**
 * 
 * @author dcarvaja
 *
 */
public interface OutputQueueMapper extends IBatisMapper {
	
	OutputQueue getOutputQueueByInputQueueAndTypeAndAOCAndGroup(
			@Param("inputQueue") InputQueue inputQueue,	@Param("businessType") String businessType, 
					@Param("officeAccount") OfficeAccount officeAccount, @Param("group") Group group);
	
}