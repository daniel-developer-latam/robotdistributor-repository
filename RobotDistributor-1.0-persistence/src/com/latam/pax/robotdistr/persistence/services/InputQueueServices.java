package com.latam.pax.robotdistr.persistence.services;

import java.util.List;

import com.latam.arq.commons.persistence.ibatis.IBatisPersistenceServices;
import com.latam.pax.robotdistr.domain.InputQueue;
import com.latam.pax.robotdistr.exceptions.InputQueueException;

/**
 * 
 * @author dcarvaja
 *
 */
public class InputQueueServices extends IBatisPersistenceServices<InputQueueMapper> {
	
	
	public List<InputQueue> getAllInputQueue() throws InputQueueException {
		try {
			return mapper.getAllInputQueue();
		} catch (Exception e) {
			throw new InputQueueException("Cannot get InputQueue from database", e);
		}		
	}	
}
