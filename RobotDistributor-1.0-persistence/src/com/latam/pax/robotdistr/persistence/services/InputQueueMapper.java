package com.latam.pax.robotdistr.persistence.services;

import java.util.List;

import com.latam.arq.commons.persistence.ibatis.IBatisMapper;
import com.latam.pax.robotdistr.domain.InputQueue;
import com.latam.pax.robotdistr.exceptions.InputQueueException;


/**
 * 
 * @author dcarvaja
 *
 */
public interface InputQueueMapper extends IBatisMapper {	
	
	List<InputQueue> getAllInputQueue() throws InputQueueException;
	
}