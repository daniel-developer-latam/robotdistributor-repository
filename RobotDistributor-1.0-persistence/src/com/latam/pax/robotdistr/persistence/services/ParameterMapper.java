package com.latam.pax.robotdistr.persistence.services;

import org.apache.ibatis.annotations.Param;

import com.latam.arq.commons.persistence.ibatis.IBatisMapper;
import com.latam.pax.robotdistr.domain.Parameter;
import com.latam.pax.robotdistr.exceptions.ParameterException;


/**
 * 
 * @author dcarvaja
 *
 */
public interface ParameterMapper extends IBatisMapper {

	Parameter getParameterByKey(@Param("key") String key) throws ParameterException;	
	
}