package com.latam.pax.robotdistr.persistence.services;

import com.latam.arq.commons.persistence.ibatis.IBatisPersistenceServices;
import com.latam.pax.robotdistr.domain.Group;
import com.latam.pax.robotdistr.domain.InputQueue;
import com.latam.pax.robotdistr.domain.OfficeAccount;
import com.latam.pax.robotdistr.domain.OutputQueue;
import com.latam.pax.robotdistr.exceptions.OutputQueueException;

/**
 * 
 * @author dcarvaja
 *
 */
public class OutputQueueServices extends IBatisPersistenceServices<OutputQueueMapper> {
		
	public OutputQueue getOutputQueueByInputQueueAndTypeAndAOCAndGroup(InputQueue inputQueue, String businessType, OfficeAccount officeAccount, Group group) throws OutputQueueException{
		try {
			return mapper.getOutputQueueByInputQueueAndTypeAndAOCAndGroup(inputQueue, businessType, officeAccount, group);
		} catch (Exception e) {
			throw new OutputQueueException("Cannot get OutputQueue from database", e);
		}		
	}
	
}
