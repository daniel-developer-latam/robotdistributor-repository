package com.latam.pax.robotdistr.persistence.services;

import com.latam.arq.commons.persistence.ibatis.IBatisPersistenceServices;
import com.latam.pax.robotdistr.domain.Parameter;
import com.latam.pax.robotdistr.exceptions.ParameterException;

/**
 * 
 * @author dcarvaja
 *
 */
public class ParameterServices extends IBatisPersistenceServices<ParameterMapper> {

	public Parameter getParameterByKey(String key) throws ParameterException {
		try {
			return mapper.getParameterByKey(key);
		} catch (Exception e) {
			throw new ParameterException("Cannot get Parameter from database", e);
		}
	}
	
}
