package com.latam.pax.robotdistr.command;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@ToString
public class ToDistributeCommand implements Command, Serializable {	
	private static final long serialVersionUID = 1L;
	
	@Getter
	@Setter
	@XmlElement
	private String uuid;
	
	@Getter
	@Setter
	@XmlElement
	private String recordLocator;
	
	@Getter
	@Setter
	@XmlElement
	private String number;
	
	@Getter
	@Setter
	@XmlElement
	private String pseudoCityCode;
	
	@Getter
	@Setter
	@XmlElement
	private String mirrorNumber;
	
	@Getter
	@Setter
	@XmlElement
	private String mirrorPseudoCityCode;
	
}
