package com.latam.pax.robotdistr.ejb.impl;

import java.util.List;
import java.util.UUID;

import com.google.common.collect.Lists;
import com.latam.arq.commons.exceptions.LATAMException;
import com.latam.pax.robotdistr.domain.PassengerNameRecord;
import com.latam.pax.robotdistr.ejb.services.BusinessRule;
import com.latam.pax.robotdistr.enums.EventStatusEnum;
import com.latam.pax.robotdistr.enums.RuleEngineStatusEnum;
import com.latam.pax.robotdistr.event.BusinessEvent;
import com.latam.pax.robotdistr.util.UtilValidator;

/**
 * Represents the Rule Engine. This abstract class is implemented for the next rules:
 * 
 * <br /><br />
 * 
 * {@link IntegrityBusinessRuleImpl}
 * <br />
 * {@link GroupValidBusinessRuleImpl}
 * 
 * <br /><br>
 * Also acts as <b>Adapter</b> between the definition of the rules and their implementation
 * 
 * @author Everis
 * @author Daniel Carvajal (daniel.carvajal.soto@everis.com)
 * @version 1.0
 *  
 */
public class RuleEngineImpl {
	
	private List<BusinessRule> businessRules;
	
	private RuleEngineStatusEnum ruleStatus;	   
	
	private UUID universallyUniqueIdentifier;
	
	/**
	 * 
	 * Process all business rule config for passengerNameRecord until some failure
	 * 
	 * @param passengerNameRecord
	 */
	public void process(PassengerNameRecord passengerNameRecord) throws LATAMException {
		universallyUniqueIdentifier = UUID.randomUUID();		
		passengerNameRecord.setUuid(universallyUniqueIdentifier.toString());
		
		for(BusinessRule businessRule : businessRules){			
			BusinessEvent event = businessRule.execute(passengerNameRecord);
			//
			if (EventStatusEnum.RULE_SUCCESS.equals(event.getEventStatus()) || 
					EventStatusEnum.RULE_WARNING.equals(event.getEventStatus())) { // its ok
				//			
				ruleStatus = RuleEngineStatusEnum.SUCCESS; // set status engine
				
			}else if(EventStatusEnum.RULE_EXCEPTION.equals(event.getEventStatus())){
				ruleStatus = RuleEngineStatusEnum.EXCEPTION;
				break;
				
			}else if(EventStatusEnum.RULE_ERROR.equals(event.getEventStatus())){
				ruleStatus = RuleEngineStatusEnum.ERROR;
				break;
			}
		}		
	}
	
	public boolean addBusinessRule(BusinessRule businessRule) {
		boolean isAdd = false;
		if(UtilValidator.isNotEmpty(businessRules)){
			isAdd = businessRules.add(businessRule);
		}else{
			businessRules = Lists.newArrayList();
			isAdd = businessRules.add(businessRule);
		}
		return isAdd;
	}
		
	public List<BusinessRule> getBusinessRules() {
		return businessRules;
	}
	
	/**
	 * Set next rule business to process
	 * 
	 * @param ruleServiceFollowing
	 */
	public void setBusinessRules(List<BusinessRule> businessRules){
		this.businessRules = businessRules;
	}

	/**
	 * Get de {@link RuleEngineStatusEnum} for the engine.
	 * <br />
	 * The rule engine have 2 status: OK or NOK. 
	 * <br />
	 * In case OK execute all rules otherwise it executes until the rule that failed.
	 * 
	 * @return the rule engine status
	 */
	public RuleEngineStatusEnum getRuleStatus() {
		return ruleStatus;
	}	

}
