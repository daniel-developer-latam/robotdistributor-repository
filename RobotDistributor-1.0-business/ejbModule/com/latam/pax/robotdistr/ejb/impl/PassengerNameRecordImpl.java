package com.latam.pax.robotdistr.ejb.impl;

import java.util.List;
import java.util.Properties;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;

import com.latam.arq.commons.appconfig.properties.AppConfig;
import com.latam.arq.commons.appconfig.properties.AppConfigInterceptor;
import com.latam.arq.commons.exceptions.LATAMException;
import com.latam.arq.commons.exceptions.LATAMExceptionService;
import com.latam.arq.commons.logger.BusinessLoggingInterceptor;
import com.latam.arq.commons.ws.clients.WSAddPNRToHostQueueClient;
import com.latam.arq.commons.ws.clients.WSCountMessagesInQueueClient;
import com.latam.arq.commons.ws.clients.WSDeleteMessagesInQueueByRangeClient;
import com.latam.arq.commons.ws.clients.WSDisplayMessagesInQueueByRangeClient;
import com.latam.arq.commons.ws.clients.WSDisplayPnrByRecordLocatorClient;
import com.latam.entities.v3_0.QueueType;
import com.latam.pax.robotdistr.domain.InputQueue;
import com.latam.pax.robotdistr.domain.PassengerNameRecord;
import com.latam.pax.robotdistr.ejb.services.PassengerNameRecordService;
import com.latam.pax.robotdistr.enums.WebServiceStatusEnum;
import com.latam.pax.robotdistr.util.UtilConstant;
import com.latam.pax.robotdistr.util.UtilConverter;
import com.latam.pax.robotdistr.util.UtilValidator;
import com.latam.pax.robotdistr.util.UtilWsConverter;
import com.latam.ws.v3_0.CountMessagesInQueueRQ;
import com.latam.ws.v3_0.CountMessagesInQueueRS;
import com.latam.ws.v3_0.DeleteMessagesInQueueByRangeRQ;
import com.latam.ws.v3_0.DeleteMessagesInQueueByRangeRS;
import com.latam.ws.v3_0.DisplayMessagesInQueueByRangeRQ;
import com.latam.ws.v3_0.DisplayMessagesInQueueByRangeRS;
import com.latam.ws.v3_1.AddPNRToHostQueueRQ;
import com.latam.ws.v3_1.AddPNRToHostQueueRS;
import com.latam.ws.v3_2.DisplayPnrByRecordLocatorRQ;
import com.latam.ws.v3_2.DisplayPnrByRecordLocatorRS;


/**
 * EJB interface for {@link PassengerNameRecordService}.
 * <br /><br />
 * 
 * Encapsulates call clients ws SABRE in passenger name record operations.
 * 
 * @author Everis
 * @author Daniel Carvajal (daniel.carvajal.soto@everis.com)
 * @version 1.0
 *  
 */
@Stateless(name = "PassengerNameRecordImpl", description = "")
@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
@Interceptors({ AppConfigInterceptor.class })
public class PassengerNameRecordImpl implements PassengerNameRecordService {

	@EJB
	private LATAMExceptionService latamExceptionFactory;
	
	@AppConfig
	private Properties properties;

	/**
	 * 
	 * {@inheritDoc}
	 * 
	 */
	@Override	
	@Interceptors({BusinessLoggingInterceptor.class})
	public List<String> getRecordLocatorsInQueueByRange(InputQueue inputQueue, Integer indexFrom,
			Integer indexTo) throws LATAMException {
		List<String> recordLocators = null;		
		DisplayMessagesInQueueByRangeRS serviceResponse = null;
		DisplayMessagesInQueueByRangeRQ serviceRequest = new DisplayMessagesInQueueByRangeRQ();

		
		serviceRequest.setQueue(UtilWsConverter.toQueueType(inputQueue));
		serviceRequest.setIndexFrom(UtilConverter.toBigInteger(indexFrom));
		serviceRequest.setIndexTo(UtilConverter.toBigInteger(indexTo));
		
		// send and response ws
		serviceResponse = WSDisplayMessagesInQueueByRangeClient.call(serviceRequest, latamExceptionFactory);		
		
		// converter
		recordLocators = UtilWsConverter.toRecordLocators(serviceResponse);

		return recordLocators;
	}
	
	/**
	 * 
	 * {@inheritDoc}
	 * 
	 */
	@Override
	@Interceptors({BusinessLoggingInterceptor.class})
	public PassengerNameRecord getPnrByRecordLocator(String recordLocator)
			throws LATAMException {
		PassengerNameRecord passengerNameRecord = null;
		DisplayPnrByRecordLocatorRS serviceResponse = null;		
		DisplayPnrByRecordLocatorRQ serviceRequest = new DisplayPnrByRecordLocatorRQ();			
		
		serviceRequest.setRecordLocator(recordLocator);
						
		// send and response ws
		serviceResponse = WSDisplayPnrByRecordLocatorClient.call(serviceRequest, latamExceptionFactory);
		
		// transform to pnr
		passengerNameRecord = UtilWsConverter.toPassengerNameRecord(serviceResponse);
		
		return passengerNameRecord; 
	}		

	/**
	 * 
	 * {@inheritDoc}
	 * 
	 */
	@Override
	@Interceptors({BusinessLoggingInterceptor.class})
	public boolean deletePnrsInQueueByRange(InputQueue inputQueue, Integer indexFrom, Integer indexTo) throws LATAMException {
		boolean isDeteleMessages = false;
		//
		DeleteMessagesInQueueByRangeRS serviceResponse = null;
		DeleteMessagesInQueueByRangeRQ serviceRequest = new DeleteMessagesInQueueByRangeRQ();
		
		serviceRequest.setQueue(UtilWsConverter.toQueueType(inputQueue));
		serviceRequest.setIndexFrom(UtilConverter.toBigInteger(indexFrom));
		serviceRequest.setIndexTo(UtilConverter.toBigInteger(indexTo));	
					
		// send and response ws
		serviceResponse = WSDeleteMessagesInQueueByRangeClient.call(serviceRequest, latamExceptionFactory);

		if(UtilValidator.isNotEmpty(serviceResponse.getServiceStatus()) &&					
		   serviceResponse.getServiceStatus().getCode() == WebServiceStatusEnum.SUCCESS.getCode()){		   
			isDeteleMessages = true;
		}		
		return isDeteleMessages;
	}

	/**
	 * 
	 * {@inheritDoc}
	 * 
	 */
	@Override
	@Interceptors({BusinessLoggingInterceptor.class})
	public boolean addPnrInQueue(PassengerNameRecord passengerNameRecord, QueueType queueType) throws LATAMException {
		boolean isAddMessageInQueue = false;

		AddPNRToHostQueueRS serviceResponse = null;		
		AddPNRToHostQueueRQ serviceRequest = new AddPNRToHostQueueRQ();

		serviceRequest.setQueue(queueType);		
		serviceRequest.setReasonCode(
				properties.getProperty(UtilConstant.ADDPNRTOHOSTQUEUE_3_1_0_REQUEST_PREFATORYINSTRUCTIONCODE));		
		serviceRequest.setRecordLocator(passengerNameRecord.getRecordLocator());
		
		// send and response ws
		serviceResponse = WSAddPNRToHostQueueClient.call(serviceRequest, latamExceptionFactory);

		if (UtilValidator.isNotEmpty(serviceResponse.getServiceStatus())				
				&& serviceResponse.getServiceStatus().getCode() == WebServiceStatusEnum.SUCCESS.getCode()) {
			isAddMessageInQueue = true;			
		}
		
		return isAddMessageInQueue;
	}
	
	/**
	 * 
	 * {@inheritDoc}
	 * 
	 * 
	 */
	@Interceptors({BusinessLoggingInterceptor.class})
	public Integer countPnrsInQueue(InputQueue inputQueue) throws LATAMException {
		Integer countMessages = 0;
		//
		CountMessagesInQueueRS serviceResponse = null;
		CountMessagesInQueueRQ serviceRequest = new CountMessagesInQueueRQ();
		
		serviceRequest.setQueue(UtilWsConverter.toQueueType(inputQueue));
		
		// send and response ws
		serviceResponse = WSCountMessagesInQueueClient.call(serviceRequest, latamExceptionFactory);
				
		countMessages =  UtilWsConverter.toAmmountMessage(serviceResponse);
		
		return countMessages;
		
	}	
	
	
	/**
	 * 
	 * {@inheritDoc}
	 * 
	 */
	@Override
	@Interceptors({BusinessLoggingInterceptor.class})
	public void sendPnrToQueue(PassengerNameRecord passengerNameRecord, QueueType queueType) throws LATAMException {
		this.addPnrInQueue(passengerNameRecord, queueType);		
	}
		
}
