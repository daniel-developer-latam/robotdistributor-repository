package com.latam.pax.robotdistr.ejb.impl;

import java.util.Set;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.validation.ConstraintViolation;

import com.google.common.collect.Sets;
import com.latam.arq.commons.exceptions.LATAMException;
import com.latam.pax.robotdistr.domain.PassengerNameRecord;
import com.latam.pax.robotdistr.ejb.services.GroupValidBusinessRuleService;
import com.latam.pax.robotdistr.ejb.services.BusinessEventHandlerService;
import com.latam.pax.robotdistr.enums.EventStatusEnum;
import com.latam.pax.robotdistr.event.BusinessEvent;
import com.latam.pax.robotdistr.event.GroupValidatedBusinessEvent;
import com.latam.pax.robotdistr.util.UtilValidator;
import com.latam.pax.robotdistr.validator.group.GroupValidGroup;

/**
 * EJB implementation for interface for {@link GroupValidBusinessRuleService}.
 * <br /><br />
 * 
 *  PassengerNameRecordIntegrityBusinessRule implementation
 *  
 * @author Everis
 * @author Daniel Carvajal (daniel.carvajal.soto@everis.com)
 * @version 1.0
 *  
 */

@Stateless(name = "GroupValidBusinessRuleImpl", description = "")
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class GroupValidBusinessRuleImpl extends RuleEngineImpl implements GroupValidBusinessRuleService {

	@EJB
	private BusinessEventHandlerService businessEventHandlerService;
	
	/**
	 * 
	 * {@inheritDoc}
	 * 
	 */
	@Override
	public BusinessEvent execute(PassengerNameRecord passengerNameRecord) throws LATAMException {
		GroupValidatedBusinessEvent groupValidatedBusinessEvent = null;
		Set<ConstraintViolation<?>> constraintViolations = Sets.newHashSet();
		
		// validate group name
		boolean containsValidGroup = false;
		if(UtilValidator.isNotEmpty(passengerNameRecord.getGroup()) &&
				UtilValidator.isNotEmpty(passengerNameRecord.getGroup().getGroupName())){
			containsValidGroup = true;
		}
		passengerNameRecord.setValidGroup(containsValidGroup);
		
		// evaluate flag		
		constraintViolations.addAll(
				UtilValidator.validate(passengerNameRecord.getGroup(),GroupValidGroup.class));
		
		if (constraintViolations.isEmpty()) {			
			groupValidatedBusinessEvent = new GroupValidatedBusinessEvent(passengerNameRecord, constraintViolations, EventStatusEnum.RULE_SUCCESS);			
		} else {
			groupValidatedBusinessEvent = new GroupValidatedBusinessEvent(passengerNameRecord, constraintViolations, EventStatusEnum.RULE_EXCEPTION);			
		}
		//haldle event
		businessEventHandlerService.handle(groupValidatedBusinessEvent);
		
		return groupValidatedBusinessEvent;		
	}
}
