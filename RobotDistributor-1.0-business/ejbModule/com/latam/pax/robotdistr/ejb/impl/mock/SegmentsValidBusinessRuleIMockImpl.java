package com.latam.pax.robotdistr.ejb.impl.mock;

import java.util.Set;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;
import javax.validation.ConstraintViolation;

import com.google.common.collect.Sets;
import com.latam.arq.commons.appconfig.properties.AppConfigInterceptor;
import com.latam.arq.commons.exceptions.LATAMException;
import com.latam.pax.robotdistr.domain.PassengerNameRecord;
import com.latam.pax.robotdistr.ejb.impl.RuleEngineImpl;
import com.latam.pax.robotdistr.ejb.services.mock.SegmentsValidBusinessRuleMockService;
import com.latam.pax.robotdistr.enums.EventStatusEnum;
import com.latam.pax.robotdistr.event.BusinessEvent;
import com.latam.pax.robotdistr.event.SegmentsValidatedBusinessEvent;

import lombok.extern.slf4j.Slf4j;


@Slf4j
@Stateless(name = "SegmentsValidBusinessRuleImpl", description = "")
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
@Interceptors({ AppConfigInterceptor.class })
public class SegmentsValidBusinessRuleIMockImpl extends RuleEngineImpl implements SegmentsValidBusinessRuleMockService {

	@Override	
	public BusinessEvent execute(PassengerNameRecord passengerNameRecord) throws LATAMException {
		SegmentsValidatedBusinessEvent segmentsValidatedBusinessEvent = null;
		Set<ConstraintViolation<?>> constraintViolations = Sets.newHashSet();
		
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			logger.error(e.getMessage(), e);
		}
		
		segmentsValidatedBusinessEvent = 
						new SegmentsValidatedBusinessEvent(passengerNameRecord, constraintViolations, EventStatusEnum.RULE_EXCEPTION);		
				
		return segmentsValidatedBusinessEvent;
	}			
}
