package com.latam.pax.robotdistr.ejb.impl;

import java.util.Set;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;
import javax.validation.ConstraintViolation;

import com.google.common.collect.Sets;
import com.latam.arq.commons.appconfig.properties.AppConfigInterceptor;
import com.latam.arq.commons.exceptions.LATAMException;
import com.latam.pax.robotdistr.domain.OutputQueue;
import com.latam.pax.robotdistr.domain.Parameter;
import com.latam.pax.robotdistr.domain.PassengerNameRecord;
import com.latam.pax.robotdistr.ejb.services.BusinessEventHandlerService;
import com.latam.pax.robotdistr.ejb.services.CheckDomBrazilBusinessRuleService;
import com.latam.pax.robotdistr.ejb.services.RepositoryService;
import com.latam.pax.robotdistr.ejb.services.RouteBusinessRuleService;
import com.latam.pax.robotdistr.enums.EventStatusEnum;
import com.latam.pax.robotdistr.enums.ParametersEnum;
import com.latam.pax.robotdistr.enums.QueueTypeEnum;
import com.latam.pax.robotdistr.event.BusinessEvent;
import com.latam.pax.robotdistr.event.IntegrityValidatedBusinessEvent;
import com.latam.pax.robotdistr.event.RoutedBusinessEvent;
import com.latam.pax.robotdistr.util.UtilValidator;
import com.latam.pax.robotdistr.validator.group.IntegrityGroupName;

/**
 * EJB implementation for interface for {@link CheckDomBrazilBusinessRuleService}.
 * <br /><br />
 * 
 *  PassengerNameRecordSegmentsValidBusinessRule implementation
 *  
 * @author Everis
 * @author Daniel Carvajal (daniel.carvajal.soto@everis.com)
 * @version 1.0
 *  
 */
@Stateless(name = "RouteBusinessRuleImpl", description = "")
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
@Interceptors({ AppConfigInterceptor.class })
public class RouteBusinessRuleImpl extends RuleEngineImpl implements RouteBusinessRuleService {

	@EJB
	private RepositoryService repositoryService;
	
	@EJB
	private BusinessEventHandlerService businessEventHandlerService;
		
	
	/**
	 * 
	 * 
	 * {@inheritDoc}
	 * 
	 * 
	 * TODO Colocar en los comentarios que la cola por defecto es DOM  
	 * 
	 */
	@Override	
	public BusinessEvent execute(PassengerNameRecord passengerNameRecord) throws LATAMException {
		RoutedBusinessEvent routedBusinessEvent = null;
		OutputQueue outputQueues = null;	
		Set<ConstraintViolation<?>> constraintViolations = Sets.newHashSet();
		
		try {		
			constraintViolations.addAll(
					UtilValidator.validate(passengerNameRecord.getGroup(), IntegrityGroupName.class));
			if(constraintViolations.isEmpty()){
				// Set group type
				passengerNameRecord.getGroup().setGroupType( // groupType 4 first characters
						passengerNameRecord.getGroup().getGroupName().substring(0, 4));
			}else{
				IntegrityValidatedBusinessEvent integrityValidatedBusinessEvent = 
						new IntegrityValidatedBusinessEvent(passengerNameRecord, constraintViolations, EventStatusEnum.RULE_WARNING);
				businessEventHandlerService.handle(integrityValidatedBusinessEvent);
			}
			
			//					
			if(passengerNameRecord.isContainsFullBrSegments()){ // is DOM segments
				// Dom queue
				outputQueues = 
						repositoryService.getOutputQueueByInputQueueAndTypeAndAOCAndGroup(
							passengerNameRecord.getInputQueue(), QueueTypeEnum.DOMESTICO.getKey(), passengerNameRecord.getOfficeAccount(), passengerNameRecord.getGroup());
				
				if(UtilValidator.isNotEmpty(outputQueues)) { // if exist config					
					routedBusinessEvent = 
						new RoutedBusinessEvent(passengerNameRecord, outputQueues, constraintViolations, EventStatusEnum.RULE_SUCCESS);
					
				}else{
					// Default Dom queue
					Parameter queueDomNumberParameter = repositoryService.getParameterByKey(ParametersEnum.QUEUE_DEFAULT_DOM_NUMBER.getKey());
					Parameter queueDomPseudoCityCode = repositoryService.getParameterByKey(ParametersEnum.QUEUE_DEFAULT_DOM_PSEUDOCITYCODE.getKey());
					OutputQueue defaultOutputQueue = new OutputQueue();
					defaultOutputQueue.setBusinessType(QueueTypeEnum.DOMESTICO.getKey());
					defaultOutputQueue.setNumber(queueDomNumberParameter.getValue());
					defaultOutputQueue.setPseudoCityCode(queueDomPseudoCityCode.getValue());
					//
					routedBusinessEvent = 
							new RoutedBusinessEvent(passengerNameRecord, defaultOutputQueue, constraintViolations, EventStatusEnum.RULE_EXCEPTION);
				}			
			}else { // is INTER segments
				//Inter Queue
				outputQueues = 
						repositoryService.getOutputQueueByInputQueueAndTypeAndAOCAndGroup(
								passengerNameRecord.getInputQueue(), QueueTypeEnum.INTERNACIONAL.getKey(), passengerNameRecord.getOfficeAccount(), passengerNameRecord.getGroup());
				
				if(UtilValidator.isNotEmpty(outputQueues)) { // if exist config
					routedBusinessEvent = 
							new RoutedBusinessEvent(passengerNameRecord, outputQueues, constraintViolations, EventStatusEnum.RULE_SUCCESS);
					
				}else{
					// Default Inter queue
					Parameter queueInterumberParameter = repositoryService.getParameterByKey(ParametersEnum.QUEUE_DEFAULT_INTER_NUMBER.getKey());
					Parameter queueInterPseudoCityCode = repositoryService.getParameterByKey(ParametersEnum.QUEUE_DEFAULT_INTER_PSEUDOCITYCODE.getKey());
					OutputQueue defaultOutputQueue = new OutputQueue();
					defaultOutputQueue.setBusinessType(QueueTypeEnum.INTERNACIONAL.getKey());
					defaultOutputQueue.setNumber(queueInterumberParameter.getValue());
					defaultOutputQueue.setPseudoCityCode(queueInterPseudoCityCode.getValue());
					//
					routedBusinessEvent = 
							new RoutedBusinessEvent(passengerNameRecord, defaultOutputQueue, constraintViolations, EventStatusEnum.RULE_EXCEPTION);	
				}
			}		 
			// handle event
			businessEventHandlerService.handle(routedBusinessEvent);
			
		}catch(LATAMException e) {
			routedBusinessEvent = 
					new RoutedBusinessEvent(passengerNameRecord, null, constraintViolations, EventStatusEnum.RULE_EXCEPTION, e);
			businessEventHandlerService.handle(routedBusinessEvent);
			
			throw e;
		}					
		return routedBusinessEvent;		
	}			
}
