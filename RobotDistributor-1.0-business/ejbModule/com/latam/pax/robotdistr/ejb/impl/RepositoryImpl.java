package com.latam.pax.robotdistr.ejb.impl;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;

import com.latam.arq.commons.exceptions.LATAMException;
import com.latam.arq.commons.exceptions.LATAMExceptionService;
import com.latam.arq.commons.logger.BusinessLoggingInterceptor;
import com.latam.arq.commons.persistence.PersistenceUtilException;
import com.latam.arq.commons.persistence.PersistenceUtilService;
import com.latam.pax.robotdistr.domain.Group;
import com.latam.pax.robotdistr.domain.InputQueue;
import com.latam.pax.robotdistr.domain.OfficeAccount;
import com.latam.pax.robotdistr.domain.OutputQueue;
import com.latam.pax.robotdistr.domain.Parameter;
import com.latam.pax.robotdistr.ejb.services.RepositoryService;
import com.latam.pax.robotdistr.exceptions.InputQueueException;
import com.latam.pax.robotdistr.exceptions.OutputQueueException;
import com.latam.pax.robotdistr.exceptions.ParameterException;
import com.latam.pax.robotdistr.persistence.services.InputQueueServices;
import com.latam.pax.robotdistr.persistence.services.OutputQueueServices;
import com.latam.pax.robotdistr.persistence.services.ParameterServices;
import com.latam.pax.robotdistr.util.UtilConstant;


@Stateless(name = "RepositoryImpl", description = "")
@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
public class RepositoryImpl implements RepositoryService {

	@EJB
	private PersistenceUtilService persistenceService;
		
	@EJB
	private LATAMExceptionService exceptionFactory;
	
	@Override
	@Interceptors({BusinessLoggingInterceptor.class})
	public Parameter getParameterByKey(String key) throws LATAMException {
		try {
			ParameterServices services = persistenceService.getService(ParameterServices.class);
			return services.getParameterByKey(key);
		} catch (ParameterException e) {
			throw exceptionFactory.createLATAMException(UtilConstant.ROBOT_DISTRIBUTOR_PARAMETER_EXCEPTION_ID, e);
		} catch (PersistenceUtilException e) {
			throw exceptionFactory.createLATAMException(UtilConstant.PERSISTENCE_UTIL_EXCEPTION_ID, e);
		}
	}
	
	@Override
	@Interceptors({BusinessLoggingInterceptor.class})
	public List<InputQueue> getAllInputQueue() throws LATAMException {
		try {
			InputQueueServices services = persistenceService.getService(InputQueueServices.class);
			return services.getAllInputQueue();
		} catch (InputQueueException e) {
			throw exceptionFactory.createLATAMException(UtilConstant.ROBOT_DISTRIBUTOR_INPUTQUEUE_EXCEPTION_ID, e);
		} catch (PersistenceUtilException e) {
			throw exceptionFactory.createLATAMException(UtilConstant.PERSISTENCE_UTIL_EXCEPTION_ID, e);
		}
	}

	@Override
	@Interceptors({BusinessLoggingInterceptor.class})
	public OutputQueue getOutputQueueByInputQueueAndTypeAndAOCAndGroup(InputQueue inputQueue, String businessType, OfficeAccount officeAccount, Group group) throws LATAMException {
		try {
			OutputQueueServices services = persistenceService.getService(OutputQueueServices.class);
			return services.getOutputQueueByInputQueueAndTypeAndAOCAndGroup(inputQueue, businessType, officeAccount, group);
		} catch (OutputQueueException e) {
			throw exceptionFactory.createLATAMException(UtilConstant.ROBOT_DISTRIBUTOR_OUTPUTQUEUE_EXCEPTION_ID, e);
		} catch (PersistenceUtilException e) {
			throw exceptionFactory.createLATAMException(UtilConstant.PERSISTENCE_UTIL_EXCEPTION_ID, e);
		}
	}	
}
