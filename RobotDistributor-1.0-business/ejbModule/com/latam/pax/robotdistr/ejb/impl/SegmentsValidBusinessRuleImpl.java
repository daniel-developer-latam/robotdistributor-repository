package com.latam.pax.robotdistr.ejb.impl;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;
import javax.validation.ConstraintViolation;

import com.google.common.collect.Sets;
import com.latam.arq.commons.appconfig.properties.AppConfigInterceptor;
import com.latam.arq.commons.exceptions.LATAMException;
import com.latam.pax.robotdistr.domain.FlightSegment;
import com.latam.pax.robotdistr.domain.Parameter;
import com.latam.pax.robotdistr.domain.PassengerNameRecord;
import com.latam.pax.robotdistr.ejb.services.BusinessEventHandlerService;
import com.latam.pax.robotdistr.ejb.services.RepositoryService;
import com.latam.pax.robotdistr.ejb.services.SegmentsValidBusinessRuleService;
import com.latam.pax.robotdistr.enums.EventStatusEnum;
import com.latam.pax.robotdistr.enums.ParametersEnum;
import com.latam.pax.robotdistr.event.BusinessEvent;
import com.latam.pax.robotdistr.event.SegmentsValidatedBusinessEvent;
import com.latam.pax.robotdistr.util.UtilValidator;
import com.latam.pax.robotdistr.validator.group.SegmentsValidGroup;

/**
 * EJB implementation for interface for {@link PassengerNameRecordSegmentsValidBusinessRuleEJBService}.
 * <br /><br />
 * 
 *  PassengerNameRecordSegmentsValidBusinessRule implementation
 *  
 * @author Everis
 * @author Daniel Carvajal (daniel.carvajal.soto@everis.com)
 * @version 1.0
 *  
 */
@Stateless(name = "SegmentsValidBusinessRuleService", description = "")
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
@Interceptors({ AppConfigInterceptor.class })
public class SegmentsValidBusinessRuleImpl extends RuleEngineImpl implements SegmentsValidBusinessRuleService {

	
	@EJB
	private RepositoryService repositoryService;
	
	@EJB
	private BusinessEventHandlerService businessEventHandlerService;
	
	
	/**
	 * 
	 * 
	 * {@inheritDoc}
	 * 
	 */
	@Override	
	public BusinessEvent execute(PassengerNameRecord passengerNameRecord) throws LATAMException {
		SegmentsValidatedBusinessEvent segmentsValidatedBusinessEvent = null;
		Set<ConstraintViolation<?>> constraintViolations = Sets.newHashSet();
		try {				
			Parameter parameter = repositoryService.getParameterByKey(ParametersEnum.SEGMENT_STATUS.getKey());		
			List<String> segmentCodes = Arrays.asList(parameter.getValue().trim().split(","));
			// validate segments	
			boolean containsValidSegments = false;					
				for (FlightSegment flightSegment : passengerNameRecord.getFlightSegments()) {			
					if (UtilValidator.isNotEmpty(flightSegment)) {
						containsValidSegments = 
									segmentCodes.stream().anyMatch(segmentCode -> segmentCode.trim().equals(flightSegment.getSegmentStatus()));
						if(containsValidSegments){
							break;
						}							
					}		
				} // end for		
			passengerNameRecord.setValidSegments(containsValidSegments);				
		
			// evaluate flag		 		
			constraintViolations.addAll(UtilValidator.validate(passengerNameRecord,	SegmentsValidGroup.class));
		
			if (constraintViolations.isEmpty()) {
				segmentsValidatedBusinessEvent = 
						new SegmentsValidatedBusinessEvent(passengerNameRecord, constraintViolations, EventStatusEnum.RULE_SUCCESS);		
			} else {
				segmentsValidatedBusinessEvent =
						new SegmentsValidatedBusinessEvent(passengerNameRecord, constraintViolations, EventStatusEnum.RULE_EXCEPTION);		
			}
			// handle event
			businessEventHandlerService.handle(segmentsValidatedBusinessEvent);
			
		}catch(LATAMException e){
			segmentsValidatedBusinessEvent =
					new SegmentsValidatedBusinessEvent(passengerNameRecord, constraintViolations, EventStatusEnum.RULE_ERROR, e);
			// handle event
			businessEventHandlerService.handle(segmentsValidatedBusinessEvent);
			
			throw e;
		}		
		return segmentsValidatedBusinessEvent;
	}			
}
