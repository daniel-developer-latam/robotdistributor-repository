package com.latam.pax.robotdistr.ejb.impl;

import java.util.Optional;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import org.apache.commons.collections4.CollectionUtils;

import com.latam.arq.commons.exceptions.LATAMException;
import com.latam.arq.commons.exceptions.LATAMExceptionService;
import com.latam.pax.robotdistr.domain.Parameter;
import com.latam.pax.robotdistr.domain.PassengerNameRecord;
import com.latam.pax.robotdistr.domain.Remark;
import com.latam.pax.robotdistr.ejb.services.RemarkService;
import com.latam.pax.robotdistr.ejb.services.RepositoryService;
import com.latam.pax.robotdistr.enums.ParametersEnum;
import com.latam.pax.robotdistr.util.UtilValidator;

@Stateless(name = "RemarkImpl", description = "")
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class RemarkImpl implements RemarkService {

	@EJB
	private RepositoryService repositoryService;
	
	@EJB
	private LATAMExceptionService latamExceptionFactory;

	
	/**
	 * 
	 * {@inheritDoc}
	 * 
	 * 
	 */
	@Override
	public boolean containsPosBrSpecialMarkFromPnr(PassengerNameRecord passengerNameRecord) throws LATAMException {
		boolean containSpecialMark = false;
		
		Parameter parameter = repositoryService.getParameterByKey(ParametersEnum.CITY_CODE_SPECIAL_MARK.getKey());
		// Detect special mark into remark
		Optional<Remark> remarkWithSpecialMark =
				CollectionUtils.emptyIfNull(passengerNameRecord.getRemarks()).stream() //			
						.filter(remark -> UtilValidator.isNotEmpty(remark))
				.filter(remark -> remark.getText().trim().equals(parameter.getValue().trim()))
		.findAny();
		//
		if(remarkWithSpecialMark.isPresent()){
			containSpecialMark = true;
		}	
		return containSpecialMark;
	}	
}
