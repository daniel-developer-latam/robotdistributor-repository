package com.latam.pax.robotdistr.ejb.impl;

import java.util.Set;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.validation.ConstraintViolation;

import com.google.common.collect.Sets;
import com.latam.arq.commons.exceptions.LATAMException;
import com.latam.pax.robotdistr.domain.PassengerNameRecord;
import com.latam.pax.robotdistr.ejb.services.EventBusService;
import com.latam.pax.robotdistr.ejb.services.IntegrityBusinessRuleService;
import com.latam.pax.robotdistr.enums.EventStatusEnum;
import com.latam.pax.robotdistr.event.BusinessEvent;
import com.latam.pax.robotdistr.event.IntegrityValidatedBusinessEvent;
import com.latam.pax.robotdistr.util.UtilValidator;
import com.latam.pax.robotdistr.validator.group.IntegrityGroup;

/**
 * EJB implementation for interface for {@link IntegrityBusinessRuleImpl}.
 * <br /><br />
 * 
 *  PassengerNameRecordIntegrityBusinessRule implementation
 *  
 * @author Everis
 * @author Daniel Carvajal (daniel.carvajal.soto@everis.com)
 * @version 1.0
 *  
 */
@Stateless(name = "IntegrityBusinessRuleImpl", description = "")
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class IntegrityBusinessRuleImpl extends RuleEngineImpl implements IntegrityBusinessRuleService {
	
	@EJB
	private EventBusService eventBusService;
	
	/**
	 * 
	 * {@inheritDoc}
	 * 
	 */
	@Override
	public BusinessEvent execute(PassengerNameRecord passengerNameRecord) throws LATAMException {
		IntegrityValidatedBusinessEvent integrityValidatedBusinessEvent = null;		
		Set<ConstraintViolation<?>> constraintViolations = Sets.newHashSet();
		
		constraintViolations.addAll( //pnr
				UtilValidator.validate(passengerNameRecord, IntegrityGroup.class));
		if(UtilValidator.isNotEmpty(passengerNameRecord.getGroup())){
			constraintViolations.addAll( //group
					UtilValidator.validate(passengerNameRecord.getGroup(), IntegrityGroup.class));
		}
		if(UtilValidator.isNotEmpty(passengerNameRecord.getFlightSegments())){
			constraintViolations.addAll( //segments
					UtilValidator.validate(passengerNameRecord.getFlightSegments(), IntegrityGroup.class));
		}	
		
		if(constraintViolations.isEmpty()){			
			integrityValidatedBusinessEvent =
						new IntegrityValidatedBusinessEvent(passengerNameRecord, constraintViolations, EventStatusEnum.RULE_SUCCESS);			
		}else{
			integrityValidatedBusinessEvent =
						new IntegrityValidatedBusinessEvent(passengerNameRecord, constraintViolations, EventStatusEnum.RULE_EXCEPTION);		
		}
		//haldle event
		eventBusService.publishEvent(integrityValidatedBusinessEvent);
		
		return integrityValidatedBusinessEvent;	
	}
}
