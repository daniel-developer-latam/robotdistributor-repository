package com.latam.pax.robotdistr.ejb.impl;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import com.latam.arq.commons.exceptions.LATAMException;
import com.latam.pax.robotdistr.ejb.services.BusinessEventHandlerService;
import com.latam.pax.robotdistr.ejb.services.CommandBusService;
import com.latam.pax.robotdistr.ejb.services.EventBusService;
import com.latam.pax.robotdistr.event.BusinessEvent;

/**
 * EJB implementation for interface {@link CommandBusService}.
 * <br /><br /> 
 *  
 * Publish messages in Queues.
 * 
 * @author Everis
 * @author Daniel Carvajal (daniel.carvajal.soto@everis.com)
 * @version 1.0
 *  
 *  @see <a href="http://www.enterpriseintegrationpatterns.com/patterns/messaging/ObserverJmsExample.html">
 * 				 http://www.enterpriseintegrationpatterns.com/patterns/messaging/ObserverJmsExample.html</a> 
 * 
 * 
 * TODO dejar bien documentado que distribuye a cola JMS y a cola HOST.
 *  
 */

@Stateless(name = "EventBusImpl", description = "")
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class EventBusImpl implements EventBusService {
				
	
	@EJB
	private BusinessEventHandlerService businessEventHandlerService;
		
	
	
	/**
	 * 
	 * {@inheritDoc}
	 * 
	 */
	@Override
	public void publishEvent(BusinessEvent event) throws LATAMException {
		businessEventHandlerService.handle(event);							
	}	
}
