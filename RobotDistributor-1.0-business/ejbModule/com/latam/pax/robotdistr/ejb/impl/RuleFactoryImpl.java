package com.latam.pax.robotdistr.ejb.impl;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.naming.NamingException;

import com.latam.arq.commons.exceptions.LATAMException;
import com.latam.arq.commons.util.LocalJNDI;
import com.latam.pax.robotdistr.ejb.services.BusinessRule;
import com.latam.pax.robotdistr.ejb.services.RuleFactoryService;

import lombok.extern.slf4j.Slf4j;


/**
 * EJB implementation for interface {@link RobotPriceQuoteRuleFactoryEJBService}.
 * <br /><br /> 
 *  
 * Factory for the Rule Engine {@link RuleEngineImpl}.
 * 
 * @author Everis
 * @author Daniel Carvajal (daniel.carvajal.soto@everis.com)
 * @version 1.0
 * 
 */
@Slf4j
@Stateless(name = "RuleFactoryImpl", description = "")
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class RuleFactoryImpl implements RuleFactoryService {

	/**
	 * 
	 * {@inheritDoc}
	 * 
	 */
	@Override 
	public RuleEngineImpl createNewEngineInstance(Class<?>... clazzBusinessRules) throws LATAMException {
		RuleEngineImpl ruleEngine = new RuleEngineImpl();
		try {
			for (Class<?> clazzRule : clazzBusinessRules) {
				BusinessRule businessRuleFollowing = (BusinessRule) LocalJNDI.lookupEJB(clazzRule);
				ruleEngine.addBusinessRule(businessRuleFollowing);			
			}
		} catch (NamingException e) {
			if(logger.isErrorEnabled()){
				logger.error(e.getMessage(), e);
			}
		}		
		return ruleEngine;
	}

}
