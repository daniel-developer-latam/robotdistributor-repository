package com.latam.pax.robotdistr.ejb.impl.mock;

import java.util.Set;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;
import javax.validation.ConstraintViolation;

import com.google.common.collect.Sets;
import com.latam.arq.commons.appconfig.properties.AppConfigInterceptor;
import com.latam.arq.commons.exceptions.LATAMException;
import com.latam.pax.robotdistr.domain.OutputQueue;
import com.latam.pax.robotdistr.domain.PassengerNameRecord;
import com.latam.pax.robotdistr.ejb.impl.RuleEngineImpl;
import com.latam.pax.robotdistr.ejb.services.mock.RouteBusinessRuleMockService;
import com.latam.pax.robotdistr.enums.EventStatusEnum;
import com.latam.pax.robotdistr.event.BusinessEvent;
import com.latam.pax.robotdistr.event.RoutedBusinessEvent;

import lombok.extern.slf4j.Slf4j;


@Slf4j
@Stateless(name = "RouteBusinessRuleMockImpl", description = "")
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
@Interceptors({ AppConfigInterceptor.class })
public class RouteBusinessRuleMockImpl extends RuleEngineImpl implements RouteBusinessRuleMockService {

		
	@Override	
	public BusinessEvent execute(PassengerNameRecord passengerNameRecord) throws LATAMException {
		RoutedBusinessEvent routedBusinessEvent = null;
		OutputQueue outputQueues = null;	
		Set<ConstraintViolation<?>> constraintViolations = Sets.newHashSet();
		
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			logger.error(e.getMessage(), e);
		}
		
		
		routedBusinessEvent = 
				new RoutedBusinessEvent(passengerNameRecord, outputQueues, constraintViolations, EventStatusEnum.RULE_SUCCESS);
							
		return routedBusinessEvent;		
	}			
}
