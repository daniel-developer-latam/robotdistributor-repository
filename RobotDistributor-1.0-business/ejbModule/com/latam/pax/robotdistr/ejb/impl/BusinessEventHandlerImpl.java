package com.latam.pax.robotdistr.ejb.impl;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import com.latam.arq.commons.exceptions.LATAMException;
import com.latam.entities.v3_0.QueueType;
import com.latam.pax.robotdistr.domain.InputQueue;
import com.latam.pax.robotdistr.domain.OutputQueue;
import com.latam.pax.robotdistr.domain.Parameter;
import com.latam.pax.robotdistr.ejb.services.BusinessEventHandlerService;
import com.latam.pax.robotdistr.ejb.services.PassengerNameRecordService;
import com.latam.pax.robotdistr.ejb.services.RepositoryService;
import com.latam.pax.robotdistr.enums.EventStatusEnum;
import com.latam.pax.robotdistr.enums.ParametersEnum;
import com.latam.pax.robotdistr.event.BusinessEvent;
import com.latam.pax.robotdistr.event.CheckedDomBrazilBusinessEvent;
import com.latam.pax.robotdistr.event.DefaultRoutedBusinessEvent;
import com.latam.pax.robotdistr.event.GroupValidatedBusinessEvent;
import com.latam.pax.robotdistr.event.IntegrityValidatedBusinessEvent;
import com.latam.pax.robotdistr.event.MirrorRoutedBusinessEvent;
import com.latam.pax.robotdistr.event.RoutedBusinessEvent;
import com.latam.pax.robotdistr.event.SegmentsValidatedBusinessEvent;
import com.latam.pax.robotdistr.util.UtilCustomLogger;
import com.latam.pax.robotdistr.util.UtilLogger;


/**
 * EJB implementation for interface {@link RobotPriceQuoteEventHandlerEJBService}.
 * <br /><br />
 * 
 * Implementation of the event in CQRS pattern.
 * 
 * @author Everis
 * @author Daniel Carvajal (daniel.carvajal.soto@everis.com)
 * @version 1.0
 * 
 * @see <a href="http://cqrs.nu/Faq/commands-and-events">http://cqrs.nu/Faq/commands-and-events</a>
 *  
 */
@Stateless(name = "BusinessEventHandlerImpl", description = "")
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class BusinessEventHandlerImpl implements BusinessEventHandlerService {
		
	@EJB
	private RepositoryService repositoryService;
	
	@EJB
	private PassengerNameRecordService passengerNameRecordService;
	
	
	public void handle(BusinessEvent event) throws LATAMException {
		UtilCustomLogger.logHandlerInfo(BusinessEventHandlerImpl.class.getSimpleName(), event);
		
		if(event instanceof IntegrityValidatedBusinessEvent){
			IntegrityValidatedBusinessEvent integrityValidatedBusinessEvent = (IntegrityValidatedBusinessEvent) event;
			handle(integrityValidatedBusinessEvent);
			
		}else if(event instanceof GroupValidatedBusinessEvent){
			GroupValidatedBusinessEvent groupValidatedBusinessEvent = (GroupValidatedBusinessEvent) event;
			handle(groupValidatedBusinessEvent);
			
		}else if(event instanceof SegmentsValidatedBusinessEvent){
			SegmentsValidatedBusinessEvent segmentsValidatedBusinessEvent = (SegmentsValidatedBusinessEvent) event;
			handle(segmentsValidatedBusinessEvent);
			
		}else if(event instanceof CheckedDomBrazilBusinessEvent){
			CheckedDomBrazilBusinessEvent checkedDomBrazilBusinessEvent = (CheckedDomBrazilBusinessEvent) event;
			handle(checkedDomBrazilBusinessEvent);
		
		}else if(event instanceof RoutedBusinessEvent){
			RoutedBusinessEvent routedBusinessEvent = (RoutedBusinessEvent) event;
			handle(routedBusinessEvent);		
		
		}else if(event instanceof DefaultRoutedBusinessEvent){
			DefaultRoutedBusinessEvent defaultRoutedBusinessEvent = (DefaultRoutedBusinessEvent) event;
			handle(defaultRoutedBusinessEvent);
			
		} else if(event instanceof MirrorRoutedBusinessEvent){
			MirrorRoutedBusinessEvent mirrorRoutedBusinessEvent = (MirrorRoutedBusinessEvent) event;
			handle(mirrorRoutedBusinessEvent);
		}		
	}
	
	@Override
	public void handle(IntegrityValidatedBusinessEvent event) throws LATAMException {
		
		if(EventStatusEnum.RULE_SUCCESS.equals(event.getEventStatus())) {								
			UtilLogger.logInfo(event);							
		}
		if(EventStatusEnum.RULE_WARNING.equals(event.getEventStatus())) {								
			UtilLogger.logWarn(event);							
		}
		if(EventStatusEnum.RULE_EXCEPTION.equals(event.getEventStatus())){
			try {
				UtilLogger.logException(event);
							
				Parameter queueDefaultNumberParameter =  repositoryService.getParameterByKey(ParametersEnum.QUEUE_DEFAULT_DEFAULT_NUMBER.getKey());
				Parameter queueDefaultPseudoCityCodeParameter =  repositoryService.getParameterByKey(ParametersEnum.QUEUE_DEFAULT_DEFAULT_PSEUDOCITYCODE.getKey());
								
				QueueType queueType = new QueueType();
				queueType.setNumberQueue(queueDefaultNumberParameter.getValue());
				queueType.setPseudoCityCode(queueDefaultPseudoCityCodeParameter.getValue());
				// 
				passengerNameRecordService.sendPnrToQueue(event.getPassengerNameRecord(), queueType);						
				//
				DefaultRoutedBusinessEvent defaultRoutedBusinessEvent = 
						new DefaultRoutedBusinessEvent(event.getPassengerNameRecord(), queueType, event.getConstraintViolations(), EventStatusEnum.RULE_SUCCESS);
				handle(defaultRoutedBusinessEvent);
				
			} catch(LATAMException e){
				DefaultRoutedBusinessEvent defaultRoutedBusinessEvent = 
						new DefaultRoutedBusinessEvent(event.getPassengerNameRecord(), null, null, EventStatusEnum.RULE_ERROR, e);
				handle(defaultRoutedBusinessEvent);
				
				throw e;
			}
		}		
		if(EventStatusEnum.FAIL.equals(event.getEventStatus())){
			try {
				UtilLogger.logError(event);
							
				Parameter queueDefaultNumberParameter =  repositoryService.getParameterByKey(ParametersEnum.QUEUE_DEFAULT_DEFAULT_NUMBER.getKey());
				Parameter queueDefaultPseudoCityCodeParameter =  repositoryService.getParameterByKey(ParametersEnum.QUEUE_DEFAULT_DEFAULT_PSEUDOCITYCODE.getKey());
								
				QueueType queueType = new QueueType();
				queueType.setNumberQueue(queueDefaultNumberParameter.getValue());
				queueType.setPseudoCityCode(queueDefaultPseudoCityCodeParameter.getValue());
				// 
				passengerNameRecordService.sendPnrToQueue(event.getPassengerNameRecord(), queueType);						
				//
				DefaultRoutedBusinessEvent defaultRoutedBusinessEvent = 
						new DefaultRoutedBusinessEvent(event.getPassengerNameRecord(), queueType, event.getConstraintViolations(), EventStatusEnum.RULE_SUCCESS);
				handle(defaultRoutedBusinessEvent);
				
			} catch(LATAMException e){
				DefaultRoutedBusinessEvent defaultRoutedBusinessEvent = 
						new DefaultRoutedBusinessEvent(event.getPassengerNameRecord(), null, null, EventStatusEnum.RULE_ERROR, e);
				handle(defaultRoutedBusinessEvent);
				
				throw e;
			}
		}	
	}

	@Override
	public void handle(GroupValidatedBusinessEvent event) throws LATAMException {
		if(EventStatusEnum.RULE_SUCCESS.equals(event.getEventStatus())) {								
			UtilLogger.logInfo(event);							
		}
		if(EventStatusEnum.RULE_EXCEPTION.equals(event.getEventStatus())) {
			UtilLogger.logException(event);
		}									
	}
	
	@Override
	public void handle(SegmentsValidatedBusinessEvent event) throws LATAMException {
		if(EventStatusEnum.RULE_SUCCESS.equals(event.getEventStatus())) {								
			UtilLogger.logInfo(event);							
		}		
		if(EventStatusEnum.RULE_EXCEPTION.equals(event.getEventStatus())) {
			UtilLogger.logException(event);				
		}				
		if(EventStatusEnum.RULE_ERROR.equals(event.getEventStatus())) {
			UtilLogger.logError(event);				
		}				
	}	
	
	
	@Override
	public void handle(CheckedDomBrazilBusinessEvent event) throws LATAMException {
			
		if(EventStatusEnum.RULE_SUCCESS.equals(event.getEventStatus())) {								
			UtilLogger.logInfo(event);							
		}
		if(EventStatusEnum.RULE_EXCEPTION.equals(event.getEventStatus())) {
			try {			
				UtilLogger.logException(event);
				
				//send mirror queue
				InputQueue mirrorInputQueue = event.getPassengerNameRecord().getInputQueue();
				//
				QueueType queueType = new QueueType();
				queueType.setNumberQueue(mirrorInputQueue.getMirrorNumber());
				queueType.setPseudoCityCode(mirrorInputQueue.getMirrorPseudoCityCode());
				// 
				passengerNameRecordService.sendPnrToQueue(event.getPassengerNameRecord(), queueType);						
				//				
				
				MirrorRoutedBusinessEvent mirrorRoutedBusinessEvent = 
						new MirrorRoutedBusinessEvent(event.getPassengerNameRecord(), queueType, event.getConstraintViolations(), EventStatusEnum.RULE_SUCCESS);
				handle(mirrorRoutedBusinessEvent);			
			
			}catch(LATAMException e){
				MirrorRoutedBusinessEvent mirrorRoutedBusinessEvent = 
						new MirrorRoutedBusinessEvent(event.getPassengerNameRecord(), null, null, EventStatusEnum.RULE_ERROR, e);
				handle(mirrorRoutedBusinessEvent);		
				
				throw e;
			}
		}
		if(EventStatusEnum.RULE_ERROR.equals(event.getEventStatus())){
			UtilLogger.logError(event);			
		}
	}

	@Override
	public void handle(RoutedBusinessEvent event) throws LATAMException {
		if(EventStatusEnum.RULE_SUCCESS.equals(event.getEventStatus())) {
			OutputQueue outputQueue = event.getOutputQueue();
			QueueType queueType = new QueueType();
			queueType.setNumberQueue(outputQueue.getNumber());
			queueType.setPseudoCityCode(outputQueue.getPseudoCityCode());
			// 
			passengerNameRecordService.sendPnrToQueue(event.getPassengerNameRecord(), queueType);				
			UtilLogger.logInfo(event);						
		}
		if(EventStatusEnum.RULE_EXCEPTION.equals(event.getEventStatus())) {
			try {			
				UtilLogger.logException(event);
				
				OutputQueue defaultOutputQueue = event.getOutputQueue();
				QueueType queueType = new QueueType();
				queueType.setNumberQueue(defaultOutputQueue.getNumber());
				queueType.setPseudoCityCode(defaultOutputQueue.getPseudoCityCode());
				// 
				passengerNameRecordService.sendPnrToQueue(event.getPassengerNameRecord(), queueType);
						
				DefaultRoutedBusinessEvent defaultRoutedBusinessEvent = 
								new DefaultRoutedBusinessEvent(event.getPassengerNameRecord(), queueType, event.getConstraintViolations(), EventStatusEnum.RULE_SUCCESS);
				handle(defaultRoutedBusinessEvent);
					
			}catch (LATAMException e) {
				MirrorRoutedBusinessEvent mirrorRoutedBusinessEvent = 
						new MirrorRoutedBusinessEvent(event.getPassengerNameRecord(), null, null, EventStatusEnum.RULE_ERROR, e);
				handle(mirrorRoutedBusinessEvent);	
				
						
				throw e;
			}
		}
	}

	@Override
	public void handle(DefaultRoutedBusinessEvent event) throws LATAMException {
		if(EventStatusEnum.RULE_SUCCESS.equals(event.getEventStatus())) {
			UtilCustomLogger.logInfoWithDefaultQueue(event);
		}		
		if(EventStatusEnum.RULE_ERROR.equals(event.getEventStatus())) {
			UtilLogger.logError(event);
		}
	}

	@Override
	public void handle(MirrorRoutedBusinessEvent event) throws LATAMException {
		if(EventStatusEnum.RULE_SUCCESS.equals(event.getEventStatus())) {
			UtilLogger.logInfoWithDefaultQueue(event);
		}		
		if(EventStatusEnum.RULE_ERROR.equals(event.getEventStatus())) {
			UtilLogger.logError(event);
		}
	}
}
