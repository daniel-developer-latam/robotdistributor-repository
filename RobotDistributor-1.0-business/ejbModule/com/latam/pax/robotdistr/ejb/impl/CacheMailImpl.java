package com.latam.pax.robotdistr.ejb.impl;

import java.util.Arrays;

import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.interceptor.Interceptors;

import com.latam.arq.commons.appconfig.properties.AppConfigUtil;
import com.latam.arq.commons.cache.MethodCacheConfig;
import com.latam.arq.commons.cache.MethodCacheInterceptor;
import com.latam.arq.commons.exceptions.LATAMException;
import com.latam.arq.commons.logger.BusinessLoggingInterceptor;
import com.latam.arq.commons.mail.MailSenderEJBService;
import com.latam.arq.commons.mail.exception.MailSenderServiceException;
import com.latam.pax.robotdistr.domain.Parameter;
import com.latam.pax.robotdistr.ejb.services.CacheMailService;
import com.latam.pax.robotdistr.ejb.services.RepositoryService;
import com.latam.pax.robotdistr.enums.ParametersEnum;
import com.latam.pax.robotdistr.event.RobotDistributorGeneratedExceptionEvent;
import com.latam.pax.robotdistr.util.UtilConstant;
import com.latam.pax.robotdistr.util.UtilLogger;
import com.latam.pax.robotdistr.util.UtilTemplateLoader;

@Singleton(name="CacheMailImpl", description="")
public class CacheMailImpl implements CacheMailService{
	
	@EJB
	private MailSenderEJBService mailSenderEJBService;
	
	@EJB
	private RepositoryService repositoryService;
	
	@Override
	@Interceptors({BusinessLoggingInterceptor.class, MethodCacheInterceptor.class})
	@MethodCacheConfig(cacheName="sendMailCache", maxLifeSeconds=3600)
	public boolean sendMail(RobotDistributorGeneratedExceptionEvent event) throws MailSenderServiceException, LATAMException {
		boolean isSendMail = false;
		
		String mailTemplate = UtilTemplateLoader.getTemplate(event);
		
		Parameter parameterTo = repositoryService.getParameterByKey(ParametersEnum.MAIL_TO_DESTINATION.getKey());
		Parameter parameterCc = repositoryService.getParameterByKey(ParametersEnum.MAIL_CC_DESTINATION.getKey());
		//
		String[] toDestinations = parameterTo.getValue().trim().split(",");
		String[] ccDestinations = parameterCc.getValue().trim().split(",");
		
		String subject = String.format(UtilConstant.ROBOT_DISTRIBUTOR_MAIL_SUBJECT_FORMAT, AppConfigUtil.getAppName());
		
		UtilLogger.logInfo(
				String.format("SEND NOTIFICATION MAIL TO : [%s]  CC: [%s])", Arrays.toString(toDestinations), Arrays.toString(ccDestinations)));
		
		mailSenderEJBService.sendHtmlMessage(toDestinations, ccDestinations, subject, mailTemplate);
		isSendMail = true;
		
		return isSendMail;
				
	}
}
