package com.latam.pax.robotdistr.ejb.impl;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.xml.bind.JAXBException;

import com.latam.arq.commons.exceptions.LATAMException;
import com.latam.pax.robotdistr.ejb.services.CommandBusService;
import com.latam.pax.robotdistr.ejb.services.NotificationEventBusService;
import com.latam.pax.robotdistr.event.RobotDistributorGeneratedExceptionEvent;
import com.latam.pax.robotdistr.util.JAXBConverter;
import com.latam.pax.robotdistr.util.UtilCustomLogger;

import lombok.extern.slf4j.Slf4j;

/**
 * EJB implementation for interface {@link CommandBusService}.
 * <br /><br /> 
 *  
 * Publish messages in Queues.
 * 
 * @author Everis
 * @author Daniel Carvajal (daniel.carvajal.soto@everis.com)
 * @version 1.0
 *  
 *  @see <a href="http://www.enterpriseintegrationpatterns.com/patterns/messaging/ObserverJmsExample.html">
 * 				 http://www.enterpriseintegrationpatterns.com/patterns/messaging/ObserverJmsExample.html</a> 
 * 
 * 
 * TODO dejar bien documentado que distribuye a cola JMS y a cola HOST.
 *  
 */

@Slf4j
@Stateless(name = "NotificationEventBusImpl", description = "")
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class NotificationEventBusImpl implements NotificationEventBusService {
	
	@Resource(mappedName="jms/factory/robotDistributorFactory")	
	private ConnectionFactory connectionFactory;
	
	
	@Resource(mappedName="jms/queue/robotDistributor/notificationEventBusQueue")
	private Queue queue; 
	
	
	private Connection connection;
	
	
	/**
	 * 
	 * {@inheritDoc}
	 * 
	 */
	@Override
	public void publishEvent(RobotDistributorGeneratedExceptionEvent event) {		
		try {		
			openChannel();
			
			Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);			
			MessageProducer messageProducer = session.createProducer(queue);
			
			// marshal command			
			JAXBConverter<RobotDistributorGeneratedExceptionEvent> jaxbConverter = new JAXBConverter<>(RobotDistributorGeneratedExceptionEvent.class);
			String xmlCommand = jaxbConverter.marshal(event);
			
			// send message
			Message message = session.createTextMessage(xmlCommand);
			messageProducer.send(message);	
			
			UtilCustomLogger.logBusInfo(NotificationEventBusImpl.class.getSimpleName(), event);
			
		} catch (JMSException  | JAXBException | LATAMException e) {
			if(logger.isErrorEnabled()){
				logger.error(e.getMessage(), e);
			}		 
		}
	}

	
	@Override
	public void openChannel() throws LATAMException {
		try {
			// open resources
			connection = connectionFactory.createConnection();
			
		} catch (JMSException e) {			
			if(logger.isErrorEnabled()){
				logger.error(e.getMessage(), e);
			}			
		}		
	}

	@Override
	public void closeChannel() throws LATAMException {		
		try {
			// close resources
			connection.close();
			
		} catch (JMSException e) {			
			if(logger.isErrorEnabled()){
				logger.error(e.getMessage(), e);
			}			
		}				
	}
}
