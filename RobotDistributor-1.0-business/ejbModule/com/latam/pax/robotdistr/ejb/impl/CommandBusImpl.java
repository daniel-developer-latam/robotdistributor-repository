package com.latam.pax.robotdistr.ejb.impl;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.xml.bind.JAXBException;

import com.latam.arq.commons.exceptions.LATAMException;
import com.latam.pax.robotdistr.command.ToDistributeCommand;
import com.latam.pax.robotdistr.ejb.services.CommandBusService;
import com.latam.pax.robotdistr.ejb.services.NotificationEventBusService;
import com.latam.pax.robotdistr.enums.EventStatusEnum;
import com.latam.pax.robotdistr.event.RobotDistributorGeneratedExceptionEvent;
import com.latam.pax.robotdistr.util.JAXBConverter;
import com.latam.pax.robotdistr.util.UtilCustomLogger;

import lombok.extern.slf4j.Slf4j;

/**
 * EJB implementation for interface {@link CommandBusService}.
 * <br /><br /> 
 *  
 * Publish messages in Queues.
 * 
 * @author Everis
 * @author Daniel Carvajal (daniel.carvajal.soto@everis.com)
 * @version 1.0
 *  
 *  @see <a href="http://www.enterpriseintegrationpatterns.com/patterns/messaging/ObserverJmsExample.html">
 * 				 http://www.enterpriseintegrationpatterns.com/patterns/messaging/ObserverJmsExample.html</a> 
 * 
 * 
 * TODO dejar bien documentado que distribuye a cola JMS y a cola HOST.
 *  
 */

@Slf4j
@Stateless(name = "MessageProducerImpl", description = "")
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class CommandBusImpl implements CommandBusService {
	
	@Resource(mappedName="jms/factory/robotDistributorFactory")	
	private ConnectionFactory connectionFactory;
	
	
	@Resource(mappedName="jms/queue/robotDistributor/commandBusQueue")
	private Queue queue; 
	
	@EJB
	private NotificationEventBusService notificationEventBusService;
	
	
	private Connection connection;
	
	
	/**
	 * 
	 * {@inheritDoc}
	 * 
	 */
	@Override
	public void sendCommandToDistribute(ToDistributeCommand toDistributeCommand)
			throws LATAMException {		
		try {
			openChannel();
			
			Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);			
			MessageProducer messageProducer = session.createProducer(queue);
			
			// marshal command			
			JAXBConverter<ToDistributeCommand> jaxbConverter = new JAXBConverter<>(ToDistributeCommand.class);
			String xmlCommand = jaxbConverter.marshal(toDistributeCommand);
			
			// send message
			Message message = session.createTextMessage(xmlCommand);
			messageProducer.send(message);	
			
			UtilCustomLogger.logBusInfo(CommandBusImpl.class.getSimpleName(), toDistributeCommand);
					
			
		} catch (JMSException  | JAXBException e) {
			if(logger.isErrorEnabled()){
				logger.error(e.getMessage(), e);
			}
			RobotDistributorGeneratedExceptionEvent event =
					new RobotDistributorGeneratedExceptionEvent(
							String.valueOf(EventStatusEnum.FAIL.getCode()), e.getClass().getSimpleName(), e.getMessage() , EventStatusEnum.FAIL);		
			notificationEventBusService.publishEvent(event);	
		}	
	}

	
	@Override
	public void openChannel() throws LATAMException {
		try {
			// open resources
			connection = connectionFactory.createConnection();
			
		} catch (JMSException e) {			
			if(logger.isErrorEnabled()){
				logger.error(e.getMessage(), e);
			}			
		}		
	}

	@Override
	public void closeChannel() throws LATAMException {		
		try {
			// close resources
			connection.close();
			
		} catch (JMSException e) {			
			if(logger.isErrorEnabled()){
				logger.error(e.getMessage(), e);
			}			
		}				
	}
}
