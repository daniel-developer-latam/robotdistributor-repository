package com.latam.pax.robotdistr.ejb.impl;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;

import org.apache.commons.collections4.CollectionUtils;

import com.google.common.collect.Lists;
import com.latam.arq.commons.appconfig.properties.AppConfigInterceptor;
import com.latam.arq.commons.cache.MethodCacheConfig;
import com.latam.arq.commons.cache.MethodCacheInterceptor;
import com.latam.arq.commons.exceptions.LATAMException;
import com.latam.arq.commons.exceptions.LATAMExceptionService;
import com.latam.arq.commons.ws.clients.WSDisplayAirportClient;
import com.latam.pax.robotdistr.domain.FlightSegment;
import com.latam.pax.robotdistr.domain.Parameter;
import com.latam.pax.robotdistr.domain.PassengerNameRecord;
import com.latam.pax.robotdistr.ejb.services.FlightSegmentService;
import com.latam.pax.robotdistr.ejb.services.RepositoryService;
import com.latam.pax.robotdistr.enums.ParametersEnum;
import com.latam.pax.robotdistr.util.UtilValidator;
import com.latam.pax.robotdistr.util.UtilWsConverter;
import com.latam.ws.v3_0.DisplayAirportRQ;
import com.latam.ws.v3_0.DisplayAirportRS;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Stateless(name = "FlightSegmentImpl", description = "")
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
@Interceptors({ AppConfigInterceptor.class })
public class FlightSegmentImpl implements FlightSegmentService {

	@EJB
	private RepositoryService repositoryService;
	
	@EJB
	private LATAMExceptionService latamExceptionFactory;
	

	private final static String COUNTRY_CITY_CODE = "BR";
	
	/**
	 * 
	 * {@inheritDoc}
	 * 
	 * 
	 */
	@Override
	public boolean containsFullBrSegmentsFromPnr(PassengerNameRecord passengerNameRecord) throws LATAMException {
		boolean containsValidSegments = false;
			if (UtilValidator.isNotEmpty(passengerNameRecord.getFlightSegments())) {
				for (FlightSegment flightSegment : passengerNameRecord.getFlightSegments()) {
					String countryCodeDepartureAirport = getCountryCodeByAirportCode(flightSegment.getDepartureAirport());
					String countryCodeArrivalAirport = getCountryCodeByAirportCode(flightSegment.getArrivalAirport());
					if(COUNTRY_CITY_CODE.equalsIgnoreCase(countryCodeDepartureAirport) && 
							COUNTRY_CITY_CODE.equalsIgnoreCase(countryCodeArrivalAirport)){
						containsValidSegments = true;						
					}else{	
						containsValidSegments = false;
						break;
					}
				} // end for
			}
			return containsValidSegments;
		}	
	
	
	/**
	 * 
	 * 
	 * {@inheritDoc}
	 * 
	 * 
	 */	
	@Interceptors({MethodCacheInterceptor.class})
	@MethodCacheConfig(cacheName = "getCountryCodeByAirportCode", maxLifeSeconds = 86400000) 
	protected String getCountryCodeByAirportCode(String airportCode) throws LATAMException{
		String countryIsoCode = null;
		//
		DisplayAirportRS serviceResponse = null;
		DisplayAirportRQ serviceRequest = new DisplayAirportRQ();
		//
		serviceRequest.setAirportCode(airportCode);	
		serviceRequest.setIsoCountryCode(""); // default value		
		
		// send and response ws
		serviceResponse = WSDisplayAirportClient.call(serviceRequest, latamExceptionFactory);
				
		countryIsoCode = UtilWsConverter.toCountryISOCode(serviceResponse);
		
		return countryIsoCode;
	}
	
	
	

	@Override
	public List<FlightSegment> getHoldingFlightSegmentFromPnr(PassengerNameRecord passengerNameRecord)
			throws LATAMException {
		List<FlightSegment> validFlightSegmentList = Lists.newArrayList();
		try {
			Parameter parameter = repositoryService.getParameterByKey(ParametersEnum.AIRLINE_CODES.getKey());
			List<String> airLinesCodes = Arrays.asList(parameter.getValue().trim().split(","));		
			if(UtilValidator.isNotEmpty(parameter)) {				
				validFlightSegmentList = 
					CollectionUtils.emptyIfNull(passengerNameRecord.getFlightSegments()).stream()			
						.filter(validFlightSegment -> {
							boolean isValidFlightSegment = false;		
							isValidFlightSegment = 
									airLinesCodes.stream()										
										.anyMatch(airlineCode -> airlineCode.trim().equals(validFlightSegment.getAirlineCode()));						
							return isValidFlightSegment;		
						})			
					.collect(Collectors.toList());
			}
		}catch(LATAMException e){
			if(logger.isErrorEnabled()){
				logger.error(e.getMessage(), e);
			}
		}
		return validFlightSegmentList;
	}

}
