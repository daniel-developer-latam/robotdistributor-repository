
package com.latam.pax.robotdistr.ejb.impl;

import java.util.List;
import java.util.Set;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.validation.ConstraintViolation;

import com.google.common.collect.Sets;
import com.latam.arq.commons.exceptions.LATAMException;
import com.latam.pax.robotdistr.domain.FlightSegment;
import com.latam.pax.robotdistr.domain.PassengerNameRecord;
import com.latam.pax.robotdistr.ejb.services.BusinessEventHandlerService;
import com.latam.pax.robotdistr.ejb.services.CheckDomBrazilBusinessRuleService;
import com.latam.pax.robotdistr.ejb.services.FlightSegmentService;
import com.latam.pax.robotdistr.ejb.services.RemarkService;
import com.latam.pax.robotdistr.ejb.services.RepositoryService;
import com.latam.pax.robotdistr.enums.EventStatusEnum;
import com.latam.pax.robotdistr.event.CheckedDomBrazilBusinessEvent;
import com.latam.pax.robotdistr.event.BusinessEvent;
import com.latam.pax.robotdistr.util.UtilValidator;
import com.latam.pax.robotdistr.validator.group.ContainsDomBrSegmentsGroup;

/**
 * EJB implementation for interface for {@link CheckDomBrazilBusinessRuleService}.
 * <br /><br />
 * 
 *  PassengerNameRecordSegmentsValidBusinessRule implementation
 *  
 * @author Everis
 * @author Daniel Carvajal (daniel.carvajal.soto@everis.com)
 * @version 1.0
 *  
 */
@Stateless(name = "CheckDomBrazilBusinessRuleImpl", description = "")
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class CheckDomBrazilBusinessRuleImpl extends RuleEngineImpl implements CheckDomBrazilBusinessRuleService {

	@EJB
	private FlightSegmentService flightSegmentService;
	
	@EJB
	private RemarkService remarkService;
	
	@EJB
	private RepositoryService repositoryService;
	
	@EJB
	private BusinessEventHandlerService businessEventHandlerService;
	
	
	/**
	 * 
	 * 
	 * {@inheritDoc}
	 * 
	 * 
	 */
	@Override	
	public BusinessEvent execute(PassengerNameRecord passengerNameRecord) throws LATAMException {
		CheckedDomBrazilBusinessEvent checkedDomBrazilBusinessEvent = null;
		Set<ConstraintViolation<?>> constraintViolations = Sets.newHashSet();		
		try {		
			// Pos Br logic					 
			boolean containsPosBrSpecialMark = remarkService.containsPosBrSpecialMarkFromPnr(passengerNameRecord);
			passengerNameRecord.setContainsPosBrSpecialMark(containsPosBrSpecialMark);
			
			// Full Br logic
			List<FlightSegment> flightSegments = flightSegmentService.getHoldingFlightSegmentFromPnr(passengerNameRecord);
			passengerNameRecord.setFlightSegments(flightSegments);
			boolean containsFullBrSegments = flightSegmentService.containsFullBrSegmentsFromPnr(passengerNameRecord);
			passengerNameRecord.setContainsFullBrSegments(containsFullBrSegments);
			
			// evaluate flag			
			constraintViolations.addAll(UtilValidator.validate(passengerNameRecord,	ContainsDomBrSegmentsGroup.class));			
			
			if(containsPosBrSpecialMark || containsFullBrSegments){
				checkedDomBrazilBusinessEvent = 
						new CheckedDomBrazilBusinessEvent(passengerNameRecord, constraintViolations, EventStatusEnum.RULE_SUCCESS);
			}else{
				checkedDomBrazilBusinessEvent = 
						new CheckedDomBrazilBusinessEvent(passengerNameRecord, constraintViolations, EventStatusEnum.RULE_EXCEPTION);
			}		
			
			// handle event
			businessEventHandlerService.handle(checkedDomBrazilBusinessEvent);
		}catch(LATAMException e){
			checkedDomBrazilBusinessEvent = 
					new CheckedDomBrazilBusinessEvent(passengerNameRecord, constraintViolations, EventStatusEnum.RULE_ERROR, e);
			// handle event
			businessEventHandlerService.handle(checkedDomBrazilBusinessEvent);
			
			throw e;
		}			
		return checkedDomBrazilBusinessEvent;
	}			
}
