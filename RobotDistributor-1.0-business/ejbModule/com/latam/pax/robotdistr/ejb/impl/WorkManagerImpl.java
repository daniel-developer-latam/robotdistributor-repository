package com.latam.pax.robotdistr.ejb.impl;

import java.util.List;
import java.util.Properties;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.enterprise.concurrent.ManagedThreadFactory;
import javax.interceptor.Interceptors;

import com.latam.arq.commons.appconfig.properties.AppConfig;
import com.latam.arq.commons.appconfig.properties.AppConfigInterceptor;
import com.latam.arq.commons.concurrent.LatamWorkManager;
import com.latam.arq.commons.exceptions.LATAMException;
import com.latam.pax.robotdistr.domain.InputQueue;
import com.latam.pax.robotdistr.ejb.services.NotificationEventBusService;
import com.latam.pax.robotdistr.ejb.services.RepositoryService;
import com.latam.pax.robotdistr.ejb.services.WorkManagerService;
import com.latam.pax.robotdistr.enums.EventStatusEnum;
import com.latam.pax.robotdistr.event.RobotDistributorGeneratedExceptionEvent;
import com.latam.pax.robotdistr.util.UtilConstant;
import com.latam.pax.robotdistr.util.UtilConverter;
import com.latam.pax.robotdistr.util.UtilLogger;
import com.latam.pax.robotdistr.util.UtilValidator;
import com.latam.pax.robotdistr.work.ToDistributeWork;
import com.latam.pax.robotdistr.work.ToDistributeWorkListener;

import lombok.extern.slf4j.Slf4j;

/**
 * EJB implementation  for intenface {@link RobotDis}
 * 
 * Contains the batch process price passenger name record 
 * 
 * 
 * @author Everis
 * @author Daniel Carvajal (daniel.carvajal.soto@everis.com)
 * @version 1.0
 *  
 */

@Slf4j
@Stateless(name = "WorkManagerImpl", description = "")
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
@Interceptors({ AppConfigInterceptor.class })
public class WorkManagerImpl implements WorkManagerService {
		
	@EJB
	private RepositoryService repositoryService;
	
	@EJB
	private NotificationEventBusService notificationEventBus;
	
	@AppConfig
	Properties properties;
	
	@Resource(mappedName = "mtrd/robotdistributor")
	private ManagedThreadFactory managedThreadFactory;
	
	/**
	 * 
	 * {@inheritDoc}
	 * 
	 */
	public void executeRobotDistributor() {			
		try{
			if(!ToDistributeWorkListener.existsWorkActive()){
				int corePoolSize =
						UtilConverter.toInteger(properties.getProperty(UtilConstant.ROBOT_DISTRIBUTOR_LATAMWORKMANAGER_CORE_POOL_SIZE));
				//
				LatamWorkManager<Integer> latamWorkManager = new LatamWorkManager<Integer>(corePoolSize, managedThreadFactory);		
				//
				List<InputQueue> inputQueues = repositoryService.getAllInputQueue();	
				if(UtilValidator.isNotEmpty(inputQueues)){
					for(InputQueue inputQueue : inputQueues) { // 0... n
						//
						ToDistributeWork toDistributeWork = new ToDistributeWork();
						toDistributeWork.setInputQueue(inputQueue);
					
						ToDistributeWorkListener toDistributeWorkListener = new ToDistributeWorkListener();
						toDistributeWorkListener.setInputQueue(inputQueue);
						//
						latamWorkManager.addWork(toDistributeWork, toDistributeWorkListener); 
					}//end for
					
					latamWorkManager.start();
					
				}else{
					UtilLogger.logWarn("Cannot get inputQueue from database.");
				}
			}else{
				UtilLogger.logWarn("Exists other timer process in working!!!");
			}
			
		}catch(LATAMException e){					
			RobotDistributorGeneratedExceptionEvent event =
						new RobotDistributorGeneratedExceptionEvent(
								String.valueOf(EventStatusEnum.FAIL.getCode()), e.getClass().getSimpleName(), e.getMessage(), EventStatusEnum.FAIL);
			notificationEventBus.publishEvent(event);	
			if(logger.isErrorEnabled()){
				logger.error(e.getMessage(), e);
			}
		}					
	}			
}
