package com.latam.pax.robotdistr.ejb.impl.mock;

import java.util.Set;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.validation.ConstraintViolation;

import com.google.common.collect.Sets;
import com.latam.arq.commons.exceptions.LATAMException;
import com.latam.pax.robotdistr.domain.PassengerNameRecord;
import com.latam.pax.robotdistr.ejb.impl.RuleEngineImpl;
import com.latam.pax.robotdistr.ejb.services.mock.GroupValidBusinessRuleMockService;
import com.latam.pax.robotdistr.enums.EventStatusEnum;
import com.latam.pax.robotdistr.event.BusinessEvent;
import com.latam.pax.robotdistr.event.GroupValidatedBusinessEvent;

import lombok.extern.slf4j.Slf4j;


@Slf4j
@Stateless(name = "GroupValidBusinessRuleMockImpl", description = "")
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class GroupValidBusinessRuleMockImpl extends RuleEngineImpl implements GroupValidBusinessRuleMockService {

	
	@Override
	public BusinessEvent execute(PassengerNameRecord passengerNameRecord) throws LATAMException {
		GroupValidatedBusinessEvent groupValidatedBusinessEvent = null;
		Set<ConstraintViolation<?>> constraintViolations = Sets.newHashSet();
		
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			logger.error(e.getMessage(), e);
		}
		
		groupValidatedBusinessEvent = new GroupValidatedBusinessEvent(passengerNameRecord, constraintViolations, EventStatusEnum.RULE_SUCCESS);			
			
		return groupValidatedBusinessEvent;		
	}
}
