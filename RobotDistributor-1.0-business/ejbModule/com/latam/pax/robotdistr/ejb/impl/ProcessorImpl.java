package com.latam.pax.robotdistr.ejb.impl;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import com.latam.arq.commons.exceptions.LATAMException;
import com.latam.pax.robotdistr.domain.PassengerNameRecord;
import com.latam.pax.robotdistr.ejb.services.CheckDomBrazilBusinessRuleService;
import com.latam.pax.robotdistr.ejb.services.GroupValidBusinessRuleService;
import com.latam.pax.robotdistr.ejb.services.IntegrityBusinessRuleService;
import com.latam.pax.robotdistr.ejb.services.ProcessorService;
import com.latam.pax.robotdistr.ejb.services.RouteBusinessRuleService;
import com.latam.pax.robotdistr.ejb.services.RuleFactoryService;
import com.latam.pax.robotdistr.ejb.services.SegmentsValidBusinessRuleService;
import com.latam.pax.robotdistr.enums.RuleEngineStatusEnum;
import com.latam.pax.robotdistr.util.UtilCustomLogger;

/**
 * EJB implementation for interface {@link ProcessorService}.
 * <br /><br /> 
 *  
 * Load y maintains the {@link PassengerNameRecord} object into aggregate context, validate their
 * integrity, apply business logic and throw associates events.
 * 
 * @author Everis
 * @author Daniel Carvajal (daniel.carvajal.soto@everis.com)
 * @version 1.0
 *  
 *  @see <a href="http://cqrs.nu/Faq/aggregates">http://cqrs.nu/Faq/aggregates</a> 
 *  
 */
@Stateless(name = "ProcessorImpl", description = "")
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class ProcessorImpl implements ProcessorService {
	
	@EJB
	private RuleFactoryService ruleFactoryService;
	
	/**
	 * 
	 * {@inheritDoc}
	 * @throws Exception 
	 * 
	 */
	@Override
	public void process(PassengerNameRecord passengerNameRecord) throws LATAMException {	
		RuleEngineImpl ruleEngine =	
				ruleFactoryService.createNewEngineInstance(				
						IntegrityBusinessRuleService.class, // step 1
						GroupValidBusinessRuleService.class, // step 3 
						SegmentsValidBusinessRuleService.class, // step 4							
						CheckDomBrazilBusinessRuleService.class, // step 2
						RouteBusinessRuleService.class); // step 5
		ruleEngine.process(passengerNameRecord);
		
		if(RuleEngineStatusEnum.SUCCESS.equals(ruleEngine.getRuleStatus())){
			UtilCustomLogger.logRuleEngineInfo(
					passengerNameRecord.getUuid(), passengerNameRecord.getRecordLocator(), ruleEngine.getRuleStatus());
		}else{
			UtilCustomLogger.logRuleEngineInfo(
					 passengerNameRecord.getUuid(), passengerNameRecord.getRecordLocator(), ruleEngine.getRuleStatus());
		}
	}		
}
