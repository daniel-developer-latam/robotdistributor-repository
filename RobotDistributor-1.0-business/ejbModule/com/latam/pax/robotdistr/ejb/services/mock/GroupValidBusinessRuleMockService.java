package com.latam.pax.robotdistr.ejb.services.mock;

import javax.ejb.Local;

import com.latam.arq.commons.exceptions.LATAMException;
import com.latam.pax.robotdistr.domain.PassengerNameRecord;
import com.latam.pax.robotdistr.ejb.services.BusinessRule;
import com.latam.pax.robotdistr.event.BusinessEvent;


@Local
public interface GroupValidBusinessRuleMockService extends BusinessRule {
	
	BusinessEvent execute(PassengerNameRecord passengerNameRecord) throws LATAMException;

}
