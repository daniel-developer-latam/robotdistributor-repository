package com.latam.pax.robotdistr.ejb.services;

import javax.ejb.Local;

import com.latam.arq.commons.exceptions.LATAMException;
import com.latam.pax.robotdistr.event.RobotDistributorGeneratedExceptionEvent;


/**
 * EJB interface for {@link CommandBusImpl}.
 * 
 * @author Everis
 * @author Daniel Carvajal (daniel.carvajal.soto@everis.com)
 * @version 1.0
 *  
 */
@Local
public interface NotificationEventBusService {

	/**
	 * Send a {@link ToDistributeCommand} to a JMS Queue.
	 * <br /><br />
	 * <b>Default: </b>RobotDistributorQueue
	 *  
	 * 
	 * @param passengerNameRecordToDistributeCommand
	 * @throws LATAMException
	 */
	void publishEvent(RobotDistributorGeneratedExceptionEvent event);
	
	/**
	 * 
	 * @throws LATAMException
	 */
	void openChannel() throws LATAMException;
	
	/**
	 * 
	 * @throws LATAMException
	 */
	void closeChannel() throws LATAMException;
}
