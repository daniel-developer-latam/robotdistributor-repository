package com.latam.pax.robotdistr.ejb.services;

import javax.ejb.Local;

import com.latam.arq.commons.exceptions.LATAMException;
import com.latam.pax.robotdistr.ejb.impl.BusinessEventHandlerImpl;
import com.latam.pax.robotdistr.event.CheckedDomBrazilBusinessEvent;
import com.latam.pax.robotdistr.event.DefaultRoutedBusinessEvent;
import com.latam.pax.robotdistr.event.BusinessEvent;
import com.latam.pax.robotdistr.event.GroupValidatedBusinessEvent;
import com.latam.pax.robotdistr.event.IntegrityValidatedBusinessEvent;
import com.latam.pax.robotdistr.event.MirrorRoutedBusinessEvent;
import com.latam.pax.robotdistr.event.RoutedBusinessEvent;
import com.latam.pax.robotdistr.event.SegmentsValidatedBusinessEvent;

/**
 * EJB interface for {@link BusinessEventHandlerImpl}.
 * 
 * @author Everis
 * @author Daniel Carvajal (daniel.carvajal.soto@everis.com)
 * @version 1.0
 *  
 */
@Local
public interface BusinessEventHandlerService {
	
	
	public void handle(BusinessEvent event) throws LATAMException;
		
	
	/**
	 * 
	 * handle and processes {@link IntegrityValidatedBusinessEvent}
	 * 
	 * 
	 * @param event
	 */
	void handle(IntegrityValidatedBusinessEvent event) throws LATAMException;
	
	/**
	 * 
	 * handle and processes {@link IntegrityValidatedBusinessEvent}
	 * 
	 * 
	 * @param event
	 */
	void handle(SegmentsValidatedBusinessEvent event) throws LATAMException;
	
	
	/**
	 * 
	 * handle and processes {@link GroupValidatedBusinessEvent}
	 * 
	 * 
	 * @param event
	 */	
	void handle(GroupValidatedBusinessEvent event) throws LATAMException;
	
	/**
	 * 
	 * handle and processes {@link CheckedDomBrazilBusinessEvent}
	 * 
	 * 
	 * @param event
	 */	
	void handle(CheckedDomBrazilBusinessEvent event) throws LATAMException;
	
	/**
	 * 
	 * handle and processes {@link RoutedBusinessEvent}
	 * 
	 * 
	 * @param event
	 */	
	void handle(RoutedBusinessEvent event) throws LATAMException;
	
	
	/**
	 * 
	 * handle and processes {@link DefaultRoutedBusinessEvent}
	 * 
	 * 
	 * @param event
	 */	
	void handle(DefaultRoutedBusinessEvent event) throws LATAMException;
	
	/**
	 * 
	 * handle and processes {@link DefaultRoutedBusinessEvent}
	 * 
	 * 
	 * @param event
	 */	
	void handle(MirrorRoutedBusinessEvent event) throws LATAMException;
}
