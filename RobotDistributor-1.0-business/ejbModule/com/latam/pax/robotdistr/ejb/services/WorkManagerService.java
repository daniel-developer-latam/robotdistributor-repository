package com.latam.pax.robotdistr.ejb.services;

import javax.ejb.Local;

/**
 * EJB local interface for {@link RobotPriceQuoteProcessorEJBImpl}
 * 
 * @author Everis
 * @author Daniel Carvajal (daniel.carvajal.soto@everis.com)
 * @version 1.0
 *  
 */
@Local
public interface WorkManagerService {

	/**
	 * Execute the Robot Distributor bussiness process
	 * 
	 */
	void executeRobotDistributor();
	
}
