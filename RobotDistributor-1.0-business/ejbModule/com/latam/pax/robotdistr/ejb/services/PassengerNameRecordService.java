package com.latam.pax.robotdistr.ejb.services;

import java.util.List;

import javax.ejb.Local;

import com.latam.arq.commons.exceptions.LATAMException;
import com.latam.arq.commons.ws.clients.WSAddPNRToHostQueueClient;
import com.latam.arq.commons.ws.clients.WSCountMessagesInQueueClient;
import com.latam.arq.commons.ws.clients.WSDeleteMessagesInQueueByRangeClient;
import com.latam.arq.commons.ws.clients.WSDisplayMessagesInQueueByRangeClient;
import com.latam.arq.commons.ws.clients.WSDisplayPnrByRecordLocatorClient;
import com.latam.entities.v3_0.QueueType;
import com.latam.pax.robotdistr.command.ToDistributeCommand;
import com.latam.pax.robotdistr.domain.InputQueue;
import com.latam.pax.robotdistr.domain.PassengerNameRecord;
import com.latam.pax.robotdistr.ejb.impl.PassengerNameRecordImpl;

/**
 * EJB interface for {@link PassengerNameRecordImpl}.
 * 
 * @author Everis
 * @author Daniel Carvajal (daniel.carvajal.soto@everis.com)
 * @version 1.0
 *  
 */
@Local
public interface PassengerNameRecordService {
	
	/**
	 *
	 * call {@link WSDisplayMessagesInQueueByRangeClient}. This get response from SABRE queue results and transform a recordLocator list.
	 * 
	 * @param queueType
	 * @param indexFrom
	 * @param indexTo
	 * @return record locators list
	 * @throws LATAMException
	 */
	List<String> getRecordLocatorsInQueueByRange(InputQueue inputQueue, Integer indexFrom, Integer indexTo) throws LATAMException;
	
	/**
	 * call {@link WSDeleteMessagesInQueueByRangeClient}. This delete passenger name records from SABRE queue by range.
	 * 
	 * @param queueType
	 * @param indexFrom
	 * @param indexTo
	 * @return
	 * @throws LATAMException
	 */
	boolean deletePnrsInQueueByRange(InputQueue inputQueue, Integer indexFrom, Integer indexTo) throws LATAMException;
	
	/**
	 * call {@link WSDisplayPnrByRecordLocatorClient}. This get info to passenger name record and transform to {@link ToDistributeCommand}
	 * 
	 * @param recordLocator
	 * @return {@link ToDistributeCommand}
	 * @throws LATAMException
	 */
	PassengerNameRecord getPnrByRecordLocator(String recordLocator) throws LATAMException;
	
	/**
	 * 
	 * call {@link WSAddPNRToHostQueueClient}. This add {@link PassengerNameRecord} to sabre queue.
	 * 
	 * @param passengerNameRecord
	 * @return true if ok else false
	 * @throws LATAMException
	 */
	boolean addPnrInQueue(PassengerNameRecord passengerNameRecord, QueueType queueType) throws LATAMException;	
		
	
	/**
	 * 
	 * call {@link WSCountMessagesInQueueClient}. This count amount of the message in the {@link QueueType}.
	 * 
	 * @param queueType
	 * @return count of the messages
	 * @throws LATAMException
	 */
	Integer countPnrsInQueue(InputQueue inputQueue) throws LATAMException;
	
	/**
	 * Send a {@link PassengerNameRecord} to SABRE queue.
	 * <br /><br />
	 * <b>Default: </b>HQD253
	 * 
	 *  
	 * @param passengerNameRecord
	 * @throws LATAMException
	 */
	void sendPnrToQueue(PassengerNameRecord passengerNameRecord, QueueType queueType) throws LATAMException;

	
		
}
