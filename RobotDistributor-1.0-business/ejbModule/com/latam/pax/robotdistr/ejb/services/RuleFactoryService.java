package com.latam.pax.robotdistr.ejb.services;

import javax.ejb.Local;

import com.latam.arq.commons.exceptions.LATAMException;
import com.latam.pax.robotdistr.ejb.impl.RuleEngineImpl;

/**
 * EJB interface for {@link RuleFactoryImpl}.
 * 
 * @author Everis
 * @author Daniel Carvajal (daniel.carvajal.soto@everis.com)
 * @version 1.0
 *  
 */
@Local
public interface RuleFactoryService {

	/**
	 * 
	 * Create a new rule engine instance with business rules list defined.   
	 * 
	 * 
	 * @param business rule to process
	 * @return rule engine
	 */
	RuleEngineImpl createNewEngineInstance(Class<?>... clazzes) throws LATAMException;
}
