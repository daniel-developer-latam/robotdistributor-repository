package com.latam.pax.robotdistr.ejb.services;

import javax.ejb.Local;

import com.latam.arq.commons.exceptions.LATAMException;
import com.latam.arq.commons.mail.exception.MailSenderServiceException;
import com.latam.pax.robotdistr.event.RobotDistributorGeneratedExceptionEvent;

@Local
public interface CacheMailService {
	
	boolean sendMail(RobotDistributorGeneratedExceptionEvent code)throws MailSenderServiceException, LATAMException;
	
}
