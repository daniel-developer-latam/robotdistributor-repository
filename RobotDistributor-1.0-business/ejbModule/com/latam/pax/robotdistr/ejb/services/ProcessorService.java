package com.latam.pax.robotdistr.ejb.services;

import javax.ejb.Local;

import com.latam.arq.commons.exceptions.LATAMException;
import com.latam.pax.robotdistr.domain.PassengerNameRecord;

/**
 * EJB interface for {@link ProcessorImpl}.
 * 
 * @author Everis
 * @author Daniel Carvajal (daniel.carvajal.soto@everis.com)
 * @version 1.0
 *  
 */
@Local
public interface ProcessorService {		
	
	/**
	 * Load y maintains the {@link PassengerNameRecord} object into aggregate context, validate their
	 * integrity, apply business logic and throw associates events.
	 *	 
	 * 
	 * @param passengerNameRecord
	 */
	void process(PassengerNameRecord passengerNameRecord) throws LATAMException;
	
}
