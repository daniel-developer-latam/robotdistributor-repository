package com.latam.pax.robotdistr.ejb.services;

import javax.ejb.Local;

import com.latam.arq.commons.exceptions.LATAMException;
import com.latam.pax.robotdistr.domain.PassengerNameRecord;
import com.latam.pax.robotdistr.ejb.impl.SegmentsValidBusinessRuleImpl;
import com.latam.pax.robotdistr.event.BusinessEvent;

/**
 * EJB interface for {@link SegmentsValidBusinessRuleImpl}.
 * <br /><br />
 * 
 * PassengerNameRecordSegmentsValidBusinessRule definition
 * 
 * @author Everis
 * @author Daniel Carvajal (daniel.carvajal.soto@everis.com)
 * @version 1.0
 *  
 */
@Local
public interface SegmentsValidBusinessRuleService extends BusinessRule {
	
	/**
	 * 
	 * Validate if passenger name record contains valid segments.
	 * <br />
	 * Before throws {@link PassengerNameRecordSegmentsValidatedEvent} to {@link RobotPriceQuoteEventHandlerEJBImpl}
	 * 
	 * 
	 * @param passenger name record to validate
	 * @return event with status 
	 */	 
	BusinessEvent execute(PassengerNameRecord passengerNameRecord) throws LATAMException;

}
