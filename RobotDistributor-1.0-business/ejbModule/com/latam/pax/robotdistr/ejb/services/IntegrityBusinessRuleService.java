package com.latam.pax.robotdistr.ejb.services;

import javax.ejb.Local;

import com.latam.arq.commons.exceptions.LATAMException;
import com.latam.pax.robotdistr.domain.PassengerNameRecord;
import com.latam.pax.robotdistr.event.BusinessEvent;

/**
 * EJB interface for {@link IntegrityBusinessRuleMockImpl}.
 * <br /><br />
 * 
 * PassengerNameRecordIntegrityBusinessRule definition
 * 
 * @author Everis
 * @author Daniel Carvajal (daniel.carvajal.soto@everis.com)
 * @version 1.0
 *  
 */
@Local
public interface IntegrityBusinessRuleService extends BusinessRule {

	/**
	 * 
	 * Validate if all domain object into passenger name record contains values. 
	 * <br />
	 * Before throws {@link PassengerNameRecordIntegrityValidatedEvent} to {@link BusinessEventHandlerImpl}
	 * 
	 * @param passenger name record to validate
	 * @return event with status 
	 */
	BusinessEvent execute(PassengerNameRecord passengerNameRecord) throws LATAMException;
	
}
