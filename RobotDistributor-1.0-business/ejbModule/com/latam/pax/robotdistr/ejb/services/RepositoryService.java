package com.latam.pax.robotdistr.ejb.services;

import java.util.List;

import javax.ejb.Local;

import com.latam.arq.commons.exceptions.LATAMException;
import com.latam.pax.robotdistr.domain.Group;
import com.latam.pax.robotdistr.domain.InputQueue;
import com.latam.pax.robotdistr.domain.OfficeAccount;
import com.latam.pax.robotdistr.domain.OutputQueue;
import com.latam.pax.robotdistr.domain.Parameter;
import com.latam.pax.robotdistr.ejb.impl.RepositoryImpl;


/**
 * EJB local interface for {@link RepositoryImpl}
 * 
 * @author Everis
 * @author Daniel Carvajal (daniel.carvajal.soto@everis.com)
 * @version 1.0
 *  
 */
@Local
public interface RepositoryService {	
			
	Parameter getParameterByKey(String key) throws LATAMException;
	
	List<InputQueue> getAllInputQueue() throws LATAMException;	
	
	OutputQueue getOutputQueueByInputQueueAndTypeAndAOCAndGroup(InputQueue inputQueue, String businessType, OfficeAccount officeAccount, Group group)  throws LATAMException;

}
