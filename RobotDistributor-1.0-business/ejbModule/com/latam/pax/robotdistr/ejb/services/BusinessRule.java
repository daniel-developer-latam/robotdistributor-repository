package com.latam.pax.robotdistr.ejb.services;

import com.latam.arq.commons.exceptions.LATAMException;
import com.latam.pax.robotdistr.domain.PassengerNameRecord;
import com.latam.pax.robotdistr.event.BusinessEvent;

/**
 * Define commons method for de rule engine. This is extends from next rules interfaces:
 * <br /><br />
 *  
 * {@link IntegrityBusinessRuleService}
 * <br />
 * {@link GroupValidBusinessRuleService}
 * <br /> 
 * 
 * <br />
 * <br />
 * 
 * 
 * @author Everis
 * @author Daniel Carvajal (daniel.carvajal.soto@everis.com)
 * @version 1.0
 *  
 */
public interface BusinessRule {
	
	/**
	 * Execute 1 business rule for passengerNameRecord
	 * 
	 * @param passengerNameRecord
	 * @return event with status 
	 */
	BusinessEvent execute(PassengerNameRecord passengerNameRecord) throws LATAMException;	

}
