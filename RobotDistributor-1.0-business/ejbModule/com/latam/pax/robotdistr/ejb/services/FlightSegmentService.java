package com.latam.pax.robotdistr.ejb.services;

import java.util.List;

import javax.ejb.Local;

import com.latam.arq.commons.exceptions.LATAMException;
import com.latam.pax.robotdistr.domain.FlightSegment;
import com.latam.pax.robotdistr.domain.PassengerNameRecord;
import com.latam.pax.robotdistr.ejb.impl.FlightSegmentImpl;

/**
 * EJB interface for {@link FlightSegmentImpl}.
 * 
 * @author Everis
 * @author Daniel Carvajal (daniel.carvajal.soto@everis.com)
 * @version 1.0
 *  
 */
@Local
public interface FlightSegmentService {
	
	/**
	 * Validate if contains valid {@link FlightSegment} for passenger name record.
	 * 
	 * @param passengerNameRecord
	 * @return true if ok else false
	 * @throws LATAMException
	 */
	boolean containsFullBrSegmentsFromPnr(PassengerNameRecord passengerNameRecord) throws LATAMException;
	
	
	/**
	 * Get {@link FlightSegment} valid from passenger name record.
	 * 
	 * 
	 * @param passengerNameRecord
	 * @return {@link List} of {@link FlightSegment}
	 * @throws LATAMException
	 */
	List<FlightSegment> getHoldingFlightSegmentFromPnr(PassengerNameRecord passengerNameRecord) throws LATAMException;
	
}
