package com.latam.pax.robotdistr.ejb.services;

import javax.ejb.Local;

import com.latam.arq.commons.exceptions.LATAMException;
import com.latam.pax.robotdistr.domain.FlightSegment;
import com.latam.pax.robotdistr.domain.PassengerNameRecord;
import com.latam.pax.robotdistr.ejb.impl.RemarkImpl;

/**
 * EJB interface for {@link RemarkImpl}.
 * 
 * @author Everis
 * @author Daniel Carvajal (daniel.carvajal.soto@everis.com)
 * @version 1.0
 *  
 */
@Local
public interface RemarkService {
	
	/**
	 * Validate if contains valid {@link FlightSegment} for passenger name record.
	 * 
	 * @param passengerNameRecord
	 * @return true if ok else false
	 * @throws LATAMException
	 */
	boolean containsPosBrSpecialMarkFromPnr(PassengerNameRecord passengerNameRecord) throws LATAMException;
		
	
}
