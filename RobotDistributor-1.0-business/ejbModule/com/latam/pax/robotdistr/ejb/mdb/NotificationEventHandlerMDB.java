package com.latam.pax.robotdistr.ejb.mdb;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.EJB;
import javax.ejb.MessageDriven;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;
import javax.xml.bind.JAXBException;

import com.latam.arq.commons.exceptions.LATAMException;
import com.latam.arq.commons.mail.exception.MailSenderServiceException;
import com.latam.pax.robotdistr.ejb.services.CacheMailService;
import com.latam.pax.robotdistr.event.RobotDistributorGeneratedExceptionEvent;
import com.latam.pax.robotdistr.util.JAXBConverter;
import com.latam.pax.robotdistr.util.UtilCustomLogger;

import lombok.extern.slf4j.Slf4j;


/**
 * EJB implementation for interface {@link MessageListener}.
 * <br /><br />
 * 
 * Implementation of the "C" in CQRS pattern.  
 * 
 * @author Everis
 * @author Daniel Carvajal (daniel.carvajal.soto@everis.com)
 * @version 1.0
 * 
 * @see <a href="http://cqrs.nu/Faq/command-handlers">http://cqrs.nu/Faq/command-handlers</a>
 *  
 */
@Slf4j
@MessageDriven(mappedName="jms/queue/robotDistributor/notificationEventBusQueue", activationConfig =  {
		@ActivationConfigProperty(propertyName = "acknowledgeMode",
                                  propertyValue = "Auto-acknowledge"),
        @ActivationConfigProperty(propertyName = "destinationType",
                                  propertyValue = "javax.jms.Queue")
    })
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class NotificationEventHandlerMDB implements MessageListener {
	
	@EJB
	CacheMailService cacheMailService;
	
	@Override
	public void onMessage(Message message) {
		try {		
			if(!message.getJMSRedelivered() && (message instanceof TextMessage)) {
					// unmarshal message
					TextMessage textMessage = (TextMessage) message;
					String xmlCommand = textMessage.getText();

					JAXBConverter<RobotDistributorGeneratedExceptionEvent> jaxbConverter = new JAXBConverter<>(RobotDistributorGeneratedExceptionEvent.class);
					RobotDistributorGeneratedExceptionEvent event = jaxbConverter.unmarshal(xmlCommand);
					
					UtilCustomLogger.logHandlerInfo(NotificationEventHandlerMDB.class.getSimpleName(), event);
						
					handle(event);										
				}	
		} catch (JMSException | JAXBException | MailSenderServiceException | LATAMException e ) {
			if (logger.isErrorEnabled()) {
				logger.error(e.getMessage(), e);
			}
		} 
	}
	
	public void handle(RobotDistributorGeneratedExceptionEvent event) throws LATAMException, MailSenderServiceException {
		cacheMailService.sendMail(event);	
	}	
}
