package com.latam.pax.robotdistr.ejb.mdb;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.EJB;
import javax.ejb.MessageDriven;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;
import javax.xml.bind.JAXBException;

import com.latam.arq.commons.exceptions.LATAMException;
import com.latam.pax.robotdistr.command.ToDistributeCommand;
import com.latam.pax.robotdistr.domain.PassengerNameRecord;
import com.latam.pax.robotdistr.ejb.services.EventBusService;
import com.latam.pax.robotdistr.ejb.services.NotificationEventBusService;
import com.latam.pax.robotdistr.ejb.services.PassengerNameRecordService;
import com.latam.pax.robotdistr.ejb.services.ProcessorService;
import com.latam.pax.robotdistr.enums.EventStatusEnum;
import com.latam.pax.robotdistr.event.IntegrityValidatedBusinessEvent;
import com.latam.pax.robotdistr.event.RobotDistributorGeneratedExceptionEvent;
import com.latam.pax.robotdistr.util.JAXBConverter;
import com.latam.pax.robotdistr.util.UtilCustomLogger;
import com.latam.pax.robotdistr.util.UtilWsConverter;

import lombok.extern.slf4j.Slf4j;


/**
 * EJB implementation for interface {@link MessageListener}.
 * <br /><br />
 * 
 * Implementation of the "C" in CQRS pattern.  
 * 
 * @author Everis
 * @author Daniel Carvajal (daniel.carvajal.soto@everis.com)
 * @version 1.0
 * 
 * @see <a href="http://cqrs.nu/Faq/command-handlers">http://cqrs.nu/Faq/command-handlers</a>
 *  
 */
@Slf4j
@MessageDriven(mappedName="jms/queue/robotDistributor/commandBusQueue", activationConfig =  {
		@ActivationConfigProperty(propertyName = "acknowledgeMode",
                                  propertyValue = "Auto-acknowledge"),
        @ActivationConfigProperty(propertyName = "destinationType",
                                  propertyValue = "javax.jms.Queue")
    })
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class CommandHandlerMDB implements MessageListener {
			
	@EJB
	private ProcessorService processorService;
	
	@EJB
	private PassengerNameRecordService passengerNameRecordService;
	
	@EJB
	private NotificationEventBusService notificationEventBusService;
	
	@EJB
	private EventBusService eventBusService;
	
	@Override
	public void onMessage(Message message) {
		PassengerNameRecord passengerNameRecord = null;
		try {			
			if (message instanceof TextMessage) {
				// unmarshal message
				TextMessage textMessage = (TextMessage) message;
				String xmlCommand = textMessage.getText();

				JAXBConverter<ToDistributeCommand> jaxbConverter = new JAXBConverter<>(ToDistributeCommand.class);
				ToDistributeCommand command = jaxbConverter.unmarshal(xmlCommand);
								
				// to domain
				passengerNameRecord =			
						passengerNameRecordService.getPnrByRecordLocator(command.getRecordLocator());
				//uuid	
				passengerNameRecord.setUuid(command.getUuid());					
				passengerNameRecord.setInputQueue(UtilWsConverter.toInputQueue(command));
					
				UtilCustomLogger.logHandlerInfo(CommandHandlerMDB.class.getSimpleName(), passengerNameRecord);
																
				// process pnr business logic
				processorService.process(passengerNameRecord);				 
			}	
		
		} catch(LATAMException e1 ){
			// To notification Queue
			RobotDistributorGeneratedExceptionEvent notificationEvent =
					new RobotDistributorGeneratedExceptionEvent(
							String.valueOf(EventStatusEnum.FAIL.getCode()), e1.getClass().getSimpleName(), e1.getMessage(), EventStatusEnum.FAIL);		
			notificationEventBusService.publishEvent(notificationEvent);
			
			//To default default Queue
			IntegrityValidatedBusinessEvent integrityEvent =
					new IntegrityValidatedBusinessEvent(passengerNameRecord, null, EventStatusEnum.FAIL);		
			try {
				eventBusService.publishEvent(integrityEvent);
				
			} catch (LATAMException e2) {
				if (logger.isErrorEnabled()) {
					logger.error(e2.getMessage(), e2);
				}
			}				
				
			if (logger.isErrorEnabled()) {
				logger.error(e1.getMessage(), e1);
			}
		
		} catch (JMSException | JAXBException e) {
			RobotDistributorGeneratedExceptionEvent notificationEvent =
					new RobotDistributorGeneratedExceptionEvent(
							String.valueOf(EventStatusEnum.FAIL.getCode()), e.getClass().getSimpleName(), e.getMessage() , EventStatusEnum.FAIL);		
			notificationEventBusService.publishEvent(notificationEvent);
			
			if (logger.isErrorEnabled()) {
				logger.error(e.getMessage(), e);
			}
		}			
	}
}
