package com.latam.pax.robotdistr.work;

import java.util.List;
import java.util.NoSuchElementException;

import javax.naming.NamingException;

import com.latam.arq.commons.concurrent.LatamWork;
import com.latam.arq.commons.exceptions.LATAMException;
import com.latam.arq.commons.util.LocalJNDI;
import com.latam.pax.robotdistr.command.ToDistributeCommand;
import com.latam.pax.robotdistr.domain.InputQueue;
import com.latam.pax.robotdistr.ejb.services.CommandBusService;
import com.latam.pax.robotdistr.ejb.services.NotificationEventBusService;
import com.latam.pax.robotdistr.enums.EventStatusEnum;
import com.latam.pax.robotdistr.event.RobotDistributorGeneratedExceptionEvent;
import com.latam.pax.robotdistr.iterator.MessageConsumerIterator;
import com.latam.pax.robotdistr.util.UtilLogger;
import com.latam.pax.robotdistr.util.UtilValidator;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * EJB implementation  for intenface {@link RobotDis}
 * 
 * Contains the batch process price passenger name record 
 * 
 * 
 * @author Everis
 * @author Daniel Carvajal (daniel.carvajal.soto@everis.com)
 * @version 1.0
 *  
 */

@Slf4j
public class ToDistributeWork extends LatamWork<Integer> {	
			
	private CommandBusService commandBusService;
	
	private NotificationEventBusService notificationEventBus;
	
	@Getter
	@Setter
	private InputQueue inputQueue;

	
	public ToDistributeWork(){
		try {			
			this.commandBusService = LocalJNDI.lookupEJB(CommandBusService.class);
			this.notificationEventBus = LocalJNDI.lookupEJB(NotificationEventBusService.class);
		} catch (NamingException e) {
			if(logger.isErrorEnabled()){
				logger.error(e.getMessage(), e);
			}
		}		
	}
	
	@Override
	public Integer execute() throws LATAMException {
		Integer pnrSendCount = 0;
		try {
			MessageConsumerIterator messageConsumerIterator = new MessageConsumerIterator(inputQueue);
			
			if(messageConsumerIterator.isExistMessages()){
				while(messageConsumerIterator.hasNext()){				
					List<String> recordLocators = messageConsumerIterator.next();
				
					for(String recordLocator : recordLocators){
						if(UtilValidator.isNotEmpty(recordLocator)){
							// get pnr command
							ToDistributeCommand command = new ToDistributeCommand();
							command.setUuid(getWorkId());
							//
							command.setRecordLocator(recordLocator);
							// queue
							command.setNumber(inputQueue.getNumber());
							command.setPseudoCityCode(inputQueue.getPseudoCityCode());
							command.setMirrorNumber(inputQueue.getMirrorNumber());
							command.setMirrorPseudoCityCode(inputQueue.getMirrorPseudoCityCode());
										
							//send command to distribute
							commandBusService.sendCommandToDistribute(command);
				
							pnrSendCount++;
						}
					}
					messageConsumerIterator.remove();
				}
			}else{
				UtilLogger.logInfo(
						String.format("No exist Pnr in Queue [%s%s]", 
								messageConsumerIterator.getInputQueue().getPseudoCityCode(), messageConsumerIterator.getInputQueue().getNumber()));
		
			}						
		
		}catch (NoSuchElementException | IllegalStateException | LATAMException e) {			
			RobotDistributorGeneratedExceptionEvent event =
					new RobotDistributorGeneratedExceptionEvent(
							String.valueOf(EventStatusEnum.FAIL.getCode()), e.getClass().getSimpleName(), e.getMessage(), EventStatusEnum.FAIL);		
			notificationEventBus.publishEvent(event);
			
			if(logger.isErrorEnabled()){
				logger.error(e.getMessage(), e);
			}						
			
		}catch(NamingException e){			
			RobotDistributorGeneratedExceptionEvent event =
					new RobotDistributorGeneratedExceptionEvent(
							String.valueOf(EventStatusEnum.FAIL.getCode()), e.getClass().getSimpleName(), e.getMessage(), EventStatusEnum.FAIL);		
			notificationEventBus.publishEvent(event);
			
			if(logger.isErrorEnabled()){
				logger.error(e.getMessage(), e);
			}						
		}		
		return pnrSendCount;	
	}			
}
