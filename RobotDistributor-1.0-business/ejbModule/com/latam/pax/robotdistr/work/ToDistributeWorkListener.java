package com.latam.pax.robotdistr.work;

import java.util.concurrent.atomic.AtomicInteger;

import com.latam.arq.commons.concurrent.LatamWorkEvent;
import com.latam.arq.commons.concurrent.LatamWorkListener;
import com.latam.pax.robotdistr.domain.InputQueue;
import com.latam.pax.robotdistr.enums.WorkStatusEnum;
import com.latam.pax.robotdistr.util.UtilCustomLogger;

import lombok.Getter;
import lombok.Setter;

/**
 * EJB implementation  for intenface {@link RobotDis}
 * 
 * Contains the batch process price passenger name record 
 * 
 * 
 * @author Everis
 * @author Daniel Carvajal (daniel.carvajal.soto@everis.com)
 * @version 1.0
 *  
 */

public class ToDistributeWorkListener implements LatamWorkListener<Integer> {
	
	@Getter
	@Setter
	private InputQueue inputQueue;
	
	private static AtomicInteger workCount = new AtomicInteger();

	public void workStarted(LatamWorkEvent event) {
		workCount.getAndIncrement();
		UtilCustomLogger.logWorkStartedInfo(event.getWorkId(), inputQueue, WorkStatusEnum.STARTED.getMessage());		
	}

	@Override
	public void workCompleted(LatamWorkEvent event, Integer passengerNameRecordSendCount) {
		workCount.getAndDecrement();
		UtilCustomLogger.logWorkCompletedInfo(event.getWorkId(), inputQueue, WorkStatusEnum.COMPLETED.getMessage(), passengerNameRecordSendCount);
		
	}

	@Override
	public void workAborted(LatamWorkEvent event, Exception e) {
		workCount.getAndDecrement();
		UtilCustomLogger.logWorkAborted(e, event.getWorkId(), inputQueue, WorkStatusEnum.ABORTED.getMessage(), e.getMessage());		
	}
	
	public static boolean existsWorkActive(){
		boolean isWorkActived = false;
		if(workCount.get() > 0){
			isWorkActived = true;
		}else{
			workCount.set(0); // prevent safe effects
		}
		return isWorkActived;
	}
}
