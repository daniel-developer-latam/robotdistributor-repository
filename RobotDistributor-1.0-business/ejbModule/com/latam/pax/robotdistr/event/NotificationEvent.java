package com.latam.pax.robotdistr.event;

import java.util.Set;

import javax.validation.ConstraintViolation;

import com.latam.pax.robotdistr.enums.EventStatusEnum;


/**
 * Define base methods for the business events. This is implements from next event:
 * <br /><br />
 * 
 *  
 * @author Everis
 * @author Daniel Carvajal (daniel.carvajal.soto@everis.com)
 * @version 1.0
 *  
 */
public interface NotificationEvent {
	
	/**
	 * Get passenger name record
	 * 
	 * @return passenger name record
	 */
	String getCode();
			
	/**
	 * 
	 * get constraint violations 
	 * 
	 * @return {@link Set} of {@link ConstraintViolation}
	 */
	String getSimpleName();
	
	/**
	 * 
	 * get event status
	 * 
	 * @return event status
	 */
	String getMessage();
		
	/**
	 * 
	 * get event status
	 * 
	 * @return event status
	 */
	EventStatusEnum getEventStatus();
	
}
