package com.latam.pax.robotdistr.event;

import java.io.Serializable;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.latam.arq.commons.exceptions.LATAMException;
import com.latam.entities.v3_0.QueueType;
import com.latam.pax.robotdistr.domain.PassengerNameRecord;
import com.latam.pax.robotdistr.enums.EventStatusEnum;

import groovy.transform.ToString;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
*
* Represents the passenger name record fare validated business event
* 
*
* @author Everis
* @author Daniel Carvajal (daniel.carvajal.soto@everis.com)
* @version 1.0
*  
*/
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@AllArgsConstructor
@RequiredArgsConstructor
@ToString
public class DefaultRoutedBusinessEvent implements BusinessEvent, Serializable {
	private static final long serialVersionUID = 1L;
	
	@Getter
	@XmlElement
	private final PassengerNameRecord passengerNameRecord;	
	

	@Getter
	@XmlElement
	private final QueueType outputQueue;
	
	@Getter
	@XmlElement
	private final Set<ConstraintViolation<?>> constraintViolations;
		
	@Getter
	@XmlElement
	private final EventStatusEnum eventStatus;
	
	
	@Getter
	@XmlElement
	private LATAMException latamException;	
	
}
