package com.latam.pax.robotdistr.event;

import java.util.Set;

import javax.validation.ConstraintViolation;

import com.latam.arq.commons.exceptions.LATAMException;
import com.latam.pax.robotdistr.domain.PassengerNameRecord;
import com.latam.pax.robotdistr.enums.EventStatusEnum;


/**
 * Define base methods for the business events. This is implements from next event:
 * <br /><br />
 * 
 *  
 * @author Everis
 * @author Daniel Carvajal (daniel.carvajal.soto@everis.com)
 * @version 1.0
 *  
 */
public interface BusinessEvent {
	
	/**
	 * Get passenger name record
	 * 
	 * @return passenger name record
	 */
	PassengerNameRecord getPassengerNameRecord();
	
	/**
	 * 
	 * get constraint violations 
	 * 
	 * @return {@link Set} of {@link ConstraintViolation}
	 */
	Set<ConstraintViolation<?>> getConstraintViolations();
	
	/**
	 * 
	 * get event status
	 * 
	 * @return event status
	 */
	EventStatusEnum getEventStatus();
	
	/**
	 * Get {@link LATAMException}
	 * 
	 * @return {@link LATAMException}
	 */
	LATAMException getLatamException();
	
}
