package com.latam.pax.robotdistr.event;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import com.latam.pax.robotdistr.enums.EventStatusEnum;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
*
* Represents the passenger name record fare validated business event
* 
*
* @author Everis
* @author Daniel Carvajal (daniel.carvajal.soto@everis.com)
* @version 1.0
*  
*/
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class RobotDistributorGeneratedExceptionEvent implements NotificationEvent, Serializable {
	private static final long serialVersionUID = 1L;
		
	@Getter
	@XmlElement
	private String code;
	
	@Getter
	@XmlElement
	private String simpleName;
		
	@Getter
	@XmlElement
	private String message;
	
	@Getter
	@XmlTransient
	private EventStatusEnum eventStatus;
	
}
