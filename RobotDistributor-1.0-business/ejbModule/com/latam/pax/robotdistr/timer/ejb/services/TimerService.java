package com.latam.pax.robotdistr.timer.ejb.services;

import javax.ejb.Local;

import com.latam.arq.commons.timer.ejb.services.EJBTimerService;
import com.latam.pax.robotdistr.timer.ejb.impl.TimerImpl;

/**
 * EJB local interface for {@link TimerImpl}
 * 
 * @author Everis
 * @author Daniel Carvajal (daniel.carvajal.soto@everis.com)
 * @version 1.0
 *  
 */
@Local
public interface TimerService extends EJBTimerService {
	
}
