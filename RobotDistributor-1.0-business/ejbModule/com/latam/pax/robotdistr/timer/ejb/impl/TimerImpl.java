package com.latam.pax.robotdistr.timer.ejb.impl;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import com.latam.arq.commons.timer.ejb.impl.EJBTimerImpl;
import com.latam.pax.robotdistr.ejb.services.WorkManagerService;
import com.latam.pax.robotdistr.timer.ejb.services.TimerService;
import com.latam.pax.robotdistr.timer.mbeans.RobotDistributorMBean;


/**
 * EJB implementation for {@link TimerService}
 * 
 * Contains main method for the run {@link RobotDistributorMBean}
 * 
 * @author Everis
 * @author Daniel Carvajal (daniel.carvajal.soto@everis.com)
 * @version 1.0
 *  
 */
@Stateless(name = "RobotDistributorTimerEJB", description = "")
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class TimerImpl extends EJBTimerImpl implements TimerService {	
	
	 @EJB	
	 WorkManagerService workManager;
	
	/**
	 * 
	 * {@inheritDoc}
	 * 
	 */
	@Override
	public void executeTimerAction(){
		workManager.executeRobotDistributor();
	}
}
