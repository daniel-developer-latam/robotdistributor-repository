package com.latam.pax.robotdistr.timer.mbeans;

import com.latam.arq.commons.timer.abstracts.TimerController;
import com.latam.arq.commons.timer.annotations.TimerControllerProperties;
import com.latam.pax.robotdistr.timer.ejb.impl.TimerImpl;
import com.latam.pax.robotdistr.timer.ejb.services.TimerService;
import com.latam.arq.commons.mbeans.annotations.MBean;


/**
*
* Mbean for the {@link TimerImpl}
* 
*
* @author Everis
* @author Daniel Carvajal (daniel.carvajal.soto@everis.com)
* @version 1.0
*  
*/
@MBean( 
		name="RobotDistributorMBean",
		description="Mbean for controlling an EJB Timer"
	)
	@TimerControllerProperties (
	    delayInterval=60000, //1 min
		enableOnStart=true,
		inmediate=false 
	)
public class RobotDistributorMBean extends TimerController{

	public RobotDistributorMBean() {
		super(TimerService.class);
	}
}
