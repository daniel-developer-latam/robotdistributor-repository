package com.latam.pax.robotdistr.util;

import com.latam.pax.robotdistr.event.DefaultRoutedBusinessEvent;

import lombok.extern.slf4j.Slf4j;

/**
 * Util Logger
 *  
 * @author Everis
 * @author Daniel Carvajal (daniel.carvajal.soto@everis.com)
 * @version 1.0
 *  
 */
@Slf4j
public class UtilCustomLogger {
		
	public static void logWorkStartedInfo(Object... parameters) {
		if(logger.isInfoEnabled()){
			logger.info(
					String.format(UtilConstant.ROBOTDISTRIBUTOR_1_0_WORK_STARTED_TEMPLATE, parameters));
		}
	}	
	
	public static void logWorkAborted(Throwable e, Object... parameters) {
		if(logger.isErrorEnabled()){
			logger.error(
					String.format(UtilConstant.ROBOTDISTRIBUTOR_1_0_WORK_ABORTED_TEMPLATE, parameters), e);
		}
	}
	
	public static void logWorkCompletedInfo(Object... parameters) {
		if(logger.isInfoEnabled()){
			logger.info(
					String.format(UtilConstant.ROBOTDISTRIBUTOR_1_0_WORK_COMPLETED_TEMPLATE, parameters));
		}
	}	

	
	public static void logInfoGetRecordLocatorsBySegment(Object... parameters) {
		if(logger.isInfoEnabled()){
			logger.info(
					String.format(UtilConstant.ROBOTDISTRIBUTOR_1_0_BUSINESS_GET_RECORD_SEGMENT_DEBUG_TEMPLATE, parameters));
		}
	}
	
	public static void logInfoDeleteRecordLocatorsBySegment(Object... parameters) {
		if(logger.isInfoEnabled()){
			logger.info(
					String.format(UtilConstant.ROBOTDISTRIBUTOR_1_0_BUSINESS_DELETE_RECORD_SEGMENT_DEBUG_TEMPLATE, parameters));
		}
	}
	
	public static void logInfoWithDefaultQueue(DefaultRoutedBusinessEvent event) {
		if(logger.isInfoEnabled()){
			logger.info(
					String.format(UtilConstant.ROBOTDISTRIBUTOR_1_0_BUSINESS_EVENT_DEBUG_DEFAULT_QUEUE_MESSAGE_TEMPLATE, 
							event.getPassengerNameRecord().getUuid(), event.getPassengerNameRecord().getRecordLocator(), 
							event.getClass().getSimpleName(), event.getEventStatus().getMessage(), 
							event.getOutputQueue().getNumberQueue(), event.getOutputQueue().getPseudoCityCode(), event.getPassengerNameRecord()));
			
		}
	}	
	
	
	public static void logRuleEngineInfo(Object... parameters) {
		if(logger.isInfoEnabled()){
			logger.info(
					String.format(UtilConstant.ROBOTDISTRIBUTOR_1_0_BUSINESS_RULE_INFO_MESSAGE_TEMPLATE, parameters));
		}
	}
	
	public static void logBusInfo(Object... parameters) {
		if(logger.isInfoEnabled()){
			logger.info(
					String.format(UtilConstant.ROBOTDISTRIBUTOR_1_0_BUS_INFO_TEMPLATE, parameters));
		}
	}
	
	public static void logHandlerInfo(Object... parameters) {
		if(logger.isInfoEnabled()){
			logger.info(
					String.format(UtilConstant.ROBOTDISTRIBUTOR_1_0_HANDLER_INFO_TEMPLATE, parameters));
		}
	}	
}
