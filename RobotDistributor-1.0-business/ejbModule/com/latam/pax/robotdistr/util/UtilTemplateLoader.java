package com.latam.pax.robotdistr.util;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.lang3.text.StrSubstitutor;

import com.google.common.collect.Maps;
import com.latam.arq.commons.appconfig.properties.AppConfigException;
import com.latam.arq.commons.appconfig.properties.AppConfigUtil;
import com.latam.pax.robotdistr.enums.ParametersEnum;
import com.latam.pax.robotdistr.event.RobotDistributorGeneratedExceptionEvent;

import lombok.extern.slf4j.Slf4j;

/**
 * Util Converter
 * <br /><br />
 * Contains convertions for RobotPriceQuote
 *  
 * 
 * @author Everis
 * @author Daniel Carvajal (daniel.carvajal.soto@everis.com)
 * @version 1.0
 *  
 */
@Slf4j
public final class UtilTemplateLoader {	
	
	
	public static String getTemplate(RobotDistributorGeneratedExceptionEvent event) {
		String mailTemplate = "";
		try {
			File file = AppConfigUtil.getFileFromAppFolder("RobotDistributor-1.0-mail-template.html");			
				
			mailTemplate = 
					Files.lines(Paths.get(file.toURI()), StandardCharsets.UTF_8 ).collect(Collectors.joining());
			// fill the template
			Map<String, String> mapMailTemplate = Maps.newHashMap();			
			if(UtilValidator.isNotEmpty(mailTemplate)){
				mapMailTemplate.put(ParametersEnum.MAIL_ERROR_COMPONENT.getKey(), AppConfigUtil.getAppName());
								
				SimpleDateFormat simpleDate = new SimpleDateFormat("dd-MM-yyyy hh:mm", new Locale("es_CL"));
				mapMailTemplate.put(ParametersEnum.MAIL_ERROR_DATE.getKey(), simpleDate.format(new Date()));	
				
				mapMailTemplate.put(ParametersEnum.MAIL_ERROR_CODE.getKey(), event.getCode());
				mapMailTemplate.put(ParametersEnum.MAIL_ERROR_TYPE.getKey(), event.getSimpleName());
				mapMailTemplate.put(ParametersEnum.MAIL_ERROR_MESSAGE.getKey(), event.getMessage());
								
				mailTemplate = StrSubstitutor.replace(mailTemplate, mapMailTemplate);
			}	
		
		} catch(IOException | AppConfigException e) {
			if(logger.isErrorEnabled()){
				logger.error(e.getMessage(), e);
			}			
			mailTemplate = UtilConstant.ROBOT_DISTRIBUTOR_DEFAULT_BODY_FORMAT;
		}		
		return mailTemplate;
	}
}
