package com.latam.pax.robotdistr.util;

/**
 * Util Constants
 * <br /><br />
 * 
 * Contains constants for RobotDistributor
 *   
 * 
 * @author Everis
 * @author Daniel Carvajal (daniel.carvajal.soto@everis.com)
 * @version 1.0
 *  
 */
public final class UtilConstant {
	
	// ws request
	public static final String DISPLAYMESSAGESINQUEUEBYRANGE_3_0_1_REQUEST_INDEXFROM_KEY = 
			"DisplayMessagesInQueueByRange-3.0.1.request.indexFrom";
	public static final String DISPLAYMESSAGESINQUEUEBYRANGE_3_0_1_REQUEST_INDEXTO_KEY =
			"DisplayMessagesInQueueByRange-3.0.1.request.indexTo";
	public static final String DISPLAYMESSAGESINQUEUEBYRANGE_3_0_1_REQUEST_NUMBERQUEUE_KEY = 
			"DisplayMessagesInQueueByRange-3.0.1.request.numberQueue";
	public static final String DISPLAYMESSAGESINQUEUEBYRANGE_3_0_1_REQUEST_PSEUDOCITYCODE_KEY = 
			"DisplayMessagesInQueueByRange-3.0.1.request.pseudoCityCode";		
	
	public static final String DELETEMESSAGESINQUEUEBYRANGE_3_0_1_REQUEST_INDEXFROM_KEY = 
			"DeleteMessagesInQueueByRange-3.0.1.request.indexFrom";
	public static final String DELETEMESSAGESINQUEUEBYRANGE_3_0_1_REQUEST_INDEXTO_KEY =
			"DeleteMessagesInQueueByRange-3.0.1.request.indexTo";
	public static final String DELETEMESSAGESINQUEUEBYRANGE_3_0_1_REQUEST_NUMBERQUEUE_KEY = 
			"DeleteMessagesInQueueByRange-3.0.1.request.numberQueue";
	public static final String DELETEMESSAGESINQUEUEBYRANGE_3_0_1_REQUEST_PSEUDOCITYCODE_KEY = 
			"DeleteMessagesInQueueByRange-3.0.1.request.pseudoCityCode";		
	
	public static final String ADDPNRTOHOSTQUEUE_3_1_0_REQUEST_PREFATORYINSTRUCTIONCODE = 
			"AddPNRToHostQueue-3.1.0.request.prefatoryInstructionCode";
		
	// app settings	
	public static final String ROBOT_DISTRIBUTOR_LATAMWORKMANAGER_CORE_POOL_SIZE = 
			"LatamWorkManager.corePoolSize";
	
	// templates logger
	public static final String ROBOTDISTRIBUTOR_1_0_BUSINESS_EVENT_INFO_MESSAGE_TEMPLATE =
			"UUID[%s] | PNR[%s] | EVENT[%s] | STATUS[%s]";	
	public static final String ROBOTDISTRIBUTOR_1_0_BUSINESS_EVENT_WARN_MESSAGE_TEMPLATE =
			"UUID[%s] | PNR[%s] | EVENT[%s] | STATUS[%s] | VIOLATIONS[%s] |DETAIL[PNR[%s] ]";			
	public static final String ROBOTDISTRIBUTOR_1_0_BUSINESS_EVENT_ERROR_MESSAGE_TEMPLATE =
			"UUID[%s] | PNR[%s] | EVENT[%s] | STATUS[%s] | VIOLATIONS[%s] | EXCEPTION[%s]] | DETAIL[PNR[%s] ";
	public static final String ROBOTDISTRIBUTOR_1_0_BUSINESS_EVENT_EXCEPTION_MESSAGE_TEMPLATE =
			"UUID[%s] | PNR[%s] | EVENT[%s] | STATUS[%s] | VIOLATIONS[%s] ] | DETAIL[PNR[%s] ";
	public static final String ROBOTDISTRIBUTOR_1_0_BUSINESS_EVENT_DEBUG_MESSAGE_TEMPLATE =
			"UUID[%s] | PNR[%s] | EVENT[%s] | STATUS[%s] | DETAIL[PNR[%s]]";
	public static final String ROBOTDISTRIBUTOR_1_0_BUSINESS_EVENT_DEBUG_DEFAULT_QUEUE_MESSAGE_TEMPLATE =
			"UUID[%s] | PNR[%s] | EVENT[%s] | STATUS[%s] | SEND TO QUEUE[NUMBER[%s] | PCC[%s]] | DETAIL[PNR[%s]]";
	public static final String ROBOTDISTRIBUTOR_1_0_BUSINESS_RULE_INFO_MESSAGE_TEMPLATE =
			"UUID[%s] | PNR[%s] | STATUS[%s]";	
	
	public static final String ROBOTDISTRIBUTOR_1_0_BUSINESS_GET_RECORD_SEGMENT_DEBUG_TEMPLATE =
			"GET SEGMENT FROM QUEUE[%s] - INDEXFROM[%s] | INDEXTO[%s] | PNR COUNT[%s]";	
	public static final String ROBOTDISTRIBUTOR_1_0_BUSINESS_DELETE_RECORD_SEGMENT_DEBUG_TEMPLATE =
			"DELETE SEGMENT FROM QUEUE[%s] - INDEXFROM[%s] | INDEXTO[%s]";	
	
	public static final String ROBOTDISTRIBUTOR_1_0_BUSINESS_INFO_TEMPLATE = "INFO MESSAGE[%s]";	
	public static final String ROBOTDISTRIBUTOR_1_0_BUSINESS_WARNING_TEMPLATE =	"WARNING MESSAGE[%s]";	
	public static final String ROBOTDISTRIBUTOR_1_0_BUSINESS_DEBUG_TEMPLATE = "DEBUG MESSAGE[%s]";	
	public static final String ROBOTDISTRIBUTOR_1_0_BUSINESS_ERROR_TEMPLATE = "ERROR MESSAGE[%s]";
	
	public static final String ROBOTDISTRIBUTOR_1_0_BUS_INFO_TEMPLATE = "INFO HANDLER[%s] MESSAGE SENDED[%s]";
	public static final String ROBOTDISTRIBUTOR_1_0_HANDLER_INFO_TEMPLATE = "INFO BUS[%s] MESSAGE RECEIVED[%s]";
	
	public static final String ROBOTDISTRIBUTOR_1_0_WORK_STARTED_TEMPLATE = "WORK[%s] | INPUTQUEUE[%s]  | STATUS [%s]";
	public static final String ROBOTDISTRIBUTOR_1_0_WORK_ABORTED_TEMPLATE = "WORK[%s] | INPUTQUEUE[%s] | STATUS[%s] | ERROR[%s]";
	public static final String ROBOTDISTRIBUTOR_1_0_WORK_COMPLETED_TEMPLATE = "WORK[%s] | INPUTQUEUE[%s]  | STATUS [%s] | RECORDLOCATORS SENT TO QUEUE[%s]";
	
	// exceptions	
	public static final String PERSISTENCE_UTIL_EXCEPTION_ID = "PersistenceUtilException";
	public static final String ROBOT_DISTRIBUTOR_INPUTQUEUE_EXCEPTION_ID = "InputQueueException";
	public static final String ROBOT_DISTRIBUTOR_OUTPUTQUEUE_EXCEPTION_ID = "OutputQueueException";
	public static final String ROBOT_DISTRIBUTOR_PARAMETER_EXCEPTION_ID = "ParameterException";
	
	//Mail
	public static final String ROBOT_DISTRIBUTOR_MAIL_SUBJECT_FORMAT = "Notification error component [%s]";
	public static final String ROBOT_DISTRIBUTOR_DEFAULT_BODY_FORMAT = "<html><body>Se ha producido la siguiente un error en el componente RobotDistributor-1.0</body></html>";
		
}

