package com.latam.pax.robotdistr.util;

import java.math.BigInteger;

import org.apache.commons.lang3.StringUtils;


/**
 * Util Converter
 * <br /><br />
 * Contains convertions for RobotPriceQuote
 *  
 * 
 * @author Everis
 * @author Daniel Carvajal (daniel.carvajal.soto@everis.com)
 * @version 1.0
 *  
 */
public final class UtilConverter {
		
	/**
	 * 
	 * Convert from {@link String} to {@link BigInteger}
	 * 
	 * @param str
	 * @return {@link BigInteger}
	 */
	public static Integer toInteger(String str) {
		Integer integer = 0;
		if(UtilValidator.isNotEmpty(str) &&   StringUtils.isNumeric(str)){
				integer = Integer.valueOf(str);
			}		
		return integer;		
	}	
	
	
	/**
	 * 
	 * Convert from {@link String} to {@link BigInteger}
	 * 
	 * @param str
	 * @return {@link BigInteger}
	 */
	public static BigInteger toBigInteger(Integer integer) {
		BigInteger bigInteger = BigInteger.ZERO;
		if(UtilValidator.isNotEmpty(integer)){
				bigInteger = BigInteger.valueOf(integer.longValue());
			}		
		return bigInteger;		
	}	
	
	/**
	 * 
	 * Convert from {@link String} to {@link BigInteger}
	 * 
	 * @param str
	 * @return {@link BigInteger}
	 */
	public static BigInteger toBigInteger(String str) {
		BigInteger bigInteger = BigInteger.ZERO;
		if(UtilValidator.isNotEmpty(str) &&   StringUtils.isNumeric(str)){
				bigInteger = BigInteger.valueOf(Long.valueOf(str));
			}		
		return bigInteger;		
	}	
	
}
