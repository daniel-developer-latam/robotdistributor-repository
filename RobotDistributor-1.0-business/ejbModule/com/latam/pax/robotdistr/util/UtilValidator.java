package com.latam.pax.robotdistr.util;

import java.math.BigDecimal;
import java.util.List;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;

/**
 * Util Validator  
 * 
 * @author Everis
 * @author Daniel Carvajal (daniel.carvajal.soto@everis.com)
 * @version 1.0
 *  
 */

public final class UtilValidator {
	
	/**
	 * Validate if not empty {@link String}
	 * 
	 * @param str
	 * @return true if not empty else false
	 */
	public static boolean isNotEmpty(String str) {
		boolean isNotEmpty = true;
		if (str == null || "".equals(str)) {
			isNotEmpty = false;
		}
		return isNotEmpty;
	}
	
	/**
	 * Validate if not empty {@link Object}
	 * 
	 * @param o
	 * @returntrue if not empty else false
	 */
	public static boolean isNotEmpty(Object o) {
		boolean isNotEmpty = true;
		if (o == null) {
			isNotEmpty = false;
		}
		return isNotEmpty;
	}

	
	/**
	 * Validate if not empty {@link BigDecimal} 
	 * 
	 * @param bignumber
	 * @returntrue if not empty else false
	 */
	public static boolean isNotEmpty(BigDecimal bignumber) {
		boolean isNotEmpty = true;
		if (bignumber == null) {
			isNotEmpty = false;
		}
		return isNotEmpty;
	}

	/**
	 * Validate not empty {@link List}
	 * 
	 * @param bignumber
	 * @return true if not empty else false
	 */
	@SuppressWarnings("rawtypes")
	public static boolean isNotEmpty( List list) {
		boolean isNotEmpty = true;
		if (list == null || list.isEmpty()) {
			isNotEmpty = false;
		}
		return isNotEmpty;
	}

	/**
	 * Vaidate if not empty Array
	 * 
	 * @param bignumber
	 * @return true if not empty else false
	 */
	public static boolean isNotEmpty(Object[] list) {
		boolean isNotEmpty = true;
		if (list == null || list.length == 0) {
			isNotEmpty = false;
		}
		return isNotEmpty;
	}
	
	/**
	 * Validate T according JSR-303 spec.
	 * 
	 * @param <T>
	 * @return {@link Set} with {@link ConstraintViolation}
	 */
	public static <T> Set<ConstraintViolation<T>> validate(T object) {
		Set<ConstraintViolation<T>> constraintViolations = null;
		
		Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
		constraintViolations = validator.validate(object);

		return constraintViolations;
	}
	
	/**
	 * Validate T according JSR-303 spec. by groups
	 * 
	 * @param <T>
	 * @param groups of validations
	 * @return {@link Set} with {@link ConstraintViolation}
	 */
	public static <T> Set<ConstraintViolation<T>> validate(T object, Class<?>... groups) {
		Set<ConstraintViolation<T>> constraintViolations = null;
		
		Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
		constraintViolations = validator.validate(object, groups);

		return constraintViolations;
	}
	
	/**
	 * Vaidate if not empty Array
	 * 
	 * @param bignumber
	 * @return true if not empty else false
	 */
	public static boolean isValidRecordLocator(String recordLocator) {
		boolean isValidRecordLocator = false;
		if (isNotEmpty(recordLocator) && recordLocator.matches("[A-Z]{6}")) {
			isValidRecordLocator = true;
		}
		return isValidRecordLocator;
	}
	
}
