package com.latam.pax.robotdistr.util;

import java.math.BigInteger;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;

import com.google.common.collect.Lists;
import com.latam.entities.v3_0.QueueType;
import com.latam.pax.robotdistr.command.ToDistributeCommand;
import com.latam.pax.robotdistr.domain.FlightSegment;
import com.latam.pax.robotdistr.domain.Group;
import com.latam.pax.robotdistr.domain.InputQueue;
import com.latam.pax.robotdistr.domain.OfficeAccount;
import com.latam.pax.robotdistr.domain.PassengerNameRecord;
import com.latam.pax.robotdistr.domain.Remark;
import com.latam.ws.v3_0.CountMessagesInQueueRS;
import com.latam.ws.v3_0.DisplayAirportRS;
import com.latam.ws.v3_0.DisplayMessagesInQueueByRangeRS;
import com.latam.ws.v3_2.DisplayPnrByRecordLocatorRS;
import com.latam.ws.v3_2.DisplayPnrByRecordLocatorRS.ServiceResponse;
import com.latam.ws.v3_2.DisplayPnrByRecordLocatorRS.ServiceResponse.PnrDetail;
import com.latam.ws.v3_2.DisplayPnrByRecordLocatorRS.ServiceResponse.Remarks;
import com.latam.ws.v3_2.DisplayPnrByRecordLocatorRS.ServiceResponse.Segments;
import com.latam.ws.v3_2.DisplayPnrByRecordLocatorRS.ServiceResponse.Segments.Segment;


/**
 * Util Converter
 * <br /><br />
 * Contains convertions for RobotPriceQuote
 *  
 * 
 * @author Everis
 * @author Daniel Carvajal (daniel.carvajal.soto@everis.com)
 * @version 1.0
 *  
 */
public final class UtilWsConverter {
	

	/**
	 * 
	 * Convert from {@link DisplayPnrByRecordLocatorRS} to {@link ToDistributeCommand}
	 * 
	 * @param displayPnrByRecordLocatorRS
	 * @return {@link ToDistributeCommand}
	 */
	public static PassengerNameRecord toPassengerNameRecord(DisplayPnrByRecordLocatorRS displayPnrByRecordLocatorRS) {		 
		PassengerNameRecord passengerNameRecord = new PassengerNameRecord();
		
		ServiceResponse serviceResponse = displayPnrByRecordLocatorRS.getServiceResponse();
		if (UtilValidator.isNotEmpty(serviceResponse)) {					
			// recordLocator
			if (UtilValidator.isNotEmpty(serviceResponse.getPnrDetail()) && 
				UtilValidator.isNotEmpty(serviceResponse.getPnrDetail().getPnrInfo())){					
					passengerNameRecord.setRecordLocator(
							serviceResponse.getPnrDetail().getPnrInfo().getRecordLocator());
					
					OfficeAccount officeAccount = new OfficeAccount();								
					officeAccount.setAccountingCode(serviceResponse.getPnrDetail().getPnrInfo().getAccountingCode());				
					officeAccount.setAccountingCity(serviceResponse.getPnrDetail().getPnrInfo().getAccountingCity());
					officeAccount.setAaaPseudoCityCode(serviceResponse.getPnrDetail().getPnrInfo().getAAAPseudoCityCode());
					passengerNameRecord.setOfficeAccount(officeAccount);										
					
					// group
					Group group = toGroup(serviceResponse.getPnrDetail());
					passengerNameRecord.setGroup(group);					
			}
			// segments
			if (UtilValidator.isNotEmpty(serviceResponse.getSegments())) {			
				passengerNameRecord.setFlightSegments(toFlightSegments(serviceResponse.getSegments()));
			}		
			//remark
			if (UtilValidator.isNotEmpty(serviceResponse.getRemarks())) {			
				passengerNameRecord.setRemarks(toRemarks(serviceResponse.getRemarks()));
			}		
		}		
		return passengerNameRecord;
	}
	
	
	/**
	 * 
	 * Convert from {@link PnrDetail} to {@link PassengerNameRecord} group
	 * 
	 * @param pnrDetail
	 * @return {@link Group}
	 */
	public static Group toGroup(PnrDetail pnrDetail) {
	Group group = new Group();
		if(UtilValidator.isNotEmpty(pnrDetail.getPnrInfo().getGroup())){		
			group.setGroupName(pnrDetail.getPnrInfo().getGroup().getGroupName());
		}
	return group;		
	}
	
	/**
	 * 
	 * Convert from {@link List} {@link Segment} to {@link List} {@link FlightSegment}
	 * 
	 * @param segments
	 * @return {@link List} {@link FlightSegment}
	 */
	public static List<FlightSegment> toFlightSegments(Segments segments) {
		List<FlightSegment> flightSegments  = 
				CollectionUtils.emptyIfNull(segments.getSegment()).stream()
					.filter(segment -> 
							UtilValidator.isNotEmpty(segment) && UtilValidator.isNotEmpty(segment.getFlightSegment()))
						.map(segment -> {									
							FlightSegment flightSegment = new FlightSegment();
							flightSegment.setSegmentNumber(segment.getFlightSegment().getSegmentNumber());
							flightSegment.setSegmentStatus(segment.getFlightSegment().getSegmentStatus());
							flightSegment.setDepartureAirport(segment.getFlightSegment().getDepartureAirport());
							flightSegment.setArrivalAirport(segment.getFlightSegment().getArrivalAirport());
							if(UtilValidator.isNotEmpty(segment.getFlightSegment().getFlightDesignator())) {								
								flightSegment.setAirlineCode(segment.getFlightSegment().getFlightDesignator().getAirlineCode().getValue());
							}									
							return flightSegment;
						})
				.collect(Collectors.toList());
		return flightSegments;
	}	
	
	/**
	 * 
	 * Convert from {@link List} {@link Segment} to {@link List} {@link FlightSegment}
	 * 
	 * @param segments
	 * @return {@link List} {@link FlightSegment}
	 */
	public static List<Remark> toRemarks(Remarks remarks) {
		List<Remark> dremaks  = 
				CollectionUtils.emptyIfNull(remarks.getRemark()).stream()
					.filter(remark -> 
							UtilValidator.isNotEmpty(remark) && UtilValidator.isNotEmpty(remark.getText()))
						.map(remark -> {									
							Remark dremark = new Remark();
							dremark.setText(remark.getText());
							
							return dremark;
						})
				.collect(Collectors.toList());
		return dremaks;
	}	
	
	/**
	 * 
	 * Convert from {@link DisplayMessagesInQueueByRangeRS} to {@link List} {@link String}
	 * 
	 * @param displayMessagesInQueueByRangeRS
	 * @return {@link List} {@link String}
	 */
	public static List<String> toRecordLocators(DisplayMessagesInQueueByRangeRS displayMessagesInQueueByRangeRS) {
		List<String> recordLocators = Lists.newArrayList();
		//
		if(UtilValidator.isNotEmpty(displayMessagesInQueueByRangeRS) && UtilValidator.isNotEmpty(displayMessagesInQueueByRangeRS.getMessageList())){
			recordLocators = 
				CollectionUtils.emptyIfNull(
						displayMessagesInQueueByRangeRS.getMessageList().getMessage()).stream().map(message -> {						
						// get second element from the list
						Optional<String> recordLocator = 
								CollectionUtils.emptyIfNull(
										message.getText()).stream().limit(2) // recordLocator position1
									.filter(text -> UtilValidator.isValidRecordLocator(text))
							.reduce((first, second) -> second);// Extract last element from the list						
						return recordLocator.orElse("");						
					})
				.filter(recordLocator -> UtilValidator.isNotEmpty(recordLocator))				
			.collect(Collectors.toList());							
			}		
		return recordLocators;
	}
	
	/**
	 * 
	 * Convert from {@link CountMessagesInQueueRS} to {@link BigInteger}
	 * 
	 * @param countMessagesInQueueRS
	 * @return {@link BigInteger}
	 */
	public static Integer toAmmountMessage(CountMessagesInQueueRS countMessagesInQueueRS) {
		Integer ammountMessages = 
			CollectionUtils.emptyIfNull(
				countMessagesInQueueRS.getQueueList()).stream()
					.map(ammountMessage -> UtilConverter.toInteger(ammountMessage.getAmountMessages()))
				.findFirst().orElse(0);
		return ammountMessages;		
	}	
	
		
	/**
	 * 
	 * Convert from {@link DisplayAirportRS} to {@link String} country ISO code
	 * 
	 * @param displayAirportRS
	 * @return {@link BigInteger}
	 */
	public static String toCountryISOCode(DisplayAirportRS displayAirportRS) {
		String countryISOCode = 
			CollectionUtils.emptyIfNull(
					displayAirportRS.getAirports().getAirport()).stream()
						.filter(country -> UtilValidator.isNotEmpty(country.getCountry()))
					.map(country -> country.getCountry().getIsoCode())
				.findFirst().orElse("");
		return countryISOCode;		
	}	
	
	/**
	 * 
	 * Convert from {@link DisplayAirportRS} to {@link String} country ISO code
	 * 
	 * @param displayAirportRS
	 * @return {@link BigInteger}
	 */
	public static QueueType toQueueType(InputQueue inputQueue) {		 
		// Set queue
		QueueType queueType = new QueueType();		
		queueType.setNumberQueue(inputQueue.getNumber()); 
		queueType.setPseudoCityCode(inputQueue.getPseudoCityCode());

		return queueType;		
	}
	
	/**
	 * 
	 * Convert from {@link DisplayAirportRS} to {@link String} country ISO code
	 * 
	 * @param displayAirportRS
	 * @return {@link BigInteger}
	 */
	public static InputQueue toInputQueue(ToDistributeCommand command) {		 
		// Set queue mirror queue		
		InputQueue inputQueue = new InputQueue();				
		inputQueue.setNumber(command.getNumber());
		inputQueue.setPseudoCityCode(command.getPseudoCityCode());
		inputQueue.setMirrorNumber(command.getMirrorNumber());
		inputQueue.setMirrorPseudoCityCode(command.getMirrorPseudoCityCode());
		
		return inputQueue;		
	}	
}
