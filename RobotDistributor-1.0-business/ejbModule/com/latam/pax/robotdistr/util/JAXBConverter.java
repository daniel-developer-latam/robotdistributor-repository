package com.latam.pax.robotdistr.util;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.io.StringReader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

/**
 * Util Class for convert object to xml and viceverse using marshaller and
 * unmarshaller process of JAXB.
 * 
 * @param <T>
 *            Class Type for JAXBConverter
 * 
 * @author TCS
 * @author Marcelo Albornoz (marcelo.albornoz@tcs.com)
 * @version 1.0
 * @created 16-may-2016
 * 
 */
public final class JAXBConverter<T> {

	private Marshaller marshaller;
	private Unmarshaller unmarshaller;

	/**
	 * 
	 * @param clazz
	 * @throws JAXBException
	 */
	public JAXBConverter(final Class<T> clazz) throws JAXBException {

		final JAXBContext jaxbContext = JAXBContext.newInstance(clazz);

		createMarshaller(jaxbContext);
		createUnmarshaller(jaxbContext);
	}

	/**
	 * 
	 * @param jaxbContext
	 * @throws JAXBException
	 */
	private void createMarshaller(final JAXBContext jaxbContext) throws JAXBException {
		marshaller = jaxbContext.createMarshaller();
		marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
	}

	/**
	 * 
	 * @param jaxbContext
	 * @throws JAXBException
	 */
	private void createUnmarshaller(final JAXBContext jaxbContext) throws JAXBException {
		unmarshaller = jaxbContext.createUnmarshaller();
	}

	/**
	 * 
	 * @param obj
	 * @return
	 * @throws JAXBException
	 */
	public String marshal(final T obj) throws JAXBException {

		final OutputStream outputStream = new ByteArrayOutputStream();
		marshaller.marshal(obj, outputStream);

		return outputStream.toString().trim();
	}

	/**
	 * 
	 * @param xml
	 * @return
	 * @throws JAXBException
	 */
	@SuppressWarnings("unchecked")
	public T unmarshal(final String xml) throws JAXBException {
		if (xml == null || xml.isEmpty()) {
			return null;
		}
		return (T) unmarshaller.unmarshal(new StringReader(xml.trim()));
	}

}
