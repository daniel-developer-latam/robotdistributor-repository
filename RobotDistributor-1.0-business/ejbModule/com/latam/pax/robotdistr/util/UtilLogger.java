package com.latam.pax.robotdistr.util;

import com.latam.pax.robotdistr.event.BusinessEvent;
import com.latam.pax.robotdistr.event.MirrorRoutedBusinessEvent;

import lombok.extern.slf4j.Slf4j;

/**
 * Util Logger
 *  
 * @author Everis
 * @author Daniel Carvajal (daniel.carvajal.soto@everis.com)
 * @version 1.0
 *  
 */
@Slf4j
public class UtilLogger {
	
	
	public static void logInfo(String message) {
		if(logger.isInfoEnabled()){
			logger.info(
					String.format(UtilConstant.ROBOTDISTRIBUTOR_1_0_BUSINESS_INFO_TEMPLATE, message));
		}
	}
	
	public static void logWarn(String message) {
		if(logger.isWarnEnabled()){
			logger.warn(
					String.format(UtilConstant.ROBOTDISTRIBUTOR_1_0_BUSINESS_WARNING_TEMPLATE, message));
		}
	}
	
	public static void logError(String message) {
		if(logger.isErrorEnabled()){
			logger.error(
					String.format(UtilConstant.ROBOTDISTRIBUTOR_1_0_BUSINESS_ERROR_TEMPLATE, message));
		}
	}	
		
	public static void logDebug(String message) {
		if(logger.isDebugEnabled()){
			logger.debug(
					String.format(UtilConstant.ROBOTDISTRIBUTOR_1_0_BUSINESS_DEBUG_TEMPLATE, message));
		}
	}
	
	public static void logInfo(BusinessEvent event) {
		if(logger.isDebugEnabled()){
			logger.debug(String.format(
					UtilConstant.ROBOTDISTRIBUTOR_1_0_BUSINESS_EVENT_DEBUG_MESSAGE_TEMPLATE, event.getPassengerNameRecord().getUuid(),
					 event.getPassengerNameRecord().getRecordLocator(), event.getClass().getSimpleName(), event.getEventStatus().getMessage(), event.getPassengerNameRecord()));
		}
		if(logger.isInfoEnabled()){
			logger.info(String.format(
					UtilConstant.ROBOTDISTRIBUTOR_1_0_BUSINESS_EVENT_INFO_MESSAGE_TEMPLATE, event.getPassengerNameRecord().getUuid(),
					event.getPassengerNameRecord().getRecordLocator(), event.getClass().getSimpleName(), event.getEventStatus().getMessage()));
		}
	}
	
	/**
	 * 
	 * @param event
	 */
	public static void logWarn(BusinessEvent event) {
		if(logger.isWarnEnabled()){
			logger.info(String.format(
					UtilConstant.ROBOTDISTRIBUTOR_1_0_BUSINESS_EVENT_WARN_MESSAGE_TEMPLATE, event.getPassengerNameRecord().getUuid(),
					event.getPassengerNameRecord().getRecordLocator(), event.getClass().getSimpleName(), event.getEventStatus().getMessage(), event.getConstraintViolations(),  event.getPassengerNameRecord()));
		}	
	}
	
	
	/**
	 * 
	 * @param event
	 */
	public static void logError(BusinessEvent event) {
		if(logger.isErrorEnabled()){		
			if(UtilValidator.isNotEmpty(event.getLatamException())){
				logger.error(String.format(
						UtilConstant.ROBOTDISTRIBUTOR_1_0_BUSINESS_EVENT_ERROR_MESSAGE_TEMPLATE, event.getPassengerNameRecord().getUuid(),
						event.getPassengerNameRecord().getRecordLocator(), event.getClass().getSimpleName(), event.getEventStatus().getMessage(),
						event.getConstraintViolations(), event.getLatamException().getMessage(), event.getPassengerNameRecord()));
				// catch
				logger.error(event.getLatamException().getName(), event.getLatamException());
			}else{
				logger.error(String.format(
						UtilConstant.ROBOTDISTRIBUTOR_1_0_BUSINESS_EVENT_ERROR_MESSAGE_TEMPLATE, event.getPassengerNameRecord().getUuid(),
						event.getPassengerNameRecord().getRecordLocator(), event.getClass().getSimpleName(), event.getEventStatus().getMessage(),
						event.getConstraintViolations(),null, event.getPassengerNameRecord()));
			}
		}	
	}
	
	public static void logException(BusinessEvent event) {
		if(logger.isInfoEnabled()){		
				logger.info(String.format(
						UtilConstant.ROBOTDISTRIBUTOR_1_0_BUSINESS_EVENT_EXCEPTION_MESSAGE_TEMPLATE, event.getPassengerNameRecord().getUuid(),
						event.getPassengerNameRecord().getRecordLocator(), event.getClass().getSimpleName(), event.getEventStatus().getMessage(),
		event.getConstraintViolations(), event.getPassengerNameRecord()));				
		}	
	}
	
	public static void logInfoWithDefaultQueue(MirrorRoutedBusinessEvent event) {
		if(logger.isInfoEnabled()){
			logger.info(
					String.format(UtilConstant.ROBOTDISTRIBUTOR_1_0_BUSINESS_EVENT_DEBUG_DEFAULT_QUEUE_MESSAGE_TEMPLATE, 
							event.getPassengerNameRecord().getUuid(), event.getPassengerNameRecord().getRecordLocator(),
							event.getClass().getSimpleName(), event.getEventStatus().getMessage(), 
							event.getMirrorQueue().getNumberQueue(), event.getMirrorQueue().getPseudoCityCode(), event.getPassengerNameRecord()));
			
		}
	}
	
}
