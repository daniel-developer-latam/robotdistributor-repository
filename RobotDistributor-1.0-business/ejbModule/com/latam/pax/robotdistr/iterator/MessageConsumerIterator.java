package com.latam.pax.robotdistr.iterator;

import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

import javax.naming.NamingException;

import com.google.common.collect.Lists;
import com.latam.arq.commons.exceptions.LATAMException;
import com.latam.arq.commons.util.LocalJNDI;
import com.latam.pax.robotdistr.domain.InputQueue;
import com.latam.pax.robotdistr.domain.Parameter;
import com.latam.pax.robotdistr.ejb.impl.PassengerNameRecordImpl;
import com.latam.pax.robotdistr.ejb.services.PassengerNameRecordService;
import com.latam.pax.robotdistr.ejb.services.RepositoryService;
import com.latam.pax.robotdistr.enums.ParametersEnum;
import com.latam.pax.robotdistr.util.UtilConverter;
import com.latam.pax.robotdistr.util.UtilCustomLogger;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;


/**
 * EJB de implementacion for inteface {@link SelectiveMessageConsumerService}.
 * <br /><br />
 * Represents the SelectiveConsumer pattern.
 * <br />
 * Consume messages the SABRE Queues through  the call to webservices client encapsulataded in {@link PassengerNameRecordImpl}.  
 * 
 * @author Everis
 * @author Daniel Carvajal (daniel.carvajal.soto@everis.com)
 * @version 1.0
 * 
 * @see <a href="http://www.enterpriseintegrationpatterns.com/patterns/messaging/MessageSelector.html">
 * 				 http://www.enterpriseintegrationpatterns.com/patterns/messaging/MessageSelector.html</a> 
 *  
 */
@Slf4j
public class MessageConsumerIterator implements Iterator<List<String>> {
	
	private Integer length;
	
	private Integer indexFrom;
	
	private Integer indexTo;
	
	private Integer segmentSize;   	
	
	private Integer queueMessageCount;
	
	private RepositoryService repositoryService;
	
	private PassengerNameRecordService passengerNameRecordService;	
		
	@Getter
	@Setter
	private InputQueue inputQueue;
		 
		
	public MessageConsumerIterator(InputQueue inputQueue) throws LATAMException, NamingException {
		this.repositoryService = LocalJNDI.lookupEJB(RepositoryService.class);
		this.passengerNameRecordService = LocalJNDI.lookupEJB(PassengerNameRecordService.class);
			
		// index
		indexTo   = -1;
		indexFrom =  0;
						
		// get segment	
		Parameter parameter = repositoryService.getParameterByKey(ParametersEnum.SEGMENT_SIZE.getKey());		
		segmentSize = UtilConverter.toInteger(parameter.getValue());
			
		// queue size
		queueMessageCount  = passengerNameRecordService.countPnrsInQueue(inputQueue);
		length = queueMessageCount - 1;
			
		setInputQueue(inputQueue);		
	}
	
	
	public boolean isExistMessages(){
		boolean isExistsMessages = false;
		if(queueMessageCount > 0){
			isExistsMessages = true;
		}
		return isExistsMessages;
	}
	
	@Override
	public boolean hasNext() {
		boolean hasNext = false;
		if((queueMessageCount > 0)  && (indexTo < length)) {
				hasNext = true;			
			}		
		return hasNext;
	}


	@Override
	public List<String> next(){
		List<String> recordLocators = Lists.newArrayList();		
		try {
			if(queueMessageCount > 0 && indexTo < length) {
					indexFrom = Math.min(indexTo == -1 ? 0 : indexTo + 1, length);
					indexTo = Math.min((indexFrom + segmentSize) - 1, length);
					//get pnr
					recordLocators.addAll( 
							passengerNameRecordService.getRecordLocatorsInQueueByRange(inputQueue, 0, (indexTo - indexFrom)));	            
					UtilCustomLogger.logInfoGetRecordLocatorsBySegment(inputQueue , 0, (indexTo - indexFrom), recordLocators.size());					
			}
		}catch(LATAMException e){
			if(logger.isErrorEnabled()){
				logger.error(e.getMessage(), e);
			}			
			throw new NoSuchElementException(e.getMessage());
		}		
		return recordLocators;
	}	
	
	@Override	
	public void remove() {
		try {
			//delete pnr
			passengerNameRecordService.deletePnrsInQueueByRange(inputQueue, 0, (indexTo - indexFrom)); 
			UtilCustomLogger.logInfoDeleteRecordLocatorsBySegment(inputQueue, 0, (indexTo - indexFrom));
			
		} catch(LATAMException e){
			if(logger.isErrorEnabled()){
				logger.error(e.getMessage(), e);
			}	
			throw new IllegalStateException(e);
		}
	}
	
}
