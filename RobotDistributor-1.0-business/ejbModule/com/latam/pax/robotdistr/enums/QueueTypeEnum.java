package com.latam.pax.robotdistr.enums;

import java.util.HashMap;

/**
 * Represent a parameters.
 * 
 * @author Everis
 * @author Daniel Carvajal (daniel.carvajal.soto@everis.com)
 * @version 1.0
 *  
 */
public enum QueueTypeEnum {	
	CREATION("CRTN", "Creation"),
	MODIFICATION("MDFN", "Modification"),
	INTERLINEAL("NTL", "Interlineal"),
	MANUAL("MNL", "Manual"),
	DOMESTICO("DOM", "Domestico"),
	INTERNACIONAL("INTER", "Internacional");
	
	private final String key;
    private final String value; 

	private QueueTypeEnum(String key, String value) {
		this.key = key;
		this.value = value;
	}

	/**
	 * get key
	 * 
	 * @return
	 */
	public String getKey() {
		return key;
	}
	
	/**
	 * get value
	 * 
	 * @return
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * 
	 * get values
	 * 
	 * @return all values
	 */
	public static HashMap<String, String> getHashMapValues() {
        HashMap<String, String> hashMap = new HashMap<String, String>();
        for (QueueTypeEnum e : QueueTypeEnum.values()) {
            hashMap.put(e.getKey(), e.getValue());
        }
        return hashMap;
    }
}
