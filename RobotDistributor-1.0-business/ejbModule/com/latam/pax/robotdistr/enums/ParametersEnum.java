package com.latam.pax.robotdistr.enums;


/**
 * Represent a parameters.
 * 
 * @author Everis
 * @author Daniel Carvajal (daniel.carvajal.soto@everis.com)
 * @version 1.0
 *  
 */
public enum ParametersEnum {	
	SEGMENT_SIZE("SEG_SIZE"),
	AIRLINE_CODES("AIRLN_COD"),
	QUEUE_DEFAULT_DOM_NUMBER("QDDOM_NUM"),
	QUEUE_DEFAULT_DOM_PSEUDOCITYCODE("QDDOM_PCC"),
	QUEUE_DEFAULT_INTER_NUMBER("QDINT_NUM"),
	QUEUE_DEFAULT_INTER_PSEUDOCITYCODE("QDINT_PCC"),
	QUEUE_DEFAULT_DEFAULT_NUMBER("QDDEF_NUM"),
	QUEUE_DEFAULT_DEFAULT_PSEUDOCITYCODE("QDDEF_PCC"),
	SEGMENT_STATUS("SEG_STATS"),
	CITY_CODE_SPECIAL_MARK("CCBR_SPRK"),
	//
	MAIL_ERROR_COMPONENT("MAIL_ERROR_COMPONENT"),
	MAIL_ERROR_DATE("MAIL_ERROR_DATE"),
	MAIL_ERROR_CODE("MAIL_ERROR_CODE"),
	MAIL_ERROR_TYPE("MAIL_ERROR_TYPE"),
	MAIL_ERROR_MESSAGE("MAIL_ERROR_MESSAGE"),
	
	
	MAIL_TO_DESTINATION("MAIL_TODES"),
	MAIL_CC_DESTINATION("MAIL_CCDES");
	
	
	private final String key;

	private ParametersEnum(String key) {
		this.key = key;
	}

	/**
	 * get key
	 * 
	 * @return
	 */
	public String getKey() {
		return key;
	}	
	
}
