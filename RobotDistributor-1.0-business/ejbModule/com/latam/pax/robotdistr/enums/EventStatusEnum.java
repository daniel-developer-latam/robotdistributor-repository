package com.latam.pax.robotdistr.enums;

import java.util.HashMap;

/**
 * Represent a event status.
 * 
 * @author Everis
 * @author Daniel Carvajal (daniel.carvajal.soto@everis.com)
 * @version 1.0
 *  
 */
public enum EventStatusEnum {
	RULE_WARNING(1, "RULE_WARNING"),
	RULE_SUCCESS(0, "RULE_SUCCESS"),
	RULE_EXCEPTION(-1, "RULE_EXCEPTION"),
	RULE_ERROR(-2, "RULE_ERROR"),
	FAIL(-3, "FAIL");
	
	

    private final Integer code;
    private final String message; 

	private EventStatusEnum(Integer code, String message) {
		this.code = code;
		this.message = message;
	}

	/**
	 * get event status code
	 * 
	 * @return
	 */
	public int getCode() {
		return code;
	}
	
	/**
	 * get event status message
	 * 
	 * @return
	 */
	public String getMessage() {
		return message;
	}
	
	/**
	 * 
	 * get all event status
	 * 
	 * @return all event status
	 */
	public static HashMap<Integer, String> getHashMapValues() {
        HashMap<Integer, String> hashMap = new HashMap<Integer, String>();
        for (EventStatusEnum e : EventStatusEnum.values()) {
            hashMap.put(e.getCode(), e.getMessage());
        }
        return hashMap;
    }
}
