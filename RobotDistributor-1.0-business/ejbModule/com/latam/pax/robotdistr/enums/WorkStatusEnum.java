package com.latam.pax.robotdistr.enums;

import java.util.HashMap;

/**
 * Represent a event status.
 * 
 * @author Everis
 * @author Daniel Carvajal (daniel.carvajal.soto@everis.com)
 * @version 1.0
 *  
 */
public enum WorkStatusEnum {	
	SUCCESS(0, "SUCCESS"),
	ERROR(-1, "ERROR"),
	WARNING(1, "WARNING"),
	STARTED(2, "STARTED"),
	COMPLETED(3, "COMPLETED"),
	ABORTED(4, "ABORTED");
	
    private final Integer code;
    private final String message; 

	private WorkStatusEnum(Integer code, String message) {
		this.code = code;
		this.message = message;
	}

	/**
	 * get event status code
	 * 
	 * @return
	 */
	public int getCode() {
		return code;
	}
	
	/**
	 * get event status message
	 * 
	 * @return
	 */
	public String getMessage() {
		return message;
	}
	
	/**
	 * 
	 * get all event status
	 * 
	 * @return all event status
	 */
	public static HashMap<Integer, String> getHashMapValues() {
        HashMap<Integer, String> hashMap = new HashMap<Integer, String>();
        for (WorkStatusEnum e : WorkStatusEnum.values()) {
            hashMap.put(e.getCode(), e.getMessage());
        }
        return hashMap;
    }
}
