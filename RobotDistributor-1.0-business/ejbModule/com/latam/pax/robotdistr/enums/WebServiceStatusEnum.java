package com.latam.pax.robotdistr.enums;

import java.util.HashMap;


/**
 * Represent a web status.
 * 
 * @author Everis
 * @author Daniel Carvajal (daniel.carvajal.soto@everis.com)
 * @version 1.0
 *  
 */
public enum WebServiceStatusEnum {	
	SUCCESS(0, "SUCCESS"),
	ERROR(-1, "ERROR");
	

    private final Integer code;
    private final String message; 

	private WebServiceStatusEnum(Integer code, String message) {
		this.code = code;
		this.message = message;
	}

	/**
	 * get web status code
	 * 
	 * @return
	 */
	public int getCode() {
		return code;
	}
	
	/**
	 * get web status message
	 * 
	 * @return
	 */
	public String getMessage() {
		return message;
	}
	
	/**
	 * 
	 * get all web status
	 * 
	 * @return all web status
	 */
    public static HashMap<Integer, String> getHashMapValues() {
        HashMap<Integer, String> hashMap = new HashMap<Integer, String>();
        for (WebServiceStatusEnum e : WebServiceStatusEnum.values()) {
            hashMap.put(e.getCode(), e.getMessage());
        }
        return hashMap;
    }
}
