package com.latam.arq.commons.ws.clients;

import javax.xml.ws.WebServiceException;
import javax.xml.ws.soap.SOAPFaultException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.latam.ws.v3_2.DisplayPnrByRecordLocatorPortType;
import com.latam.ws.v3_2.DisplayPnrByRecordLocatorRQ;
import com.latam.ws.v3_2.DisplayPnrByRecordLocatorRS;

import com.latam.entities.v3_0.ServiceStatusType;
import com.latam.arq.commons.exceptions.LATAMException;
import com.latam.arq.commons.exceptions.LATAMExceptionService;
import com.latam.arq.commons.ws.WSClientsWarmUpStep;
import com.latam.arq.commons.ws.exceptions.WSClientsException;

/**
 * Base Webservice client implementation
 * 
 * @author ANT using import-client target
 * @author LATAM Enterprise Architect
 *
 */
public class WSDisplayPnrByRecordLocatorClient {
	/**
	 * Registered Service Name
	 */
	public static final String SERVICE_NAME = "DisplayPnrByRecordLocator-3.2.0";

	/** Service client logger */
	public static final Logger logger = LoggerFactory.getLogger(WSDisplayPnrByRecordLocatorClient.class);
	
	/**<PRE>
	 * Call the webservice.
	 *
	 * (use)
	 * On a EJB implementation (inside the business layer), you can call this client using the following lines:
	 *
	 *		@EJB
	 * 		private LATAMExceptionService latamExceptionFactory;
	 *		
	 *		public void doTheServiceCall( BusinessObject bo ) throws LATAMException{
	 *			//Build the request of service
	 *			DisplayPnrByRecordLocatorRQ rq = new DisplayPnrByRecordLocatorRQ();
	 *			//Map the BO to the service request object
	 *			rq.setSomething( bo.getSomething() );
	 *
	 *			//Call the service
	 *			DisplayPnrByRecordLocatorRS rs = WSDisplayPnrByRecordLocatorClient.call( rq, latamExceptionFactory );				
	 *
	 *			//Process the Response
	 *			rs.getSomeSchemaReference();
	 *		}
	 *
	 * (appConfig)
	 * This client use the following properties on the default application configuration properties file
	 *  + DisplayPnrByRecordLocator-3.2.0.username
	 *  + DisplayPnrByRecordLocator-3.2.0.password
	 *  + DisplayPnrByRecordLocator-3.2.0.endpoint
	 *  + DisplayPnrByRecordLocator-3.2.0.applicationName
	 * 
	 * (latamexception.properties)
	 * Any call on this service required a valid configuration of the following latamexception:
	 *  + DisplayPnrByRecordLocator-3.2.0.badRequest (can use 'code' and 'message' properties)
	 *  + DisplayPnrByRecordLocator-3.2.0.noServiceStatus
	 *  + DisplayPnrByRecordLocator-3.2.0.callerror
	 *  + DisplayPnrByRecordLocator-3.2.0.creationerror
	 *  
	 *  Note: This client can be used
	 *  only inside methods located on 
	 *  the business layer.
	 *  
	 * </PRE>
	 * @param rq Request object
	 * @param latamExceptionFactory A instantiated reference to a LATAMExceptionService
	 * @return Response object
	 * @throws LATAMException
	 */
	public static DisplayPnrByRecordLocatorRS call( final DisplayPnrByRecordLocatorRQ rq, final LATAMExceptionService latamExceptionFactory ) throws LATAMException{
		try{
			final DisplayPnrByRecordLocatorPortType port = (DisplayPnrByRecordLocatorPortType)WSClientsWarmUpStep.getBindingProvider( SERVICE_NAME );
			
			RequestResponseUtils.logRequest(logger,rq);			
			final DisplayPnrByRecordLocatorRS rs = port.displayPnrByRecordLocator(rq);
			RequestResponseUtils.logResponse(logger,rs);
			
			if (rs.getServiceStatus()!=null){
				final ServiceStatusType serviceStatus = rs.getServiceStatus();
				if (serviceStatus.getCode()==0){
					return rs;
				}else{
					//Check service status codes
					throw latamExceptionFactory.createLATAMException(SERVICE_NAME + ".badRequest")
						.setProperty("code", "" + serviceStatus.getCode())
						.setProperty("message", "" + serviceStatus.getMessage());
				}
			}else{
				throw latamExceptionFactory.createLATAMException(SERVICE_NAME + ".noServiceStatus");
			}
		}catch( final SOAPFaultException e ){
			throw latamExceptionFactory.createLATAMException(SERVICE_NAME + ".callerror", RequestResponseUtils.getDetailedCauseFromSoapFault(e));
		}catch( final WebServiceException e ){
			throw latamExceptionFactory.createLATAMException(SERVICE_NAME + ".callerror",e);
		}catch( final WSClientsException e ){
			throw latamExceptionFactory.createLATAMException(SERVICE_NAME + ".creationerror",e);
		}
	}
}