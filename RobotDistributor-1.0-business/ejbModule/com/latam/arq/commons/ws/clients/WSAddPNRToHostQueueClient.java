package com.latam.arq.commons.ws.clients;

import javax.xml.ws.WebServiceException;
import javax.xml.ws.soap.SOAPFaultException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.latam.ws.v3_1.AddPNRToHostQueuePortType;
import com.latam.ws.v3_1.AddPNRToHostQueueRQ;
import com.latam.ws.v3_1.AddPNRToHostQueueRS;

import com.latam.entities.v3_0.ServiceStatusType;
import com.latam.arq.commons.exceptions.LATAMException;
import com.latam.arq.commons.exceptions.LATAMExceptionService;
import com.latam.arq.commons.ws.WSClientsWarmUpStep;
import com.latam.arq.commons.ws.exceptions.WSClientsException;

/**
 * Base Webservice client implementation
 * 
 * @author ANT using import-client target
 * @author LATAM Enterprise Architect
 *
 */
public class WSAddPNRToHostQueueClient {
	/**
	 * Registered Service Name
	 */
	public static final String SERVICE_NAME = "AddPNRToHostQueue-3.1.0";

	/** Service client logger */
	public static final Logger logger = LoggerFactory.getLogger(WSAddPNRToHostQueueClient.class);
	
	/**<PRE>
	 * Call the webservice.
	 *
	 * (use)
	 * On a EJB implementation (inside the business layer), you can call this client using the following lines:
	 *
	 *		@EJB
	 * 		private LATAMExceptionService latamExceptionFactory;
	 *		
	 *		public void doTheServiceCall( BusinessObject bo ) throws LATAMException{
	 *			//Build the request of service
	 *			AddPNRToHostQueueRQ rq = new AddPNRToHostQueueRQ();
	 *			//Map the BO to the service request object
	 *			rq.setSomething( bo.getSomething() );
	 *
	 *			//Call the service
	 *			AddPNRToHostQueueRS rs = WSAddPNRToHostQueueClient.call( rq, latamExceptionFactory );				
	 *
	 *			//Process the Response
	 *			rs.getSomeSchemaReference();
	 *		}
	 *
	 * (appConfig)
	 * This client use the following properties on the default application configuration properties file
	 *  + AddPNRToHostQueue-3.1.0.username
	 *  + AddPNRToHostQueue-3.1.0.password
	 *  + AddPNRToHostQueue-3.1.0.endpoint
	 *  + AddPNRToHostQueue-3.1.0.applicationName
	 * 
	 * (latamexception.properties)
	 * Any call on this service required a valid configuration of the following latamexception:
	 *  + AddPNRToHostQueue-3.1.0.badRequest (can use 'code' and 'message' properties)
	 *  + AddPNRToHostQueue-3.1.0.noServiceStatus
	 *  + AddPNRToHostQueue-3.1.0.callerror
	 *  + AddPNRToHostQueue-3.1.0.creationerror
	 *  
	 *  Note: This client can be used
	 *  only inside methods located on 
	 *  the business layer.
	 *  
	 * </PRE>
	 * @param rq Request object
	 * @param latamExceptionFactory A instantiated reference to a LATAMExceptionService
	 * @return Response object
	 * @throws LATAMException
	 */
	public static AddPNRToHostQueueRS call( final AddPNRToHostQueueRQ rq, final LATAMExceptionService latamExceptionFactory ) throws LATAMException{
		try{
			final AddPNRToHostQueuePortType port = (AddPNRToHostQueuePortType)WSClientsWarmUpStep.getBindingProvider( SERVICE_NAME );
			
			RequestResponseUtils.logRequest(logger,rq);			
			final AddPNRToHostQueueRS rs = port.addPNRToHostQueue(rq);
			RequestResponseUtils.logResponse(logger,rs);
			
			if (rs.getServiceStatus()!=null){
				final ServiceStatusType serviceStatus = rs.getServiceStatus();
				if (serviceStatus.getCode()==0){
					return rs;
				}else{
					//Check service status codes
					throw latamExceptionFactory.createLATAMException(SERVICE_NAME + ".badRequest")
						.setProperty("code", "" + serviceStatus.getCode())
						.setProperty("message", "" + serviceStatus.getMessage());
				}
			}else{
				throw latamExceptionFactory.createLATAMException(SERVICE_NAME + ".noServiceStatus");
			}
		}catch( final SOAPFaultException e ){
			throw latamExceptionFactory.createLATAMException(SERVICE_NAME + ".callerror", RequestResponseUtils.getDetailedCauseFromSoapFault(e));
		}catch( final WebServiceException e ){
			throw latamExceptionFactory.createLATAMException(SERVICE_NAME + ".callerror",e);
		}catch( final WSClientsException e ){
			throw latamExceptionFactory.createLATAMException(SERVICE_NAME + ".creationerror",e);
		}
	}
}