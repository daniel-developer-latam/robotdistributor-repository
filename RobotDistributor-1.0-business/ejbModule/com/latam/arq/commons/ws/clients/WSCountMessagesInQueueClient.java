package com.latam.arq.commons.ws.clients;

import javax.xml.ws.WebServiceException;
import javax.xml.ws.soap.SOAPFaultException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.latam.ws.v3_0.CountMessagesInQueuePortType;
import com.latam.ws.v3_0.CountMessagesInQueueRQ;
import com.latam.ws.v3_0.CountMessagesInQueueRS;

import com.latam.entities.v3_0.ServiceStatusType;
import com.latam.arq.commons.exceptions.LATAMException;
import com.latam.arq.commons.exceptions.LATAMExceptionService;
import com.latam.arq.commons.ws.WSClientsWarmUpStep;
import com.latam.arq.commons.ws.exceptions.WSClientsException;

/**
 * Base Webservice client implementation
 * 
 * @author ANT using import-client target
 * @author LATAM Enterprise Architect
 *
 */
public class WSCountMessagesInQueueClient {
	/**
	 * Registered Service Name
	 */
	public static final String SERVICE_NAME = "CountMessagesInQueue-3.0.1";

	/** Service client logger */
	public static final Logger logger = LoggerFactory.getLogger(WSCountMessagesInQueueClient.class);
	
	/**<PRE>
	 * Call the webservice.
	 *
	 * (use)
	 * On a EJB implementation (inside the business layer), you can call this client using the following lines:
	 *
	 *		@EJB
	 * 		private LATAMExceptionService latamExceptionFactory;
	 *		
	 *		public void doTheServiceCall( BusinessObject bo ) throws LATAMException{
	 *			//Build the request of service
	 *			CountMessagesInQueueRQ rq = new CountMessagesInQueueRQ();
	 *			//Map the BO to the service request object
	 *			rq.setSomething( bo.getSomething() );
	 *
	 *			//Call the service
	 *			CountMessagesInQueueRS rs = WSCountMessagesInQueueClient.call( rq, latamExceptionFactory );				
	 *
	 *			//Process the Response
	 *			rs.getSomeSchemaReference();
	 *		}
	 *
	 * (appConfig)
	 * This client use the following properties on the default application configuration properties file
	 *  + CountMessagesInQueue-3.0.1.username
	 *  + CountMessagesInQueue-3.0.1.password
	 *  + CountMessagesInQueue-3.0.1.endpoint
	 *  + CountMessagesInQueue-3.0.1.applicationName
	 * 
	 * (latamexception.properties)
	 * Any call on this service required a valid configuration of the following latamexception:
	 *  + CountMessagesInQueue-3.0.1.badRequest (can use 'code' and 'message' properties)
	 *  + CountMessagesInQueue-3.0.1.noServiceStatus
	 *  + CountMessagesInQueue-3.0.1.callerror
	 *  + CountMessagesInQueue-3.0.1.creationerror
	 *  
	 *  Note: This client can be used
	 *  only inside methods located on 
	 *  the business layer.
	 *  
	 * </PRE>
	 * @param rq Request object
	 * @param latamExceptionFactory A instantiated reference to a LATAMExceptionService
	 * @return Response object
	 * @throws LATAMException
	 */
	public static CountMessagesInQueueRS call( final CountMessagesInQueueRQ rq, final LATAMExceptionService latamExceptionFactory ) throws LATAMException{
		try{
			final CountMessagesInQueuePortType port = (CountMessagesInQueuePortType)WSClientsWarmUpStep.getBindingProvider( SERVICE_NAME );
			
			RequestResponseUtils.logRequest(logger,rq);			
			final CountMessagesInQueueRS rs = port.countMessagesInQueue(rq);
			RequestResponseUtils.logResponse(logger,rs);
			
			if (rs.getServiceStatus()!=null){
				final ServiceStatusType serviceStatus = rs.getServiceStatus();
				if (serviceStatus.getCode()==0){
					return rs;
				}else{
					//Check service status codes
					throw latamExceptionFactory.createLATAMException(SERVICE_NAME + ".badRequest")
						.setProperty("code", "" + serviceStatus.getCode())
						.setProperty("message", "" + serviceStatus.getMessage());
				}
			}else{
				throw latamExceptionFactory.createLATAMException(SERVICE_NAME + ".noServiceStatus");
			}
		}catch( final SOAPFaultException e ){
			throw latamExceptionFactory.createLATAMException(SERVICE_NAME + ".callerror", RequestResponseUtils.getDetailedCauseFromSoapFault(e));
		}catch( final WebServiceException e ){
			throw latamExceptionFactory.createLATAMException(SERVICE_NAME + ".callerror",e);
		}catch( final WSClientsException e ){
			throw latamExceptionFactory.createLATAMException(SERVICE_NAME + ".creationerror",e);
		}
	}
}