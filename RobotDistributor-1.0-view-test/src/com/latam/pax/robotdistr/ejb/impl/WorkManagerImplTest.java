package com.latam.pax.robotdistr.ejb.impl;

import org.junit.Test;

import com.latam.arq.commons.util.LocalJNDI;
import com.latam.pax.robotdistr.ejb.services.WorkManagerService;

import junit.framework.TestCase;

public class WorkManagerImplTest extends TestCase {

	
	WorkManagerService workManagerService;
	
	@Override
	public void setUp() throws Exception {		
	   workManagerService = LocalJNDI.lookupEJB(WorkManagerService.class);
	}	
	
	@Override
	public void tearDown() throws Exception {
		
	}
	
	@Test
	public void ExecuteRobotDistributor() throws Exception {
		workManagerService.executeRobotDistributor();
	}			
}
