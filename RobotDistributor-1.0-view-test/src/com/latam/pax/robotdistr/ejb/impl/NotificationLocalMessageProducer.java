package com.latam.pax.robotdistr.ejb.impl;

import java.util.Hashtable;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueSender;
import javax.jms.QueueSession;
import javax.jms.Session;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.xml.bind.JAXBException;

import com.latam.pax.robotdistr.event.RobotDistributorGeneratedExceptionEvent;
import com.latam.pax.robotdistr.util.JAXBConverter;
import com.latam.pax.robotdistr.util.UtilLogger;

import lombok.extern.slf4j.Slf4j;


@Slf4j
public class NotificationLocalMessageProducer{
	
	   	private static InitialContext initialContext = null;
	    private static QueueConnectionFactory queueConnectionFactory = null;
	    private static QueueConnection queueConnection = null;
	    private static QueueSession queueSession = null;
	    private static Queue queue = null;
	    private static QueueSender queueSender = null;	 
	    //
	    private static final String QCF_NAME = "jms/factory/robotDistributorFactory";
	    private static final String QUEUE_NAME = "jms/queue/robotDistributor/notificationEventBusQueue";	    
	    //	    

	    public NotificationLocalMessageProducer() {
	        try {
	            // create InitialContex
	        	Hashtable properties = new Hashtable();
	            properties.put(Context.INITIAL_CONTEXT_FACTORY, "weblogic.jndi.WLInitialContextFactory");
	            // NOTE: The port number of the server is provided in the next line, followed by the userid and password on the next two lines.
	            //properties.put(Context.PROVIDER_URL, "t3://172.27.3.22:8053");
	            //properties.put(Context.SECURITY_PRINCIPAL, "weblogic");
	            //properties.put(Context.SECURITY_CREDENTIALS, "pass");
	            //
	            initialContext = new InitialContext(properties);          
	            
	            // create QueueConnectionFactory
	            queueConnectionFactory = (QueueConnectionFactory) initialContext.lookup(QCF_NAME);            
	            // create QueueConnection
	            queueConnection = queueConnectionFactory.createQueueConnection();	            
	            // create QueueSession
	            queue = (Queue) initialContext.lookup(QUEUE_NAME);
	            // lookup Queue
	            queueSession = queueConnection.createQueueSession(false, 0);
	            // create QueueSender
	            queueSender = queueSession.createSender(queue);

	        } catch (NamingException e) {
	            logger.error(e.getMessage(), e);
	            
	        } catch (JMSException e) {
	        	logger.error(e.getMessage(), e);
	        }
	    }

	    public void publishEvent(RobotDistributorGeneratedExceptionEvent event) {
	    	try {			
				
				Session session = queueConnection.createSession(false, Session.AUTO_ACKNOWLEDGE);			
				MessageProducer messageProducer = session.createProducer(queue);
									
				// marshal command			
				JAXBConverter<RobotDistributorGeneratedExceptionEvent> jaxbConverter = new JAXBConverter<>(RobotDistributorGeneratedExceptionEvent.class);
				String xmlCommand = jaxbConverter.marshal(event);
				
				// send message
				Message message = session.createTextMessage(xmlCommand);
				messageProducer.send(message);
				
				UtilLogger.logInfo("Send command message : " + event.toString());		
							
			} catch (JMSException e) {
				if(logger.isErrorEnabled()){
					logger.error(e.getMessage(), e);
				}
			} catch (JAXBException e) {
				if(logger.isErrorEnabled()){
					logger.error(e.getMessage(), e);
				}
			} 		
	    }

	    public void jmsFinalize() {
	        // clean up
	        try {
	            queueSender.close();
	            queueSender = null;
	            queue = null;
	            queueSession.close();
	            queueSession = null;
	            queueConnection.close();
	            queueConnection = null;
	            queueConnectionFactory = null;
	            initialContext = null;
	        } catch (JMSException e) {
	        	logger.error(e.getMessage(), e);
	        }
	    }
}
