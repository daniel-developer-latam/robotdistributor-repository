package com.latam.pax.robotdistr.ejb.impl;

import java.util.List;

import org.junit.Test;

import com.latam.arq.commons.util.LocalJNDI;
import com.latam.pax.robotdistr.domain.Group;
import com.latam.pax.robotdistr.domain.InputQueue;
import com.latam.pax.robotdistr.domain.OfficeAccount;
import com.latam.pax.robotdistr.domain.OutputQueue;
import com.latam.pax.robotdistr.domain.Parameter;
import com.latam.pax.robotdistr.ejb.services.RepositoryService;
import com.latam.pax.robotdistr.enums.ParametersEnum;
import com.latam.pax.robotdistr.enums.QueueTypeEnum;
import com.latam.pax.robotdistr.util.UtilValidator;

import junit.framework.TestCase;

public class RepositoryImplTest extends TestCase {

	private RepositoryService repositoryService;

	@Override
	public void setUp() throws Exception {
		repositoryService = LocalJNDI.lookupEJB(RepositoryService.class);
	}

	@Override
	public void tearDown() throws Exception {
	}

	@Test
	public void GetParameterByKey() throws Exception {
		Parameter parameter = repositoryService.getParameterByKey(ParametersEnum.SEGMENT_SIZE.getKey());

		assertNotNull(parameter);
		assertNotNull(parameter.getKey());
		assertNotNull(parameter.getValue());
	}
	
	@Test
	public void GetAllInputQueue() throws Exception {
		List<InputQueue> inputQueues = repositoryService.getAllInputQueue();

		assertNotNull(inputQueues);
		assertTrue(UtilValidator.isNotEmpty(inputQueues));
		
	}		
	
	@Test
	public void GetOutputQueueByTypeAndAOC() throws Exception {
		OfficeAccount officeAccount = new OfficeAccount();
		officeAccount.setAccountingCode("AD");
		officeAccount.setAccountingCity("SCL");
		officeAccount.setAaaPseudoCityCode("SCL");
		//
		Group group = new Group();
		group.setGroupName("GRP1");
		group.setGroupType("GRP1");
		//
		InputQueue inputQueue = new InputQueue();
		inputQueue.setNumber("253");
		inputQueue.setPseudoCityCode("HDQ");
		
		OutputQueue outputQueue = 
				repositoryService.getOutputQueueByInputQueueAndTypeAndAOCAndGroup(
						inputQueue, QueueTypeEnum.DOMESTICO.getKey(), officeAccount, group);

		assertNotNull(outputQueue);
		assertNotNull(outputQueue.getNumber());
		assertNotNull(outputQueue.getPseudoCityCode());
		assertNotNull(outputQueue.getBusinessType());
		//
		assertNotNull(outputQueue.getOfficeAccount().getAccountingCode());
		assertNotNull(outputQueue.getOfficeAccount().getAccountingCity());
		assertNotNull(outputQueue.getOfficeAccount().getAaaPseudoCityCode());
	}
}
