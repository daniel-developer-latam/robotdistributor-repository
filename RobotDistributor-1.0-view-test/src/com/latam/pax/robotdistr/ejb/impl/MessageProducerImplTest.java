package com.latam.pax.robotdistr.ejb.impl;

import java.util.UUID;

import org.junit.Test;

import com.latam.pax.robotdistr.command.ToDistributeCommand;
import com.latam.pax.robotdistr.enums.EventStatusEnum;
import com.latam.pax.robotdistr.event.RobotDistributorGeneratedExceptionEvent;

import junit.framework.TestCase;

public class MessageProducerImplTest extends TestCase {
			
	@Override
	public void setUp() throws Exception {
	}	
	
	@Override
	public void tearDown() throws Exception {	
	}	
	
	@Test
	public void SendCommandToDistributeInQueue() throws Exception {
		CommandLocalMessageProducer cmdMessageProducer = new CommandLocalMessageProducer();
		
		for(int i = 0 ;  i < 300 ; i++){
			ToDistributeCommand command = new ToDistributeCommand();
			
			command.setUuid(UUID.randomUUID().toString());			
			command.setRecordLocator("FMKJQJ");
					
			cmdMessageProducer.sendCommandToDistributeInQueue(command);
		}		
	}
	
	@Test
	public void testPublishEvent() throws Exception {
		NotificationLocalMessageProducer notyMessageProducer = new NotificationLocalMessageProducer();
		
		try{
			int division = 1/0;
		}catch(ArithmeticException e){
						
			RobotDistributorGeneratedExceptionEvent event =
									new RobotDistributorGeneratedExceptionEvent(
											String.valueOf(EventStatusEnum.FAIL.getCode()), e.getClass().getSimpleName(), e.getMessage(), EventStatusEnum.FAIL);
			notyMessageProducer.publishEvent(event);
		}			
	}
			
}
