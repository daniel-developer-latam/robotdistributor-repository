package com.latam.pax.robotdistr.util;

import org.junit.Test;

import com.latam.pax.robotdistr.enums.EventStatusEnum;
import com.latam.pax.robotdistr.event.RobotDistributorGeneratedExceptionEvent;

import junit.framework.TestCase;

public class UtilTemplateLoaderTest extends TestCase {
			
	@Override
	public void setUp() throws Exception {
	}	
	
	@Override
	public void tearDown() throws Exception {	
	}	
			
	@Test
	public void testGetTemplate() throws Exception {
		try{
			int division = 1/0;
		}catch(ArithmeticException e){
						
			RobotDistributorGeneratedExceptionEvent event =
									new RobotDistributorGeneratedExceptionEvent(
											String.valueOf(EventStatusEnum.FAIL.getCode()), e.getClass().getSimpleName(), e.getMessage(), EventStatusEnum.FAIL);
			String template = UtilTemplateLoader.getTemplate(event);
		}				
	}	
		
	@Test
	public void testIsValidRecordLocator() throws Exception {
		
		assertTrue(UtilValidator.isValidRecordLocator("SJAPJG"));
		
		assertFalse(UtilValidator.isValidRecordLocator("END OF DISPLAY FOR REQUESTED DATA"));
		
	}	
}
