package com.latam.pax.robotdistr.domain;

import java.io.Serializable;

import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

import com.latam.pax.robotdistr.validator.group.IntegrityGroup;
import com.latam.pax.robotdistr.validator.group.IntegrityGroupName;

import lombok.Data;


/**
 * Group Domain Object
 * 
 * <br /><br />
 * 
 * Represent a Group of the Passenger Name Record(PNR)
 *   
 * 
 * @author Everis
 * @author Daniel Carvajal (daniel.carvajal.soto@everis.com)
 * @version 1.0
 *  
 */
@Data
public class Group implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Size(min = 4, message = "{domain.Group.groupName.size}", groups = {IntegrityGroupName.class})  
	@NotEmpty(message = "{domain.Group.groupName.notEmpty}", groups = {IntegrityGroup.class})
	private String groupName;		
	
	@NotEmpty(message = "{domain.Group.groupType.notEmpty}", groups = {})
	private String groupType;		

}
