package com.latam.pax.robotdistr.domain;


import java.io.Serializable;

import org.hibernate.validator.constraints.NotEmpty;

import com.latam.pax.robotdistr.validator.group.IntegrityGroup;

import lombok.Data;


/**
 * FlightSegment Domain Object
 * 
 * <br /><br />
 * 
 * Represent a Segment of the Passenger Name Record(PNR)
 *   
 * 
 * @author Everis
 * @author Daniel Carvajal (daniel.carvajal.soto@everis.com)
 * @version 1.0
 *  
 */
@Data
public class FlightSegment implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//@Id
	@NotEmpty(message = "{domain.FlightSegment.segmentNumber.notEmpty}", groups = {IntegrityGroup.class})
	private String segmentNumber;
	
	@NotEmpty(message = "{domain.FlightSegment.segmentStatus.notEmpty}", groups = {IntegrityGroup.class})
	private String segmentStatus;
			
	@NotEmpty(message = "{domain.FlightSegment.airlineCode.notEmpty}", groups = {IntegrityGroup.class})
	private String airlineCode;
	
	@NotEmpty(message = "{domain.FlightSegment.departureAirport.notEmpty}", groups = {IntegrityGroup.class})
	private String departureAirport;
	
	@NotEmpty(message = "{domain.FlightSegment.arrivalAirport.notEmpty}", groups = {IntegrityGroup.class})
	private String arrivalAirport;
	
}
