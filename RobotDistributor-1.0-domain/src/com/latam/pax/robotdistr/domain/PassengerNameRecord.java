package com.latam.pax.robotdistr.domain;

import java.io.Serializable;
import java.util.List;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import com.latam.pax.robotdistr.validator.group.ContainsDomBrSegmentsGroup;
import com.latam.pax.robotdistr.validator.group.GroupValidGroup;
import com.latam.pax.robotdistr.validator.group.IntegrityGroup;
import com.latam.pax.robotdistr.validator.group.SegmentsValidGroup;

import lombok.Data;


/**
 * Passenger Name Record Domain Object
 * 
 * <br /><br />
 * 
 * Represent a Passenger Name Record(PNR) 
 *   
 * 
 * @author Everis
 * @author Daniel Carvajal (daniel.carvajal.soto@everis.com)
 * @version 1.0
 *  
 */
@Data
public class PassengerNameRecord  implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//@WorkId
	@NotNull(message = "{domain.PassengerNameRecord.uuid.notEmpty}", groups = {IntegrityGroup.class})
	private String uuid;
	
	//@Id
	@NotEmpty(message = "{domain.PassengerNameRecord.recordLocator.notEmpty}", groups = {IntegrityGroup.class})
	private String recordLocator;
	
	@NotNull(message = "{domain.PassengerNameRecord.officeAccount.notEmpty}", groups = {IntegrityGroup.class})
	private OfficeAccount officeAccount;
	
	@NotNull(message = "{domain.PassengerNameRecord.Group.notEmpty}", groups = {IntegrityGroup.class})
	private Group group;

	@NotEmpty(message = "{domain.PassengerNameRecord.flightSegments.notEmpty}", groups = {IntegrityGroup.class})	
	private List<FlightSegment> flightSegments;
	
	@NotEmpty(message = "{domain.PassengerNameRecord.remarks.notEmpty}", groups = {})
	private List<Remark> remarks;
	
	@NotNull(message = "{domain.PassengerNameRecord.inputQueue.notEmpty}", groups = {IntegrityGroup.class})
	private InputQueue inputQueue;	
	
	private boolean validGroup;

	private boolean validSegments;
	
	private boolean containsPosBrSpecialMark;
	
	private boolean containsFullBrSegments; 
	
	/**
	 * Get is valid group 
	 * 
	 * @return true is ok else false
	 */
	@AssertTrue(message = "{domain.PassengerNameRecord.group.validGroup}", groups = GroupValidGroup.class)	
	public boolean isValidGroup() {
		return validGroup;
	}

	/**
	 * 
	 * Set valid group field
	 * 
	 * @param validGroup
	 */
	public void setValidGroup(boolean validGroup) {
		this.validGroup = validGroup;
	}	
	
	
	/**
	 * Get is valid segments 
	 * 
	 * @return true is ok else false
	 */
	@AssertTrue(message = "{domain.PassengerNameRecord.flightSegments.validSegments}", groups = SegmentsValidGroup.class)
	public boolean isValidSegments() {
		return validSegments;
	}

	/**
	 * 
	 * Set valid segments field
	 * 
	 * @param validSegments
	 */
	public void setValidSegments(boolean validSegments) {
		this.validSegments = validSegments;
	}
	
	/**
	 * Get is containsPosBrSpecialMark
	 * 
	 * @return true is ok else false
	 */
	@AssertTrue(message = "{domain.PassengerNameRecord.Remark.containsPosBrSpecialMark}", groups = ContainsDomBrSegmentsGroup.class)
	public boolean isContainsPosBrSpecialMark() {
		return containsPosBrSpecialMark;
	}

	/**
	 * 
	 * Set containsPosBrSpecialMark
	 * 
	 * @param containsPosBrSpecialMark
	 */
	public void setContainsPosBrSpecialMark(boolean containsPosBrSpecialMark) {
		this.containsPosBrSpecialMark = containsPosBrSpecialMark;
	}
	
	/**
	 * Get is containsFullBrSegments
	 * 
	 * @return true is ok else false
	 */
	@AssertTrue(message = "{domain.PassengerNameRecord.Remark.containsFullBrSegments}", groups = ContainsDomBrSegmentsGroup.class)
	public boolean isContainsFullBrSegments() {
		return containsFullBrSegments;
	}

	/**
	 * 
	 * Set containsFullBrSegments
	 * 
	 * @param containsFullBrSegments
	 */
	public void setContainsFullBrSegments(boolean containsFullBrSegments) {
		this.containsFullBrSegments = containsFullBrSegments;
	}	
	
}
