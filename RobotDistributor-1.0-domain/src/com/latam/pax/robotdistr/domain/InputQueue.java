package com.latam.pax.robotdistr.domain;

import java.io.Serializable;

import org.hibernate.validator.constraints.NotEmpty;

import lombok.Data;

@Data
public class InputQueue implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@NotEmpty(message = "{domain.InputQueue.number.notEmpty}", groups = {})	
	private String number;
	
	@NotEmpty(message = "{domain.InputQueue.pseudoCityCode.notEmpty}", groups = {})
	private String pseudoCityCode;
	
	@NotEmpty(message = "{domain.InputQueue.type.notEmpty}", groups = {})
	private String type;
	
	@NotEmpty(message = "{domain.InputQueue.mirrorNumber.notEmpty}", groups = {})	
	private String mirrorNumber;
	
	@NotEmpty(message = "{domain.InputQueue.mirrorPseudoCityCode.notEmpty}", groups = {})
	private String mirrorPseudoCityCode;
	
}
