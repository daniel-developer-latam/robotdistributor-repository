package com.latam.pax.robotdistr.domain;

import java.io.Serializable;

import org.hibernate.validator.constraints.NotEmpty;

import lombok.Data;

@Data
public class OfficeAccount implements Serializable {
	private static final long serialVersionUID = 1L;
		
	@NotEmpty(message = "{domain.OfficeAccount.accountingCode.notEmpty}", groups = {})
	private String accountingCode;
	
	@NotEmpty(message = "{domain.OfficeAccount.accountingCity.notEmpty}", groups = {})
	private String accountingCity;
	
	@NotEmpty(message = "{domain.OfficeAccount.aaaPseudoCityCode.notEmpty}", groups = {})
	private String aaaPseudoCityCode;

}
