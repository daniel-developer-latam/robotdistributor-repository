package com.latam.pax.robotdistr.domain;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import lombok.Data;

@Data
public class OutputQueue implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@NotEmpty(message = "{domain.OutputQueue.number.notEmpty}", groups = {})	
	private String number;
	
	@NotEmpty(message = "{domain.OutputQueue.pseudoCityCode.notEmpty}", groups = {})
	private String pseudoCityCode;
	
	@NotEmpty(message = "{domain.OutputQueue.businessType.notEmpty}", groups = {})
	private String businessType;
	
	@NotNull(message = "{domain.OutputQueue.officeAccount.notEmpty}", groups = {})		
	private OfficeAccount officeAccount;
	
	@NotNull(message = "{domain.OutputQueue.group.notEmpty}", groups = {})		
	private Group group;
		
}
