package com.latam.pax.robotdistr.domain;

import java.io.Serializable;

import org.hibernate.validator.constraints.NotEmpty;

import lombok.Data;

/**
 * 
 * @author dcarvaja
 *
 */
@Data
public class Parameter implements Serializable{
	private static final long serialVersionUID = 1L;

	@NotEmpty(message = "{domain.Parameter.key.notEmpty}")
	private String key;
	
	@NotEmpty(message = "{domain.Parameter.value.notEmpty}")
	private String value;

}
