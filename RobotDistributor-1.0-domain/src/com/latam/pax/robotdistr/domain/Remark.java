package com.latam.pax.robotdistr.domain;


import java.io.Serializable;

import org.hibernate.validator.constraints.NotEmpty;

import com.latam.pax.robotdistr.validator.group.IntegrityGroup;

import lombok.Data;


/**
 * Remark Domain Object
 * 
 * <br /><br />
 * 
 * Represent a Remark of the Passenger Name Record(PNR)
 *   
 * 
 * @author Everis
 * @author Daniel Carvajal (daniel.carvajal.soto@everis.com)
 * @version 1.0
 *  
 */
@Data
public class Remark implements Serializable {
	private static final long serialVersionUID = 1L;

	@NotEmpty(message = "{domain.Remark.text.notEmpty}", groups = {IntegrityGroup.class})
	private String text;	
	
}
