-- 
-- Use ${targetDBOwner} in order to use the owner name
-- Use ${targetDBServiceUserName} in order to use the service user name
--
--
GRANT DELETE, INSERT, SELECT, UPDATE ON ${targetDBOwner}.HOST_INPUT_QUEUES TO ${targetDBServiceUserName};
GRANT DELETE, INSERT, SELECT, UPDATE ON ${targetDBOwner}.HOST_OUTPUT_QUEUES TO ${targetDBServiceUserName};
GRANT DELETE, INSERT, SELECT, UPDATE ON ${targetDBOwner}.HOST_PARAM TO ${targetDBServiceUserName};


