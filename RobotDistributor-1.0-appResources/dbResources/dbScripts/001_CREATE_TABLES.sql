-- 
-- Use ${targetDBOwner} in order to use the owner name
-- Use ${targetDBServiceUserName} in order to use the service user name
--
--

-------------------------------------------------------------------------------------------------------------------
-- SCHEMA FOR TABLE HOST_INPUT_QUEUES
-------------------------------------------------------------------------------------------------------------------
CREATE TABLE ${targetDBOwner}.HOST_INPUT_QUEUES (
       HSTINQ_NUMBER_CD   		VARCHAR2(3) NOT NULL,
       HSTINQ_PCC_CD      		VARCHAR2(3) NOT NULL,
       HSTINQ_TYPE       	 	VARCHAR2(25) NOT NULL,
       HSTINQ_DESC        		VARCHAR2(255) NULL,
       HSTINQ_MIRROR_NUMBER_CD  VARCHAR2(3) NOT NULL,
       HSTINQ_MIRROR_PCC_CD     VARCHAR2(3) NOT NULL
) TABLESPACE EXLTRBTGQD_DAT;

COMMENT ON TABLE  ${targetDBOwner}.HOST_INPUT_QUEUES IS 'Represents a input queu of Host';
COMMENT ON COLUMN ${targetDBOwner}.HOST_INPUT_QUEUES.HSTINQ_NUMBER_CD IS 'Host Queue number code.';
COMMENT ON COLUMN ${targetDBOwner}.HOST_INPUT_QUEUES.HSTINQ_PCC_CD IS 'Pseudo City Code';
COMMENT ON COLUMN ${targetDBOwner}.HOST_INPUT_QUEUES.HSTINQ_TYPE IS 'Type.

CRTN Creation
MDFN Modification
NTL Interlineal
MNL Manual';
COMMENT ON COLUMN ${targetDBOwner}.HOST_INPUT_QUEUES.HSTINQ_DESC IS 'Queue description';
COMMENT ON COLUMN ${targetDBOwner}.HOST_INPUT_QUEUES.HSTINQ_MIRROR_NUMBER_CD IS 'Host Queue Mirror number code.';
COMMENT ON COLUMN ${targetDBOwner}.HOST_INPUT_QUEUES.HSTINQ_MIRROR_PCC_CD IS 'Mirror Pseudo City Code';

-------------------------------------------------------------------------------------------------------------------
-- SCHEMA FOR TABLE ${targetDBOwner}.HOST_OUTPUT_QUEUES
-------------------------------------------------------------------------------------------------------------------
CREATE TABLE ${targetDBOwner}.HOST_OUTPUT_QUEUES (
       HSTINQ_NUMBER_CD   VARCHAR2(3) NULL,
       HSTINQ_PCC_CD      VARCHAR2(3) NULL,
       HSTOUQ_ACCOUNTING_CD VARCHAR2(3) NOT NULL,
       HSTOUQ_ACCOUNTING_CITY_CD VARCHAR2(3) NOT NULL,
       HSTOUQ_ACCOUNTING_PCC_CD VARCHAR2(3) NOT NULL,
       HSTOUQ_DESC        VARCHAR2(255) NULL,
       HSTOUQ_PCC_CD      VARCHAR2(3) NOT NULL,
       HSTOUQ_NUMBER_CD   VARCHAR2(3) NOT NULL,
       HSTOUQ_BUSINESS_TYPE VARCHAR2(5) NOT NULL,
       HSTOUQ_GRP_TYPE 	  VARCHAR2(4) NOT NULL,
       HSTOUQ_ACTIVE_FLAG CHAR(1) NOT NULL
) TABLESPACE EXLTRBTGQD_DAT;

COMMENT ON TABLE  ${targetDBOwner}.HOST_OUTPUT_QUEUES IS 'Represents a output queu of Host';
COMMENT ON COLUMN ${targetDBOwner}.HOST_OUTPUT_QUEUES.HSTINQ_NUMBER_CD IS 'Host Queue number code.';
COMMENT ON COLUMN ${targetDBOwner}.HOST_OUTPUT_QUEUES.HSTINQ_PCC_CD IS 'Pseudo City Code';
COMMENT ON COLUMN ${targetDBOwner}.HOST_OUTPUT_QUEUES.HSTOUQ_ACCOUNTING_CD IS 'OAC - Accounting code.';
COMMENT ON COLUMN ${targetDBOwner}.HOST_OUTPUT_QUEUES.HSTOUQ_ACCOUNTING_CITY_CD IS 'OAC - Accounting city code';
COMMENT ON COLUMN ${targetDBOwner}.HOST_OUTPUT_QUEUES.HSTOUQ_ACCOUNTING_PCC_CD IS 'OAC - Pseudo city code.';
COMMENT ON COLUMN ${targetDBOwner}.HOST_OUTPUT_QUEUES.HSTOUQ_BUSINESS_TYPE IS 'Queue business type:

DOMestic
INTERnational
';
COMMENT ON COLUMN ${targetDBOwner}.HOST_OUTPUT_QUEUES.HSTOUQ_NUMBER_CD IS 'Host Output Queue number code.';
COMMENT ON COLUMN ${targetDBOwner}.HOST_OUTPUT_QUEUES.HSTOUQ_PCC_CD IS 'Pseudo City Code';
COMMENT ON COLUMN ${targetDBOwner}.HOST_OUTPUT_QUEUES.HSTOUQ_GRP_TYPE IS 'Host Queue group type the 4 first character from de pnr GroupName.';
COMMENT ON COLUMN ${targetDBOwner}.HOST_OUTPUT_QUEUES.HSTOUQ_ACTIVE_FLAG IS 'Is active
Yes
No';
COMMENT ON COLUMN ${targetDBOwner}.HOST_OUTPUT_QUEUES.HSTOUQ_DESC IS 'Queue description';



-------------------------------------------------------------------------------------------------------------------
-- SCHEMA FOR TABLE  ${targetDBOwner}.HOST_PARAM
-------------------------------------------------------------------------------------------------------------------
CREATE TABLE ${targetDBOwner}.HOST_PARAM (
       HSPRMR_CD          VARCHAR2(10) NOT NULL,
       HSPRMR_VAL         VARCHAR2(500) NOT NULL,
       HSPRMR_DESC        VARCHAR2(500) NULL
) TABLESPACE EXLTRBTGQD_DAT;

COMMENT ON TABLE  ${targetDBOwner}.HOST_PARAM IS 'Contains robot configuration data';
COMMENT ON COLUMN ${targetDBOwner}.HOST_PARAM.HSPRMR_CD IS 'Entry code';
COMMENT ON COLUMN ${targetDBOwner}.HOST_PARAM.HSPRMR_VAL IS 'Entry value';
COMMENT ON COLUMN ${targetDBOwner}.HOST_PARAM.HSPRMR_DESC IS 'Entry description';

                        





