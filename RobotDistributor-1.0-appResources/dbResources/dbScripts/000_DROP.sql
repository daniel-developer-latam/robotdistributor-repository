-- 
-- Use ${targetDBOwner} in order to use the owner name
-- Use ${targetDBServiceUserName} in order to use the service user name
--
--
-- **** DROP SCRIPT WILL BE EXECUTED ONLY FOR TEST ENVIRONMENTS ****
--
-- **** YOU MUST WRITE A DROP STATEMENT FOR EVERY STATEMENT ON SCRIPT FILES ****

-- 05_GIVE_GRANTS

-- 04_CREATE_SYNONYM (This synonyms aren't public just for the example. The standard require all synonym's as public)

-- 03_CREATE_INDEX
-- No drop required

-- 02_CREATE_SEQUENCE

-- 01_CREATE_TABLES

REVOKE DELETE, INSERT, SELECT, UPDATE ON EXLTRBTGQD.HOST_INPUT_QUEUES FROM SRVLTRBTGQD;
REVOKE DELETE, INSERT, SELECT, UPDATE ON EXLTRBTGQD.HOST_OUTPUT_QUEUES FROM SRVLTRBTGQD;
REVOKE DELETE, INSERT, SELECT, UPDATE ON EXLTRBTGQD.HOST_PARAM FROM SRVLTRBTGQD; 

-------------------------------------------------------------------------------------------------------------------
-- DROP SYNONYM
-------------------------------------------------------------------------------------------------------------------
DROP PUBLIC SYNONYM HSTINQ;
DROP PUBLIC SYNONYM HSTOUQ;
DROP PUBLIC SYNONYM HSPRMR;


-------------------------------------------------------------------------------------------------------------------
-- DROP TABLES
-------------------------------------------------------------------------------------------------------------------
DROP TABLE 	EXLTRBTGQD.HOST_OUTPUT_QUEUES; 
DROP TABLE  EXLTRBTGQD.HOST_INPUT_QUEUES;
DROP TABLE  EXLTRBTGQD.HOST_PARAM;


