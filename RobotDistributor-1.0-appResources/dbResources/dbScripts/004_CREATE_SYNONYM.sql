-- 
-- Use ${targetDBOwner} in order to use the owner name
-- Use ${targetDBServiceUserName} in order to use the service user name
--
--

-- This synonyms aren't public just for the example. The standard require all synonym's as public
 
  CREATE PUBLIC SYNONYM HSTINQ FOR ${targetDBOwner}.HOST_INPUT_QUEUES;
  CREATE PUBLIC SYNONYM HSTOUQ FOR ${targetDBOwner}.HOST_OUTPUT_QUEUES;
  CREATE PUBLIC SYNONYM HSPRMR FOR ${targetDBOwner}.HOST_PARAM;
  