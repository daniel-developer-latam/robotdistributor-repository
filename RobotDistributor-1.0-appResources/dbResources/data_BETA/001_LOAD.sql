-- Load script for BASIC DATA on LOCAL environments
-- 
-- Use ${targetDBOwner} in order to use the owner name
-- Use ${targetDBServiceUserName} in order to use the service user name
--

-------------------------------------------------------------------------------------------------------------------
-- SEED DATA FOR TABLE ${targetDBOwner}.HOST_INPUT_QUEUES
-------------------------------------------------------------------------------------------------------------------

Insert into ${targetDBOwner}.HOST_INPUT_QUEUES
   (HSTINQ_NUMBER_CD, HSTINQ_PCC_CD, HSTINQ_TYPE, HSTINQ_DESC, HSTINQ_MIRROR_NUMBER_CD, HSTINQ_MIRROR_PCC_CD)
 Values
   ('301', 'JZQ', 'VENTAINDIRECTA ', 'Input Queue de Venta Indirecta', '60', 'JZQ');
Insert into ${targetDBOwner}.HOST_INPUT_QUEUES
   (HSTINQ_NUMBER_CD, HSTINQ_PCC_CD, HSTINQ_TYPE, HSTINQ_DESC, HSTINQ_MIRROR_NUMBER_CD, HSTINQ_MIRROR_PCC_CD)
 Values
   ('201', 'JZQ', 'MODIFICACION ', 'Input Queue de Modificacion', '60', 'JZQ');
Insert into ${targetDBOwner}.HOST_INPUT_QUEUES
   (HSTINQ_NUMBER_CD, HSTINQ_PCC_CD, HSTINQ_TYPE, HSTINQ_DESC, HSTINQ_MIRROR_NUMBER_CD, HSTINQ_MIRROR_PCC_CD)
 Values
   ('101', 'JZQ', 'CREACION ', 'Input Queue de Creacion', '60', 'JZQ');
COMMIT;