-- Load script for BASIC DATA on LOCAL environments
-- 
-- Use ${targetDBOwner} in order to use the owner name
-- Use ${targetDBServiceUserName} in order to use the service user name
--

-------------------------------------------------------------------------------------------------------------------
-- SEED DATA FOR TABLE ${targetDBOwner}.HOST_PARAM
-------------------------------------------------------------------------------------------------------------------
INSERT INTO ${targetDBOwner}.HOST_PARAM (HSPRMR_CD,HSPRMR_VAL,HSPRMR_DESC) VALUES ('SEG_SIZE','20','Segment queue size for retreive pnr. For SABRE restriction number minor 1000');
INSERT INTO ${targetDBOwner}.HOST_PARAM (HSPRMR_CD,HSPRMR_VAL,HSPRMR_DESC) VALUES ('AIRLN_COD','LA,XL,4M,JJ,PZ','Airlines codes for the holding(Ex: LA,XL etc).');
INSERT INTO ${targetDBOwner}.HOST_PARAM (HSPRMR_CD,HSPRMR_VAL,HSPRMR_DESC) VALUES ('QDDOM_NUM','66','Queue Default DOM number.');
INSERT INTO ${targetDBOwner}.HOST_PARAM (HSPRMR_CD,HSPRMR_VAL,HSPRMR_DESC) VALUES ('QDDOM_PCC','JZQ','Queue Default DOM pcc.');
INSERT INTO ${targetDBOwner}.HOST_PARAM (HSPRMR_CD,HSPRMR_VAL,HSPRMR_DESC) VALUES ('QDINT_NUM','67','Queue Default INTER number.');
INSERT INTO ${targetDBOwner}.HOST_PARAM (HSPRMR_CD,HSPRMR_VAL,HSPRMR_DESC) VALUES ('QDINT_PCC','JZQ','Queue Default INTER pcc.');
INSERT INTO ${targetDBOwner}.HOST_PARAM (HSPRMR_CD,HSPRMR_VAL,HSPRMR_DESC) VALUES ('QDDEF_NUM','65','Queue Default Default number.');
INSERT INTO ${targetDBOwner}.HOST_PARAM (HSPRMR_CD,HSPRMR_VAL,HSPRMR_DESC) VALUES ('QDDEF_PCC','JZQ','Queue Default Default pcc.');
INSERT INTO ${targetDBOwner}.HOST_PARAM (HSPRMR_CD,HSPRMR_VAL,HSPRMR_DESC) VALUES ('SEG_STATS','GN,HN,PN','Valid segments status(Ex: GN,HK,KK etc).');
INSERT INTO ${targetDBOwner}.HOST_PARAM (HSPRMR_CD,HSPRMR_VAL,HSPRMR_DESC) VALUES ('CCBR_SPRK','GROUP PNR POS BR','CityCode BR  Special Mark');
-- TODO: Colocar los mail necesarios para PROD : to: cnrm@latam.com, cc: caterina.balocchi@latam.com, sebastian.parodi@latam.com
INSERT INTO ${targetDBOwner}.HOST_PARAM (HSPRMR_CD,HSPRMR_VAL,HSPRMR_DESC) VALUES ('MAIL_TODES','sebastian.parodi@latam.com,sebastian.parodi@latam.com','Email to destinations error(Ex: email1, email2)');
INSERT INTO ${targetDBOwner}.HOST_PARAM (HSPRMR_CD,HSPRMR_VAL,HSPRMR_DESC) VALUES ('MAIL_CCDES','sebastian.parodi@latam.com','Email cc destinations error(Ex: email1, email2)');

COMMIT;